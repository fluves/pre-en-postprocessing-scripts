import CNWS
import itertools
import os
import shutil
import logging
import time
import pandas as pd

def TestModelCombinations(ini, homefolder, testdatafolder, KeepData=False):
    """
    Runs the CNWS-model for all different possible user-choices.
    :param ini: ini-file with all testdata
    :param homefolder: folder where modelresults need to be written
    :param testdatafolder: folder where alle testdata is stored
    :param KeepData: Bool, keep the modelresults or delete them
    :return: pandas dataframe containing some statistics of all combinations
    """
    logger = logging.getLogger('CNWS')
    logger.setLevel(logging.ERROR)

    Options = ['ManualOutlet',
               'EstimClay',
               'Calibrate',
               'Adjusted Slope',
               'Buffer reduce Area',
               'Force Routing',
               'River Routing',
               'UseBuffers',
               'UseGrachten',
               'UseGeleidendeDammen',
               'OutputPerSegment']

    if not os.path.exists(homefolder):
        os.makedirs(homefolder)

    name = CNWS.GetItemFromIni(ini, 'Input', 'name', 'get')
    shp = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'shapefile catchment', 'get'))
    year = CNWS.GetItemFromIni(ini, 'Variables', 'begin_jaar', 'int')
    season = 'spring'

    catchment = CNWS.Catchment(name, shp)

    catchment.P = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'p factor map filename', 'get'))
    catchment.K = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'k factor filename', 'get'))
    catchment.DTM = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'dtm filename', 'get'))
    catchment.segmentkrt = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'river segment filename', 'get'))
    catchment.riverroutingkrt = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'river routing filename', 'get'))
    catchment.adj_edges = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'adjectant segments', 'get'))
    catchment.up_egdes = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'upstream segments', 'get'))

    oldini = CNWS.OldIniFile()
    oldini.ReadIni(ini)

    d = []

    for combinr, i in enumerate(itertools.product(range(2), repeat=len(Options))):
        print('combination %s' %combinr)
        e = {}
        e['NR'] = combinr
        skip = False

        scenario = CNWS.Scenario(catchment, combinr)
        scenario.SetModelVersie('WS')

        EBMOptions = {}
        ModelOptions = {}
        ModelOutput = {}

        # splitting the modeloptions in the python CNWS API-option structure
        for optnr, j in enumerate(i):
            e[Options[optnr]] = j

            if Options[optnr] in ['UseBuffers', 'UseGrachten', 'UseGeleidendeDammen']:
                EBMOptions[Options[optnr]] = j
            elif Options[optnr] == 'L model':
                if j == 0:
                    ModelOptions[Options[optnr]] = 'Desmet1996_Vanoost2003'
                else:
                    ModelOptions[Options[optnr]] = 'Desmet1996_McCool'
            elif Options[optnr] == 'S model':
                if j == 0:
                    ModelOptions[Options[optnr]] = 'Nearing1997'
                else:
                    ModelOptions[Options[optnr]] = 'Desmet1996'
            elif Options[optnr] == 'OutputPerSegment':
                ModelOutput[Options[optnr]] = j
            else:
                ModelOptions[Options[optnr]] = j

        # removing combinations because they are not possible in the model
        if ModelOptions['River Routing'] == 1 and ModelOutput['OutputPerSegment'] == 0:
            skip = True  # this option needs to be skipped because it is not possible
        if ModelOptions['Buffer reduce Area'] == 1 and EBMOptions['UseBuffers'] == 0:
            skip = True  # this option needs to be skipped because it is useless: no buffers -> no uparea to reduce

        e['skip'] = skip

        if not skip:
            scenario.SetModelOptions(ModelOptions, ini)
            scenario.SetEBMOptions(EBMOptions, ini)
            scenario.SetModelVariables({}, ini)
            scenario.SetOutput(ModelOutput, ini)
            scenario.CalculateTimeSettings()
            scenario.infolder = testdatafolder

            sfolder = os.path.join(homefolder, 'scenario_%s' % combinr)
            if not os.path.exists(sfolder):
                os.makedirs(sfolder)
            scenario.sfolder = sfolder

            outfolder = os.path.join(sfolder, 'output')
            if not os.path.exists(outfolder):
                os.makedirs(outfolder)
            scenario.outfolder = outfolder

            scenario.prckrt[year] = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'parcel filename', 'get'))
            scenario.Ckaarten[year] = {}
            scenario.Ckaarten[year][season] = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'c factor map filename', 'get'))
            scenario.Pkaart = catchment.P
            scenario.Kkaart = catchment.K
            scenario.DTMkaart = catchment.DTM

            if scenario.ModelOptions['ManualOutlet'] == 1:
                scenario.outletmap = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'outlet map filename', 'get'))
            if scenario.EBMOptions['UseGrachten'] == 1:
                scenario.grachtenkaart = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'ditch map filename', 'get'))
            if scenario.EBMOptions['UseGeleidendeDammen'] == 1:
                scenario.gdammenkaart = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'dam map filename', 'get'))
            if scenario.EBMOptions['UseBuffers'] == 1:
                scenario.bufferkaart = os.path.join(testdatafolder, CNWS.GetItemFromIni(ini, 'Input', 'buffer map filename', 'get'))
                oldini.ReadBufferData(scenario)
            if scenario.ModelOptions['Force Routing'] == 1:
                oldini.ReadForceRoutingData(scenario)
            if scenario.ModelOptions['River Routing'] == 1:
                scenario.adj_edges = catchment.adj_edges
                scenario.up_edges = catchment.up_egdes
                scenario.riverrouting = catchment.riverroutingkrt
            if scenario.Output['OutputPerSegment'] == 1:
                scenario.riviersegmkrt = catchment.segmentkrt

            scenario.CreateIni()
            starttime = time.time()
            try:
                scenario.RunModel()
            except CNWS.CNWSException as er:
                logger.error('model crash: %s' % er)
                logger.error(ModelOptions)
                logger.error(EBMOptions)
                logger.error(ModelOutput)
                e['Succes'] = False
            else:
                e['Succes'] = True

            endtime = time.time()
            e['time'] = endtime - starttime

            if not KeepData:
                os.chdir(homefolder)
                shutil.rmtree(scenario.sfolder, ignore_errors=True)
        else:
            logger.info('skipped following combination:')
            logger.info(ModelOptions)
            logger.info(EBMOptions)
            logger.info(ModelOutput)

        d.append(e)

    columns = Options
    columns.append('NR')
    columns.append('time')
    columns.append('skip')
    columns.append('Succes')

    df = pd.DataFrame(d, columns=columns)
    return df

