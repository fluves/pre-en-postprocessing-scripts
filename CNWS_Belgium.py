__author__ = 'Daan Renders (Fluves)'
__maintainer__ = 'Daan Renders'
__email__ = 'daan@fluves.com'

import CNWS
import pandas as pd
import geopandas as gpd
import fiona
import shapely
import xlrd
import numpy as np

import os
import logging
import time
import pdb

def makeCalibrationRun(configfile, naam, shp_catch,
                       homefolder, resolutie, jaar,
                       ktcHigh_min, ktcHigh_max,
                       ktcLow_min, ktcLow_max, steps,
                       outletshp, nr, grachtenshp, Rfactor):
    logger = logging.getLogger('CNWS')
    logger.setLevel(logging.INFO)
    logger.info('---------------------------')
    logger.info('   WS Calibration run      ')
    logger.info('---------------------------')
    logger.info(naam)
    logger.info('---------------------------')

    Output = {'OuputPerSegment': 0,
              'WriteRUSLE': 0,
              'WriteSedExport': 0,
              'WriteTillageErosion': 0,
              'WriteWaterErosion': 0}

    Opt = {'Calibrate': 1,
           'CreateKTIL': 1,
           'ManualOutlet': 1,
           'Adjusted Slope': 1,
           'Buffer reduce Area': 1,
           'River Routing': 1
           }

    if resolutie == 5:
        Opt['FilterDTM'] = 1

    EBMopt = {'UseBuffers': 1,
              'UseGras': 1,
              'UseTeelttechn': 0,
              'UseGrachten': 1}

    EBMSources = {'Erosiebesluit': 1,
                  'BO vast': 1,
                  'BO var': 1}

    Vars = {'begin_jaar': jaar,
            'Outletshp': outletshp,
            'KTcHigh_lower': ktcHigh_min,
            'KTcHigh_upper': ktcHigh_max,
            'KTcLow_lower': ktcLow_min,
            'KTcLow_upper': ktcLow_max,
            'steps': steps,
            'R': Rfactor}

    extradata_path = [grachtenshp]
    extradata_type = ['grachten']

    try:
        b = BronData()
        b.LeesConfigFile(configfile)
        bekken = Belgian_catchment(naam, shp_catch)
        scenario = Belgian_Scenario(bekken, nr)
        scenario.SetModelVersie('WS')
        scenario.SetLSmodel()

        defaultini = b.GetDefaultIni(resolutie)
        dtm = b.GetDTM(resolutie)

        scenario.SetOutput(Output, defaultini)
        scenario.SetModelOptions(Opt, defaultini)
        scenario.SetEBMOptions(EBMopt, defaultini)
        scenario.SetModelVariables(Vars, defaultini)
        scenario.CalculateTimeSettings()

        bekken.SetConstants(res=resolutie)
        bekken.PrepareRasterProperties(dtm)
        bekken.CreateFolderStructure(homefolder, scenario.jaren)
        bekken.PrepareP()
        bekken.PrepareDTM(dtm)
        bekken.PrepareK(b.GetKfact())
        bekken.PrepareLandGebruik(b.GetLandgebruik(scenario.jaren[0]))

        bekken.PrepareAdm(b.GetAdmGrenzen())
        bekken.PrepareWater(b.GetVHA(), b.GetWaterlopenWal())
        bekken.PrepareInfrastructuurGRB(b.GetGRBinfra())
        bekken.PrepareWegenregister(b.GetWegregister())
        scenario.SetEBMSources(EBMSources)
        bekken.PreparePercelen(scenario.jaren, b)
        scenario.PrepareEBM(scenario.jaren, b)

        bekken.PrepareWaterShps()
        bekken.TopologizeRivers()
        bekken.PrepareWaterRsts()
        bekken.CreateInfraTif()

        scenario.CreateFolderStructure()

        for i, extra in enumerate(extradata_path):
            scenario.AddExtraEBMData(extra, extradata_type[i])

        scenario.CreateModelInput(b.GetGewasInfo(), scenario.EBMDicts)

        pp = CNWS.PostProcessScenario(scenario)
        pp.MakeRoutingshps_saga()
        if scenario.outletmap is None:
            scenario.outletmap = os.path.join(scenario.infolder, 'Outlet.rst')
        # pp.DefineSubcatchments(scenario.outletmap)
        # pp.plotCalibrationFile()
        areas = pp.CalculateAreasPrcMap()
        calfile = os.path.join(scenario.outfolder, 'calibration.txt')
    except CNWS.CNWSException as e:
        logger.error(e)
    return calfile, areas


# Voer model 1x uit
def CalculateScenario(configfile, naam, shp_catch, modelversie,
                      homefolder, nr, Output, Opt, EBMopt, EBMSources, Belgian_Opt,
                      Vars, extradata_path, extradata_type, resolutie, postprocess=True, returnobjects=False):
    """
    Prepare all modelininput for a scenario, run the CN-WS model and do some basic post-processing

    :param configfile: string, the path of the configuration xlsx
    :param naam: string, name of the catchment
    :param shp_catch: string, the path of the shapefile of the catchment (polygon, 1 feature)
    :param modelversie: string, Modelversion, WS or CNWS
    :param homefolder: string, path to the folder where all data are written and stored
    :param Cnst: dict,
    :param nr: int, number of the scenario
    :param Output: dict
    :param Opt: dict
    :param EBMopt: dict
    :param Vars: dict
    :param extradata_path: list
    :param extradata_type: list
    :return:  nothing
    """

    bekken = None
    scenario = None
    pp = None

    logger = logging.getLogger('CNWS')
    logger.setLevel(logging.INFO)

    try:
        starttime = time.time()
        logger.info('----------------')
        logger.info('     CNWS       ')
        logger.info('----------------')
        logger.info(naam)
        logger.info('----------------')

        b = BronData()
        b.LeesConfigFile(configfile)

        bekken = Belgian_catchment(naam, shp_catch)
        scenario = Belgian_Scenario(bekken, nr)
        scenario.SetModelVersie(modelversie)
        scenario.SetLSmodel()

        defaultini = b.GetDefaultIni(resolutie)
        dtm = b.GetDTM(resolutie)

        scenario.SetOutput(Output, defaultini)
        scenario.SetModelOptions(Opt, defaultini)
        scenario.SetEBMOptions(EBMopt, defaultini)
        scenario.SetModelVariables(Vars, defaultini)
        scenario.SetBelgianOptions(Belgian_Opt)
        scenario.CalculateTimeSettings()

        bekken.SetConstants(res=resolutie)
        bekken.PrepareRasterProperties(dtm)

        bekken.CreateFolderStructure(homefolder, scenario.jaren)

        #(SG) dump Raster Properties to modelinput
        fname = os.path.join(bekken.folder,"Data_Bekken","RasterProp.pkl")
        CNWS.pickleDump(bekken.RasterProp,fname)

        bekken.PrepareP()
        bekken.PrepareDTM(dtm)
        bekken.PrepareK(b.GetKfact())
        bekken.PrepareLandGebruik(b.GetLandgebruik(scenario.jaren[0]))

        bekken.PrepareAdm(b.GetAdmGrenzen())
        bekken.PrepareWater(b.GetVHA(), b.GetWaterlopenWal())
        bekken.PrepareInfrastructuurGRB(b.GetGRBinfra())
        bekken.PrepareWegenregister(b.GetWegregister())
        scenario.SetEBMSources(EBMSources)
        bekken.PreparePercelen(scenario.jaren, b)

        bekken.TopologizeRivers()
        bekken.PrepareWaterShps()
        bekken.PrepareWaterRsts()

        bekken.CreateInfraTif()

        scenario.CreateFolderStructure()

        if scenario.BelgianOptions['VerwijderAlleTeeltInfo'] == 1:
            scenario.RemoveCropInfo(kind='All')
        elif scenario.BelgianOptions['VerwijderEnkelAkkerTeelt'] == 1:
            scenario.RemoveCropInfo()

        if scenario.ModelOptions['BankGrassStrips'] == 1:
            scenario.AddBankGrassStrips(scenario.Variables['Width BankGrassStrips'])

        scenario.PrepareEBM(b)

        for i, extra in enumerate(extradata_path):
            scenario.AddExtraEBMData(extra, extradata_type[i])

        scenario.CreateModelInput(b.GetGewasInfo(), scenario.EBMDicts)

        endtime = time.time()
        logger.info('Pre-processing took %s seconds' % round((endtime - starttime), 2))
        scenario.RunModel()
        starttime = time.time()
        logger.info('Processing took %s seconds' % round((starttime - endtime), 2))

        pp = CNWS.PostProcessScenario(scenario)
        if postprocess:
            pp.DoPostprocessing()
            # pp.DefineSubcatchments(scenario.outletmap)
            endtime = time.time()
            logger.info('Post-processing took %s seconds' % round((endtime - starttime), 2))

        #(SG) dump Raster Properties to modelinput
        fname = os.path.join(scenario.sfolder,"modelinput","RasterProp.pkl")
        CNWS.pickleDump(self.RasterProp,fname)

        logger.info('-------------')
        logger.info('FINISHED! \o/')
        logger.info('-------------')
    except CNWS.CNWSException as e:
        logger.error(e)
        return
    finally:
        os.chdir(homefolder)
        if returnobjects:
            return bekken, scenario, pp
        else:
            return

class BronData:
    """
    Class used to retrieve all source data
    """

    def __init__(self):
        """
        Construct a new BronData object
        """
        self.config_xls = None
        self.df_BO = None
        self.df_percelen = None
        self.df_divers = None
        self.df_lookup = None
        self.df_lndgbrk = None
        self.logger = logging.getLogger(__name__)

    def _zoekDataset(self, df, ID, Type):
        """
        Search a dataset in the given dataframe
        :param df: dataframe, constructed by LeesConfigFile()
        :param ID: ID of the dataset in the config xlsx
        :param Type: Type of dataset, string, 'FILE' or 'FOLDER'
        :return:
        """
        df = df.dropna()
        if any(df.id == ID):
            pad = df[df.id == ID].pad.item()
            if os.path.isfile(pad) and Type == 'FILE':
                return pad
            elif os.path.isdir(pad) and Type == 'FOLDER':
                return pad
            else:
                msg = "path of inputdata is not correct, check path '%s' in configuration xls: " % ID
                msg += pad
                self.logger.warning(msg)
                return None
        else:
            msg = "'%s' is not known as a keyword in the configuration xls or the path is not given" % ID
            self.logger.warning(msg)
            return None

    def _testFields(self, shp, lstFields):
        """
        Checks of all necessary fields are given in the source shapefile
        raises a CNWSException if nog all mandatory fieldnames are present in shp
        :param shp: input shapefile
        :param lstFields: list with mandatory fieldnames
        :return: nothing
        """
        fields = CNWS.GetFields(shp)
        for field in lstFields:
            if field not in fields:
                msg = 'ERROR: %s not in available in %s' % (field, shp)
                raise CNWS.CNWSException(msg)
        return

    def _testGeometry(self, shp, geom):
        """
        Checks if a shapefile contains the correct geometry types
        raises a CNWSException if the geometry is not  equal to geom
        :param shp: input shapefile
        :param geom: mandatory geometry type, see fiona documentation for all possibilities
        :return: nothin
        """

        if geom != CNWS.GetGeometryType(shp):
            msg = 'ERROR: %s has not the desired geometrytype (%s)' % (shp, geom)
            raise CNWS.CNWSException(msg)

    def LeesConfigFile(self, configfile):
        """
        Leest de configfile in als pandas df's als attribuut van de klasse
        """
        self.config_xls = configfile
        try:
            self.logger.info('Reading configuration xls...')
            self.df_BO = pd.read_excel(self.config_xls, sheet_name='BO')
            self.df_percelen = pd.read_excel(self.config_xls, sheet_name='percelen')
            self.df_divers = pd.read_excel(self.config_xls, sheet_name='divers')
            self.df_lookup = pd.read_excel(self.config_xls, sheet_name='look-up')
            self.df_lndgbrk = pd.read_excel(self.config_xls, sheet_name='landgebruik')
        except IOError as e:
            self.logger.error('I/O ERROR: {0} ({1})'.format(e.strerror, self.config_xls))
            msg = ''
            raise CNWS.CNWSException(msg)
        except xlrd.XLRDError as e:
            self.logger.error(e)
            msg = ''
            raise CNWS.CNWSException(msg)
        return

    def GetBOvast(self, jaar):
        """
        Searches and returns the path of Vaste BO for a given year.
        :param jaar:
        :return: the path of the shapefile, or, if the shapefile does not exist, None
        """
        BOid_vast = 'BO%s1' % str(jaar)
        shp = self._zoekDataset(self.df_BO, BOid_vast, 'FILE')
        if shp is not None:
            self._testFields(shp, ['BHPAKKET', 'LENGTE', 'BREEDTE'])
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetBOvar(self, jaar):
        """
        Searches and returns the path of Variable BO for a given year.
        :param jaar:
        :return: the path of the shapefile, or, if the shapefile does not exist, None
        """
        BOid_var = 'BO%s2' % str(jaar)
        shp = self._zoekDataset(self.df_BO, BOid_var, 'FILE')
        if shp is not None:
            self._testFields(shp, ['BHPAKKET'])
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetPercelenWal(self, jaar):
        perceelID = 'PW%s' % str(jaar)
        shp = self._zoekDataset(self.df_percelen, perceelID, 'FILE')
        if shp is not None:
            self._testFields(shp, ['CULT_COD'])
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetPercelenVl(self, jaar):
        """
        Searches and returns the path of Percelenshapefile for a given year.
        :param jaar:
        :return: the path of the shapefile, or, if the shapefile does not exist, None
        """
        perceelID = 'P%s' % str(jaar)
        shp = self._zoekDataset(self.df_percelen, perceelID, 'FILE')
        if shp is not None:
            self._testFields(shp, ['GWSCOD_H', 'GWSCOD_V', 'GWSCOD_N'])
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetVHA(self):
        """
        Searches and returns the path of the VHA for a given year.
        :return: the path of the shapefile, or, if the shapefile does not exist, None
        """
        shp = self._zoekDataset(self.df_divers, 'VHA', 'FILE')
        if shp is not None:
            self._testFields(shp, ['VHAG', 'NAAM'])
            self._testGeometry(shp, 'LineString')
        return shp

    def GetGRBfolder(self):
        """
        Searches and returns the path of the GRB-folder.
        :return: the path of the GRB-folder, or, if the folder does not exist, None
        """
        folder = self._zoekDataset(self.df_divers, 'GRB', 'FOLDER')
        return folder

    def GetGRBinfra(self):
        shp = self._zoekDataset(self.df_divers, 'GRBinfra', 'FILE')
        if shp is not None:
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetEBM_besluit(self):
        """
        Searches and returns the path of the 'gerealiseerde maatregelen'. This function also tests if all
        mandatory fields are present and if the geometry type is polygon
        :return: the path of the shapefile, or, if the file does not exist, None
        """
        shp = self._zoekDataset(self.df_divers, 'EBM', 'FILE')
        if shp is not None:
            self._testFields(shp, ['Type', 'Subtype', 'Buffercap', 'Breedte'])
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetEBM_VlBr(self):
        """
        Searches and returns the path of the 'maatregelen Vlaams Brabant'. This function also tests if all
        mandatory fields are present and if the geometry type is polygon
        :return: the path of the shapefile, or, if the file does not exist, None
        """
        shp = self._zoekDataset(self.df_divers, 'EBMVLBR', 'FILE')
        if shp is not None:
            self._testFields(shp, ['Realisatie', 'Type', 'Subtype'])
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetEBM_OVl(self):
        return self._zoekDataset(self.df_divers, 'EBMOVL', 'FILE')

    def GetAdmGrenzen(self):
        """
        Searches and returns the path of the adminstrative boundaries. This function also tests if all
        mandatory fields are present and if the geometry type is polygon
        :return: the path of the shapefile, or, if the file does not exist, None
        """
        shp = self._zoekDataset(self.df_divers, 'ADM', 'FILE')
        if shp is not None:
            self._testFields(shp, ['Gewest', 'NAME_DUT'])
            self._testGeometry(shp, 'Polygon')
        return shp

    def GetDTM(self, resolution=20):
        if resolution == 20:
            return self._zoekDataset(self.df_divers, 'DTM_20', 'FILE')
        elif resolution == 5:
            return self._zoekDataset(self.df_divers, 'DTM_5', 'FILE')
        else:
            msg = 'no dtm available for this resolution!'
            raise CNWS.CNWSException(msg)

    def GetKfact(self):
        return self._zoekDataset(self.df_divers, 'KFCT', 'FILE')

    def GetLandgebruik(self, jaar):
        lndgbrk_id = 'LNDGBR%s' % jaar
        return self._zoekDataset(self.df_lndgbrk, lndgbrk_id, 'FILE')

    def GetCNbodem(self):
        return self._zoekDataset(self.df_divers, 'CN', 'FILE')

    def GetWaterlopenWal(self):
        shp = self._zoekDataset(self.df_divers, 'CNN', 'FILE')
        if shp is not None:
            self._testGeometry(shp, 'LineString')
        return shp

    def GetWegregister(self):
        shp = self._zoekDataset(self.df_divers, 'WEG', 'FILE')
        if shp is not None:
            self._testFields(shp, ['LBLMORF'])
            self._testGeometry(shp, 'LineString')
        return shp

    def GetGewasInfo(self):
        ds = self._zoekDataset(self.df_lookup, 'GEWAS', 'FILE')
        return ds

    def GetGwsWal(self):
        ds = self._zoekDataset(self.df_lookup, 'GWSWAL', 'FILE')
        return ds

    def GetBOInfo(self):
        return self._zoekDataset(self.df_lookup, 'BO', 'FILE')

    def GetCNTable(self):
        return self._zoekDataset(self.df_lookup, 'CN', 'FILE')

    def GetStyleFolder(self):
        return self._zoekDataset(self.df_lookup, 'QGS', 'FOLDER')

    def GetDefaultIni(self, resolution=20):
        if resolution == 5:
            return self._zoekDataset(self.df_lookup, 'INI_5', 'FILE')
        elif resolution == 20:
            return self._zoekDataset(self.df_lookup, 'INI_20', 'FILE')
        else:
            msg = 'for this resolution are no default-values!'
            raise CNWS.CNWSException(msg)


class Belgian_catchment(CNWS.Catchment):
    """
    """
    def __init__(self, catchmentnaam, catchmentshp):
        CNWS.Catchment.__init__(self, catchmentnaam, catchmentshp)
        # self.catchm = CNWS.Catchment(catchmentnaam, catchmentshp)

        self.Bxl = True
        self.Wal = True
        self.Provincies = []

        self.vha = None
        self.waterWal = None

        self.prcWal = {}
        self.prcVl = {}

        self.EBMSources = {}
        self.ebmProvincie = {}
        self.ebmBesluit = {}

        self.BOvar = {}
        self.BOvast = {}

        self.grbfolder = None
        return

    def PrepareAdm(self, shpProvincies):
        """
        Check in which provinces and regions the catchment lies
        sets the self.Provincies, self.Bxl and self.Wal arguments of the bekken instance
        :param shpProvincies: shpfile with provincial boundaries
        :return: nothing
        """
        self.logger.info('Check administratieve grenzen...')
        try:
            gdf = gpd.read_file(shpProvincies)
        except Exception:
            msg = 'could not open %s ' % shpProvincies
            msg += 'Geen check op administratieve grenzen gedaan!'
            self.logger.warning(msg)
        else:
            with fiona.open(self.shp) as c:
                catch = c.next()
                geom_catch = shapely.geometry.shape(catch['geometry'])
                gdf['intersects'] = gdf.intersects(geom_catch)

            gdf = gdf.loc[gdf['intersects'] == True]
            self.Provincies = gdf['NAME_DUT'].tolist()
            gewesten = gdf['Gewest'].tolist()
            if 'Brussel' not in gewesten:
                self.Bxl = False
            if 'Wallonie' not in gewesten:
                self.Wal = False
        return

    def PrepareVHA(self, vha):
        """
        Clipt VHA-shapefile on catchment boundary
        :param vha: path of vha-shapefile
        :return: nothing
        """
        if vha is not None:
            self.logger.info('Voorbereiden VHA...')
            self.vha = os.path.split(vha)[1][:-4] + '_%s.shp' % self.naam
            self.vha = os.path.join(self.shpfolder, self.vha)
            if not os.path.isfile(self.vha):
                CNWS.clipShp(vha, self.vha, self.shp)
        return

    def PrepareWaterWal(self, water_wal):
        """
        Clipt shapefile Cours d'eau de Wallonie
        :param water_wal: path of input shapefile
        :return: nothing
        """
        if water_wal is not None:
            self.logger.info('Voorbereiden Waalse waterlopen...')
            self.waterWal = os.path.split(water_wal)[1][:-4] + '_%s.shp' % self.naam
            self.waterWal = os.path.join(self.shpfolder, self.waterWal)
            if not os.path.isfile(self.waterWal):
                CNWS.clipShp(water_wal, self.waterWal, self.shp)
        return

    def PrepareWater(self, vha, water_wal):
        self.PrepareVHA(vha)
        if self.Wal:
            self.PrepareWaterWal(water_wal)
            if self.waterWal is not None:
                if CNWS.GetFeatureCount(self.waterWal) >= 1:
                    self.logger.info('Merging Waalse en Vlaamse waterlopen...')
                    self.waterLine = 'waterlopen_line_%s.shp' % self.naam
                    self.waterLine = os.path.join(self.shpfolder, self.waterLine)
                    CNWS.MergeShps([self.vha, self.waterWal], self.waterLine, self.RasterProp['epsg'])
                else:
                    self.logger.warning('Geen Waalse waterloopsegmenten aanwezig!')
                    self.waterLine = self.vha
        else:
            self.waterLine = self.vha
        return

    def PrepareWaterGRB(self, grb_folder):
        """
        Haalt alle waterdata uit het GRB (behalve wlas)
        Sets self.waterGRB
        :param grb_folder: folder where all grb shapes are stored
        :return: Nothing
        """
        if grb_folder is not None:
            self.logger.info('Voorbereiden GRB-Water...')
            workdir = self.grbfolder
            os.chdir(workdir)
            # clip alle grb-shps die van toepassing zijn voor het model
            grbshps = ['Wgr', 'Wtz']
            grb = {}
            for shp in grbshps:
                srcShp = os.path.join(grb_folder, shp + '.shp')
                if os.path.isfile(srcShp):
                    clippedShp = shp + '_%s.shp' % self.naam
                    if not os.path.isfile(clippedShp):
                        CNWS.clipShp(srcShp, clippedShp, self.shp)

                    if shp == 'Wtz' and CNWS.GetFeatureCount(clippedShp) != 0:
                        df_WTZ = gpd.read_file(clippedShp)
                        try:
                            df_Wlas = gpd.read_file(self.vha)
                        except:
                            msg = 'ERROR: VHA not yet processed, please run prepareVHA before running PrepareWaterGRB'
                            raise CNWS.CNWSException(msg)
                        else:
                            df_poelen = df_WTZ.loc[(df_WTZ['VHAG'] == -8) | (df_WTZ['VHAG'] == -9)].copy()
                            vhapoly_df = df_WTZ.loc[(df_WTZ['VHAG'] != -8) & (df_WTZ['VHAG'] != -9)].copy()
                            if df_poelen.shape[0] != 0:
                                for wlas in df_Wlas.itertuples():
                                    df_poelen['intersection'] = df_poelen.intersects(getattr(wlas, 'geometry'))
                                    df_poelen.loc[df_poelen['intersection'], "VHAG"] = getattr(wlas, 'VHAG')
                                    df_poelen.loc[df_poelen['intersection'], "NAAM"] = getattr(wlas, 'NAAM')

                                df_extravha = df_poelen.loc[
                                    (df_poelen['VHAG'] != -8) & (df_poelen['VHAG'] != -9)].copy()
                                df_extravha.drop('intersection', axis=1, inplace=True)
                                frames = [vhapoly_df, df_extravha]
                                vhapoly_df = pd.concat(frames, ignore_index=True)

                                df_poelen = df_poelen.loc[(df_poelen['VHAG'] == -8) | (df_poelen['VHAG'] == -9)].copy()
                                df_poelen.drop('intersection', axis=1, inplace=True)

                            poelenshp = 'poelen_grb_%s.shp' % self.naam
                            grb['poelen'] = os.path.join(workdir, poelenshp)
                            df_poelen.to_file(grb['poelen'])

                            vhapoly_shp = 'vha_polygonen_grb_%s.shp' % self.naam
                            grb['vha_poly'] = os.path.join(workdir, vhapoly_shp)
                            vhapoly_df.to_file(grb['vha_poly'])
                    elif shp == 'Wgr':
                        grb['grachten'] = os.path.join(workdir, clippedShp)
                else:
                    self.logger.warning("File %s not found") % srcShp
                    break
            self.waterGRB = grb
        return

    def PrepareInfrastructuurGRB(self, grb_infra):
        """
        Clipt de infrastructuurshp van het GRB op het catchment

        :param grb_infra : folder met de shapebestanden van het grb
        :return nothing
        """
        if grb_infra is not None:
            self.logger.info('Voorbereiden GRB-infrastructuur...')
            # clip grb-infra shp
            if os.path.isfile(grb_infra):
                clippedShp = os.path.split(grb_infra)[1][:-4] + '_%s.shp' % self.naam
                clippedShp = os.path.join(self.shpfolder, clippedShp)
                if not os.path.isfile(clippedShp):
                    CNWS.clipShp(grb_infra, clippedShp, self.shp)
                    CNWS.CreateSpatialIndex(clippedShp)

                if CNWS.GetFeatureCount(clippedShp) >= 1:
                    self.infrpoly = clippedShp
        return

    def PrepareGRB(self, grb_folder):
        """
        Bereidt het GRB volledig voor (clippen/selecteren)
        :param grb_folder: de folder waar de shapefiles van het GRB bewaard worden
        :return nothing
        """
        self.logger.info('Voorbereiden GRB...')
        # self.PrepareInfrastructuurGRB(grb_folder)
        self.PrepareWaterGRB(grb_folder)
        return

    def PrepareWegenregister(self, wegenshp):
        """
        Clipt het wegenregister op basis van het catchment en maakt een
        shp met alle aardewegen aan

        :param wegenshp: de segmentenshp van het wegenregister (lijn)
        """
        if wegenshp is not None:
            self.logger.info('Voorbereiden Wegenregister...')

            os.chdir(self.shpfolder)
            clippedShp = os.path.split(wegenshp)[1][:-4] + '_%s.shp' % self.naam

            if not os.path.isfile(clippedShp):
                CNWS.clipShp(wegenshp, clippedShp, self.shp)
                CNWS.CreateSpatialIndex(clippedShp)

            if CNWS.GetFeatureCount(clippedShp) >= 1:
                self.infrline = os.path.join(self.shpfolder, clippedShp)
                gdf_line = gpd.read_file(self.infrline)
                if not 'paved' in gdf_line.columns:
                    gdf_line['paved'] = np.where(gdf_line.LBLMORF == 'aardeweg', -7, -2)
                    gdf_line.to_file(self.infrline)
        return

    def PreparePercelenWal(self, percelenWal, jaar, gwscodesWal):
        """
        clipt de waalse percelen op de bekkengrenzen en voegt het attribuut 'GWSCOD_H' toe
        Zet self.prcWal[jaar] gelijk aan de geclipte shapefile,
        of op None indien er geen features in het modeldomein liggen
        :param percelenWal: input shapefile
        :param jaar: (int) jaar
        :param gwscodesWal: csv tabel met vertaling waalse gewascodes naar GWSCOD_H
        :return:
        """
        self.logger.info('Voorbereiden Waalse percelen %s...' % jaar)
        workdir = os.path.join(self.shpfolder, str(jaar))
        os.chdir(workdir)

        if percelenWal is not None and self.Wal:
            percelen = os.path.split(percelenWal)[1][:-4] + '_%s.shp' % self.naam
            if not os.path.exists(percelen):
                CNWS.clipShp(percelenWal, percelen, self.shp)

            if CNWS.GetFeatureCount(percelen) != 0:
                # TO DO: joinen met gwswal en aanmaken veld 'GWSCOD_H'
                gdf = gpd.read_file(percelen)
                gdf['CULT_COD'] = gdf.CULT_COD.astype('int64')

                if 'GWSCOD_H' not in gdf.columns:
                    if gwscodesWal is None:
                        gdf['GWSCOD_H'] = 9999
                    else:
                        df = pd.read_csv(gwscodesWal, sep=';')
                        gdf = pd.merge(gdf, df, how='left', on='CULT_COD')

                gdf.to_file(percelen)
                self.prcWal[jaar] = percelen
            else:
                msg = 'Er zijn geen Waalse percelen in het modelgebied'
                self.logger.warning(msg)
                self.prcWal[jaar] = None
        else:
            msg = 'Er zijn geen Waalse percelen'
            self.logger.debug(msg)
            self.prcWal[jaar] = None
        return

    def PreparePercelenVl(self, percelenVl, jaar):
        """
        Clipt de Vlaamse percelen op de bekken grenzen
        Zet het self.prcVl[jaar] gelijk aan de geclipte shapefile of, indien geen percelen in modeldomein, op None
        :param percelenVl: shapefile met de vlaamse percelen
        :param jaar: jaar (int)
        :return: Nothing
        """
        self.logger.info('Voorbereiden Vlaamse percelen %s...' % jaar)
        workdir = os.path.join(self.shpfolder, str(jaar))
        os.chdir(workdir)

        if percelenVl is not None:
            percelen = os.path.split(percelenVl)[1][:-4] + '_%s.shp' % self.naam
            #import sys;sys.exit(percelen)
            if not os.path.exists(percelen):
                CNWS.clipShp(percelenVl, percelen, self.shp)
            if CNWS.GetFeatureCount(percelen) != 0:
                self.prcVl[jaar] = percelen
            else:
                msg = 'Er liggen geen Vlaamse landbouwpercelen in het modelgebied'
                self.logger.warning(msg)
                self.prcVl[jaar] = None
        else:
            msg = 'Er zijn geen Vlaamse landbouwpercelen voor %s' % str(jaar)
            self.logger.warning(msg)
            self.prcVl[jaar] = None
        return

    def Prepare_BO(self, shp_BO, Type, jaar, csvtable):
        """
        Splitst de shapefile van de BO's (VLM) op in shp's per ebm

        Parameters
        -----------
        shp_BO : polygonshapefile met de BO's
        type : 'vast' or 'var' (vaste BO's voor 5 jaar, of jaarlijks varierende BO)
        jaar : jaar waarop de BO van toepassing is (integer)
        csvtable : csv-bestand met de link tussen BO en type ebm

        Returns
        --------
        Dictionary met de de verschillende opgesplitste ebm's
        wanneer er geen features in een shp zitten wordt deze shp niet gereturned
        dict keys:
        - teelttechn
        - gras
        - buffer
        - poelen
        """
        split_BO = {}
        if shp_BO is not None and csvtable is not None:
            self.logger.info('Voorbereiden Beheersovereenkomsten %s %s...' % (jaar, Type))
            workdir = os.path.join(self.shpfolder, str(jaar))
            os.chdir(workdir)

            clippedShp = os.path.split(shp_BO)[1][:-4] + '_%s.shp' % self.naam
            if not os.path.exists(clippedShp):
                CNWS.clipShp(shp_BO, clippedShp, self.shp)
            split_BO['clippedBO'] = clippedShp

            # join shp BO met csvtable en schrijf weg
            if CNWS.GetFeatureCount(clippedShp) != 0:
                gdf = gpd.read_file(clippedShp)
                df = pd.read_csv(csvtable, sep=',')
                gdf = pd.merge(gdf, df, on='BHPAKKET')

                teelt_BO = 'teelttechn_BO_%s_%s_%s.shp' % (Type, self.naam, str(jaar))
                if not os.path.isfile(teelt_BO):
                    teelt_BOdf = gdf[((gdf['Type'] == 'ntkerend') |
                                      (gdf['Type'] == 'stoppel') |
                                      (gdf['Type'] == 'directe_inzaai'))]
                    if teelt_BOdf.shape[0] >= 1:
                        teelt_BOdf.to_file(teelt_BO)
                        split_BO['teelttechn'] = teelt_BO
                elif CNWS.GetFeatureCount(teelt_BO) >= 1:
                    split_BO['teelttechn'] = teelt_BO

                if Type == 'vast':
                    gras_BO = 'gras_BO_%s_%s.shp' % (self.naam, str(jaar))
                    if not os.path.isfile(gras_BO):
                        gras_BOdf = gdf[gdf['Type'] == 'gras']
                        if gras_BOdf.shape[0] >= 1:
                            gras_BOdf.to_file(gras_BO)
                            split_BO['gras'] = gras_BO
                    elif CNWS.GetFeatureCount(gras_BO) >= 1:
                        # zou eigenlijk altijd > 1 mtn zijn, shps met 0 features worden niet weggeschreven
                        split_BO['gras'] = gras_BO

                    buffer_BO = 'buffers_BO_%s_%s.shp' % (self.naam, str(jaar))
                    if not os.path.isfile(buffer_BO):
                        buffer_df = gdf[gdf['Type'] == 'bufferbekken'].copy()
                        if buffer_df.shape[0] >= 1:
                            buffer_df['HDam'] = .75
                            buffer_df['HKnijp'] = 0
                            buffer_df['DKnijp'] = .03
                            buffer_df['QCoef'] = .6
                            buffer_df['Boverl'] = 7
                            buffer_df['Eff'] = 75
                            buffer_df['Buffercap'] = buffer_df['LENGTE'] * buffer_df['BREEDTE'] * .75
                            buffer_df.to_file(buffer_BO)
                            split_BO['buffer'] = buffer_BO
                    elif CNWS.GetFeatureCount(buffer_BO) >= 1:
                        split_BO['buffer'] = buffer_BO

                    # poelen_BO = 'poelen_BO_%s_%s.shp' % (self.naam, str(jaar))
                    # if not os.path.isfile(poelen_BO):
                    #     poel_df = gdf[gdf['Type'] == 'poel']
                    #     if poel_df.shape[0] >= 1:
                    #         poel_df.to_file(poelen_BO)
                    #         split_BO['poelen'] = poelen_BO
                    # elif GetFeatureCount(poelen_BO) >= 1:
                    #     split_BO['poelen'] = poelen_BO

                for key in split_BO.keys():
                    split_BO[key] = os.path.join(workdir, split_BO[key])

        if Type == 'vast':
            self.BOvast[jaar] = split_BO
        elif Type == 'var':
            self.BOvar[jaar] = split_BO

        return

    def PrepareEBM_besluit(self, shp_albon, jaar):
        """
        Clipt de gerealiseerde maatregelen op het catchment
        Splitst de shapefile van de gerealiseerde maatregelen via het erosiebesluit op in shp's per ebm

        Parameters
        -----------
        shp_albon : polygoon shapefile met alle gerealiseerde maatregelen
        jaar : jaar waarin de ebm's van toepassing zijn (integer)

        Returns
        --------
        Dictionary met de de verschillende opgesplitste ebm's
        dict keys:
        - gras
        - buffer
        - grachten
        - gdammen

        """
        if shp_albon is not None:
            self.logger.info('Voorbereiden EBM erosiebesluit %s...' % jaar)
            albon_ebm = {}
            os.chdir(self.shpfolder)
            clippedShp = 'ebm_albon_%s.shp' % self.naam
            clippedShp = os.path.join(self.shpfolder, clippedShp)
            if not os.path.isfile(clippedShp):
                CNWS.clipShp(shp_albon, clippedShp, self.shp)

            workdir = os.path.join(self.shpfolder, str(jaar))
            os.chdir(workdir)
            if CNWS.GetFeatureCount(clippedShp) > 0:
                gdf = gpd.read_file(clippedShp)
                if 'Breedte' in gdf.columns:
                    gdf.rename(columns={'Breedte': 'BREEDTE'}, inplace=True)
                gdf.to_file(clippedShp)

                gdf = gdf[(gdf['Aanleg'] <= jaar)]
                grasshp = 'gras_albon_%s_%s.shp' % (self.naam, str(jaar))
                if not os.path.isfile(grasshp):
                    grasdf = gdf[((gdf['Type'] == 'grasgang')
                                  | (gdf['Type'] == 'graszone')
                                  | (gdf['Type'] == 'grasbufferstrook'))].copy()
                    if grasdf.shape[0] >= 1:
                        grasdf.to_file(grasshp)
                        albon_ebm['gras'] = grasshp
                elif CNWS.GetFeatureCount(grasshp) >= 1:
                    albon_ebm['gras'] = grasshp

                buffershp = 'buffers_albon_%s_%s.shp' % (self.naam, str(jaar))
                if not os.path.isfile(buffershp):
                    buffer_df = gdf[(((gdf['Type'] == 'aarden dam') & (gdf['Subtype'] != 'geleidende dam'))
                                     | (gdf['Type'] == 'bufferbekken')
                                     | (gdf['Type'] == 'dam plantaardige materialen')
                                     | (gdf['Type'] == 'erosiepoel'))].copy()
                    if buffer_df.shape[0] >= 1:
                        dissolvedf = gpd.GeoDataFrame()
                        dissolvedf['geometry'] = buffer_df.buffer(2)
                        dissolvedf['dissolve'] = 1
                        dissolvedf = dissolvedf.dissolve(by='dissolve')
                        multipolygon = dissolvedf.geometry.values[
                            0]  # shapely multipolygon object containing all buffer polygons

                        if multipolygon.geom_type == 'Polygon':
                            # convert polygon-instance to multipolygon instance
                            multipolygon = shapely.geometry.multipolygon.MultiPolygon([multipolygon])

                        schema = {'geometry': 'Polygon',
                                  'properties': {'id': 'int',
                                                 'Eff': 'int',
                                                 'HKnijp': 'int',
                                                 'DKnijp': 'float',
                                                 'QCoef': 'float',
                                                 'Boverl': 'int',
                                                 'opp': 'float',
                                                 'HDam': 'float',
                                                 'Buffercap': 'float'}}

                        with fiona.open(buffershp, 'w', 'ESRI Shapefile', schema) as dst:
                            for i, polygon in enumerate(multipolygon):
                                p = multipolygon[i].simplify(0.5, preserve_topology=True)
                                tempdf = buffer_df.copy()
                                tempdf['intersects'] = tempdf.intersects(p)
                                tempdf = tempdf.loc[tempdf['intersects'] == True]

                                bufcap = tempdf.Buffercap.sum().astype('float')
                                if bufcap != 0:
                                    hdam = bufcap / p.area
                                elif 'aarden dam' in tempdf.Type.values:
                                    hdam = 1.5
                                    bufcap = p.area * hdam
                                else:
                                    hdam = 1
                                    bufcap = p.area * hdam

                                if 'dam plantaardige materialen' in tempdf.Type.values:
                                    dknijp = 0.07
                                else:
                                    dknijp = 0.03

                                dst.write({'geometry': shapely.geometry.mapping(p),
                                           'properties': {'id': i + 1,
                                                          'Eff': 75,
                                                          'HKnijp': 0,
                                                          'DKnijp': dknijp,
                                                          'QCoef': 0.6,
                                                          'Boverl': 7,
                                                          'opp': p.area,
                                                          'HDam': hdam,
                                                          'Buffercap': bufcap}})

                        albon_ebm['buffer'] = buffershp
                elif CNWS.GetFeatureCount(buffershp) >= 1:
                    albon_ebm['buffer'] = buffershp

                #                grachtenshp = 'grachten_albon_%s_%s.shp' % (self.naam, str(jaar))
                #                if not os.path.isfile(grachtenshp):
                #                    grachten_df = gdf[(gdf['Type'] == 'gracht')].copy()
                #                    if grachten_df.shape[0] >= 1:
                #                        grachten_df.to_file(grachtenshp)
                #                        albon_ebm['grachten'] = grachtenshp
                #                elif GetFeatureCount(grachtenshp) >= 1:
                #                    albon_ebm['grachten'] = grachtenshp
                #
                #                gdammenshp = 'geleidendedammen_albon_%s_%s.shp' % (self.naam, str(jaar))
                #                if not os.path.isfile(gdammenshp):
                #                    gdammen_df = gdf[((gdf['Type'] == 'aarden dam') & (gdf['Subtype'] == 'geleidende dam'))].copy()
                #                    if gdammen_df.shape[0] >= 1:
                #                        gdammen_df.to_file(gdammenshp)
                #                        albon_ebm['gdammen'] = gdammenshp
                #                elif GetFeatureCount(gdammenshp) >= 1:
                #                    albon_ebm['gdammen'] = gdammenshp

                for key in albon_ebm.keys():
                    albon_ebm[key] = os.path.join(os.getcwd(), albon_ebm[key])

            self.ebmBesluit[jaar] = albon_ebm
        else:
            self.ebmBesluit[jaar] = {}
        return

    def PrepareEBMVlBr(self, inShp, jaar):
        if inShp is not None:
            self.logger.info('Voorbereiden EBM Vlaams Brabant...')
            ebm = {}
            os.chdir(self.shpfolder)
            clippedShp = 'ebm_vl_brabant_%s.shp' % self.naam
            clippedShp = os.path.join(self.shpfolder, clippedShp)
            if not os.path.isfile(clippedShp):
                CNWS.clipShp(inShp, clippedShp, self.shp)

            workdir = os.path.join(self.shpfolder, str(jaar))
            os.chdir(workdir)
            if CNWS.GetFeatureCount(clippedShp) > 0:
                gdf = gpd.read_file(clippedShp)
                gdf = gdf.loc[(gdf.Realisatie != 'Erosiebesluit')]

                grasshp = 'gras_vlbr_%s.shp' % self.naam
                if not os.path.isfile(grasshp):
                    grasdf = gdf[((gdf['Type'] == 'grasgang')
                                  | (gdf['Type'] == 'graszone')
                                  | (gdf['Type'] == 'grasbufferstrook'))].copy()
                    if grasdf.shape[0] >= 1:
                        grasdf.to_file(grasshp)
                        ebm['gras'] = grasshp
                elif CNWS.GetFeatureCount(grasshp) >= 1:
                    ebm['gras'] = grasshp

                buffershp = 'buffers_vlbr_%s.shp' % self.naam
                if not os.path.isfile(buffershp):
                    buffer_df = gdf[(((gdf['Type'] == 'aarden dam') & (gdf['Subtype'] != 'geleidende dam'))
                                     | (gdf['Type'] == 'bufferbekken')
                                     | (gdf['Type'] == 'dam plantaardige materialen')
                                     | (gdf['Type'] == 'erosiepoel'))].copy()
                    if buffer_df.shape[0] >= 1:
                        buffer_df['HKnijp'] = 0
                        buffer_df['QCoef'] = 0.6
                        buffer_df['Boverl'] = 7
                        buffer_df['Eff'] = 75
                        buffer_df['DKnijp'] = np.where(buffer_df['Type'] == 'dam plantaardige materialen', 0.07, 0.03)
                        buffer_df['HDam'] = 1
                        buffer_df['Buffercap'] = buffer_df.area * buffer_df['HDam']
                        buffer_df.to_file(buffershp)
                        ebm['buffer'] = buffershp
                elif CNWS.GetFeatureCount(buffershp) >= 1:
                    ebm['buffer'] = buffershp
            self.ebmProvincie[jaar] = ebm
        else:
            msg = 'Geen EBM uitgevoerd door Vlaams-Brabant in modelgebied'
            self.logger.info(msg)
            self.ebmProvincie[jaar] = {}
        return

    def PreparePercelen(self, jaren, sourcedata):
        """
        Voegt de Vlaamse en Waalse percelen samen en voegt alle noodzakelijke attributen toe
        Kan enkel gebruikt worden indien self.prcVL en self.prcWal voor het jaar ingevuld zijn
        :param jaren: het jaar waarin de percelenshp van toepassing is
        :param sourcedata: csv-file met alle gewascodes
        """

        for jaar in jaren:

            self.PreparePercelenVl(sourcedata.GetPercelenVl(jaar), jaar)
            if self.Wal:
                self.PreparePercelenWal(sourcedata.GetPercelenWal(jaar), jaar, sourcedata.GetGwsWal())
            else:
                self.prcWal[jaar] = None

            gwscodes = sourcedata.GetGewasInfo()

            prcCatch = os.path.join(self.shpfolder, str(jaar))
            prcCatch = os.path.join(prcCatch, 'percelen_%s_%s.shp' % (self.naam, str(jaar)))
            prcLst = []
            if not os.path.exists(prcCatch):
                if self.prcVl[jaar] is not None:
                    prcLst.append(self.prcVl[jaar])
                if self.prcWal[jaar] is not None:
                    prcLst.append(self.prcWal[jaar])

                if len(prcLst) != 0:
                    CNWS.MergeShps(prcLst, prcCatch, self.RasterProp['epsg'])
                    if CNWS.GetFeatureCount(prcCatch) == 0:  # zou eigenlijk niet mogen voorkomen
                        self.percelen[jaar] = None
                        msg = 'Geen percelen in modelgebied in %s aanwezig' % jaar
                        self.logger.warning(msg)
                    else:
                        self.logger.info('Toevoegen attributen aan percelenshp...')
                        gdf = gpd.read_file(prcCatch)
                        if 'REF_ID' in gdf.columns:
                            gdf.drop('REF_ID', axis=1, inplace=True)  # droppen van REF_ID om ogr-warning kwijt te raken
                        gdf['temp_nr'] = range(1, gdf.shape[0] + 1)
                        # kunstgreep om met id's om te kunnen gaan in gebieden met meer dan 16-bit percelen
                        gdf['NR'] = gdf.temp_nr % 32757
                        gdf.drop('temp_nr', axis=1, inplace=True)
                        fields = ['ntkerend', 'drempels', 'contour', 'gewasrest', 'groenbedek']
                        for field in fields:
                            gdf[field] = 0

                        # indien niet alle percelen een gewascode hebben: gwscod_H wordt 9999 (onbekende teelt)
                        gdf['GWSCOD_H'] = np.where(gdf['GWSCOD_H'].isnull(), 9999, gdf['GWSCOD_H'])

                        if gwscodes is not None:
                            df = pd.read_csv(gwscodes, sep=';', encoding='cp1250')
                            df = df[['GWSCOD', 'Lndgbrk', 'Grasbuffer']]
                            gdf['GWSCOD_H'] = gdf.GWSCOD_H.astype(int)
                            gdf = pd.merge(gdf, df, how='left', left_on='GWSCOD_H', right_on='GWSCOD')
                            gdf['Lndgbrk'] = np.where(gdf['Lndgbrk'].notnull(), gdf['Lndgbrk'], self.RasterProp['nodata'])
                        else:
                            prcCatch = prcCatch[:-4] + '_zonderLndgbrk.shp'
                            msg = 'gwscodes niet gekend, gwscod_h wordt op 9999 gezet voor elk perceel'
                            self.logger.warning(msg)
                            gdf['GWSCOD_H'] = 9999
                            gdf['Lndgbrk'] = self.RasterProp['nodata']
                            # gdf['RST_VAL'] = gdf.NR
                        gdf.to_file(prcCatch)
                        self.percelen[jaar] = prcCatch
                        CNWS.CreateSpatialIndex(self.percelen[jaar])
                else:
                    self.percelen[jaar] = None
            else:
                self.percelen[jaar] = prcCatch
        return


class Belgian_Scenario(CNWS.Scenario):
    """

    """
    def __init__(self, bekken, nr):
        CNWS.Scenario.__init__(self, bekken, nr)
        self.EBMSources = {}
        self.EBMDicts = {}
        self.BelgianOptions = {}
        return

    def SetEBMSources(self, Sources):
        MandatoryKeys = ['BO vast', 'BO var', 'Erosiebesluit', 'Provincie', 'RoodPaarsNtKerend', 'VormPercelen']

        for key in MandatoryKeys:
            try:
                self.EBMSources[key] = Sources[key]
            except KeyError:
                defaultvalue = 0

                msg = '%s is not given in the model options, default value (%s) is used' % (key, defaultvalue)
                self.logger.warning(msg)
                self.EBMSources[key] = defaultvalue
        return

    def SetBelgianOptions(self, Opt):
        MandatoryKeys = ['VerwijderAlleTeeltInfo', 'VerwijderEnkelAkkerTeelt']

        for key in MandatoryKeys:
            try:
                self.BelgianOptions[key] = Opt[key]
            except KeyError:
                defaultvalue = 0

                self.BelgianOptions[key] = defaultvalue

    def SetRoodPaarsOpNtKerend(self, jaar):
        if jaar >= 2014:
            gdf = gpd.read_file(self.percelenshp[jaar])
            if jaar in [2014, 2015]:
                sel = gdf['ERO_NAM'].isin(['zeer hoog', 'hoog'])
            else:
                sel =  gdf['ERO_NAM'].isin(['Paars', 'Rood'])

            sel = (sel) & (gdf['Lndgbrk'] != -4)

            gdf['ntkerend'] = np.where(sel, 1, 0)
            gdf.to_file(self.percelenshp[jaar])
        else:
            msg = 'Geen informatie over Paarse en rode percelen beschikbaar'
            self.logger.warning(msg)
        return

    def RemoveCropInfo(self, kind='EnkelAkker'):
        for jaar in self.jaren:
            gdf = gpd.read_file(self.percelenshp[jaar])
            if kind == 'EnkelAkker':
                msg = 'Verwijderen van teeltgegevens (akkers)'
                self.logger.info(msg)
                if jaar not in [2015, 2016, 2017]:
                    gdf.GWSCOD_H = np.where(gdf.Lndgbrk == -9999, 9999, gdf.GWSCOD_H)
                else:
                    gdf.GWSCOD_H = np.where(gdf.STAT_BGV == 'BG', gdf.GWSCOD_H, 9999)
            elif kind == 'All':
                msg = 'Verwijderen van (alle) teeltgegevens'
                self.logger.info(msg)
                gdf.GWSCOD_H = -9999
            else:
                msg = 'geen correcte optie gekozen!'
                raise CNWS.CNWSException(msg)
            gdf.to_file(self.percelenshp[jaar])
        return

    def PrepareEBM(self, b):
        for jaar in self.jaren:
            # preparing all EBM's to remove
            self.EBMDicts[jaar] = []
            self.catchm.ExtractGrasPercelen(jaar)
            self.catchm.Prepare_BO(b.GetBOvast(jaar), 'vast', jaar, b.GetBOInfo())
            self.catchm.Prepare_BO(b.GetBOvar(jaar), 'var', jaar, b.GetBOInfo())
            self.catchm.PrepareEBM_besluit(b.GetEBM_besluit(), jaar)
            if 'Vlaams Brabant' in self.catchm.Provincies:
                self.catchm.PrepareEBMVlBr(b.GetEBM_VlBr(), jaar)
            elif 'Oost-Vlaanderen' in self.catchm.Provincies:
                msg = 'EBM Oost-Vlaanderen not yet implemented!'
                self.logger.warning(msg)
                self.catchm.ebmProvincie[jaar] = {}
            else:
                msg = 'geen provinciale maatregelen gevonden'
                self.logger.info(msg)
                self.catchm.ebmProvincie[jaar] = {}

            self.EBMDicts[jaar].append(self.catchm.BOvast[jaar])
            self.EBMDicts[jaar].append(self.catchm.BOvar[jaar])
            self.EBMDicts[jaar].append(self.catchm.ebmBesluit[jaar])
            self.EBMDicts[jaar].append(self.catchm.ebmProvincie[jaar])
            if self.EBMSources['VormPercelen']:
                self.EBMDicts[jaar].append(self.catchm.grasstrookpercelen[jaar])

            self.MergeEBM(jaar, self.EBMDicts[jaar], ['gras'])
            if self.EBMOptions['UseGras'] == 1:
                self.RemoveKnownGrasStrips(jaar)
            else:
                self.ConvertGrasStrips2Fields(jaar)

            # setting all EBMs to User choices
            self.EBMDicts[jaar] = []
            if self.EBMSources['BO vast'] == 1:
                self.EBMDicts[jaar].append(self.catchm.BOvast[jaar])
            if self.EBMSources['BO var'] == 1:
                self.EBMDicts[jaar].append(self.catchm.BOvar[jaar])
            if self.EBMSources['Erosiebesluit'] == 1:
                self.EBMDicts[jaar].append(self.catchm.ebmBesluit[jaar])
            if self.EBMSources['Provincie'] == 1:
                self.EBMDicts[jaar].append(self.catchm.ebmProvincie[jaar])
            if self.EBMSources['VormPercelen'] == 1:
                self.EBMDicts[jaar].append(self.catchm.grasstrookpercelen[jaar])
            if self.ModelOptions['BankGrassStrips'] == 1:
                self.EBMDicts[jaar].append(self.BankGrassStrips)
            if self.EBMSources['RoodPaarsNtKerend'] == 1:
                self.SetRoodPaarsOpNtKerend(jaar)
                # self.EBMDicts[jaar].append(self.RoodPaarsNtKerend[jaar])
        return