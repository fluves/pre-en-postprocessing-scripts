__author__ = 'Daan Renders, Sacha Gobbeyn (Fluves)'
__maintainer__ = 'Daan Renders'
__email__ = 'daan@fluves.com'

# Standard libraries
import os
import subprocess
import datetime
import configparser
import shutil
import itertools
import logging
import sys
import pdb
from copy import deepcopy

# GIS and datahandling libraries
try:
    import xlrd
    import pandas as pd
    import numpy as np
    import geopandas as gpd
    import matplotlib.pyplot as plt
    from matplotlib import colors
    import fiona
    import rasterio
    from rasterio import features as rasteriofeatures
    import shapely
except ImportError as e:
    logging.error('not all necessary libraries are available for import!')
    logging.error(e)
    sys.exit()

logging.basicConfig(level=logging.ERROR, format='%(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

def GetItemFromIni(ini, section, option, Type):
    """
    Gets an item from an option in a certain section of an ini-file
    :param ini: path of the ini-file
    :param section: name of the desired section
    :param option: name of the desired option
    :param Type: type of parameter to be read (get (string), int, float or bool). if another string is giver to this
    parameter, a CNWSException is raised
    :return: None if the option or section does not exist in the ini-file or the value of the section-option
    """

    Cfg = configparser.ConfigParser()
    if os.path.isfile(ini):
        Cfg.read(ini)
        try:
            if Type == 'get':
                a = Cfg.get(section, option)
            elif Type == 'int':
                a = Cfg.getint(section, option)
            elif Type == 'float':
                a = Cfg.getfloat(section, option)
            elif Type == 'bool':
                a = Cfg.getboolean(section, option)
            else:
                raise CNWSException('not a correct Type')
        except configparser.NoOptionError:
            msg = 'Option %s does not exist in ini-file (section %s)' % (option, section)
            logger.warning(msg)
            return None
        except configparser.NoSectionError:
            msg = 'Section %s does not exist in ini-file' % section
            logger.warning(msg)
            return None
        else:
            return a
    else:
        raise CNWSException('%s does not exist' % ini)


def CheckRstDimensions(inRst, minmax, ncols, nrows, transform=None):
    """
    This function checks if the input raster has the desired dimensions.
    :param inRst: str, path to input raster
    :param minmax: list containing xmin, ymin, xmax, ymax
    :param ncols: int, number of columns in the raster
    :param nrows: int, number of rows in the raster
    :param transform, rasterio.transform-object (optional)
    :return: Bool
    """
    coordinates, transf, cols, rows = ReadRstParams(inRst)

    if cols != ncols:
        return False

    elif rows != nrows:
        return False

    elif minmax != coordinates:
        return False

    elif transform is not None:
        if transf != transform:
            return False
    else:
        return True


def ReadRstParams(inRst):
    """
    Reads all spatial dimensions of a raster
    :param inRst: path to the input raster
    :return: tuple: minmax (list), transf (rasterio transform object), cols (int), rows (int)
    """
    if os.path.exists(inRst):
        with rasterio.open(inRst) as src:
            cols = src.width
            rows = src.height
            xmin = src.bounds[0]
            ymin = src.bounds[1]
            xmax = src.bounds[2]
            ymax = src.bounds[3]
            transf = src.transform
            minmax = [xmin, ymin, xmax, ymax]
        return minmax, transf, cols, rows
    else:
        msg = 'Input raster does not exist!'
        raise CNWSException(msg)


def readRstAsArr(inRst):
    try:
        with rasterio.open(inRst) as src:
            arr = src.read(1)
    except rasterio.errors.RasterioIOError as e:
        logger.error(e)
        msg = 'could not open %s' % inRst
        raise CNWSException(msg)
    return arr


def write_arr_as_rst(arr, outrst, dtype, profile):
    mandatorykeys = ['driver', 'height', 'width', 'crs', 'transform', 'count', 'nodata', 'compress']
    for key in profile.keys():
        if key not in mandatorykeys:
            msg = 'not all mandatory keys in the profile are given!'
            raise CNWSException(msg)

    if not rasterio.dtypes.check_dtype(dtype):
        msg = 'no valid dtype chosen! %s' % dtype
        raise CNWSException(msg)

    if arr.shape[1] != profile['width'] or arr.shape[0] != profile['height']:
        msg = 'dimensions of array are not the same of the given profile'
        raise CNWSException(msg)

    with rasterio.open(outrst, 'w', dtype=dtype, **profile) as dst:
        dst.write(arr, 1)
    return


def execute_subprocess(cmd_str, msg=None):
    try:
        logger.debug(cmd_str)
        subprocess.check_call(cmd_str)
    except subprocess.CalledProcessError as e:
        logger.error(e.cmd)
        if msg is not None:
            raise CNWSException(msg)
        else:
            raise CNWSException(e.cmd)
    return


def GridDifference(inRst1, inRst2, OutRst):
    """
    Makes the difference between two grids
    :param inRst1: path to inputraster 1
    :param inRst2: path to inputraster 2
    :param OutRst: path to outputraster
    :return: nothing
    """
    cmd_str = 'saga_cmd grid_calculus 3 '
    cmd_str += '-A="%s" ' % inRst1
    cmd_str += '-B="%s" ' % inRst2
    cmd_str += '-C="%s"' % OutRst

    msg = 'Failed to make difference between %s and %s' % (inRst1, inRst2)
    execute_subprocess(cmd_str, msg)
    return


def GetFields(shp):
    """
    Gets a list of all fields in a shapefile
    :param shp: path to schapefile
    :return: list with fieldnames
    """
    try:
        with fiona.open(shp) as c:
            fields = c.schema['properties'].keys()
            return fields
    except IOError:
        logger.error('%s does not exist' % shp)
        return


def GetGeometryType(shp):
    """
    Gets the geometry type of a shapefile
    :param shp: path to shapefile
    :return: geometry type, string (see fiona documentation for all possibilities)
    """
    try:
        with fiona.open(shp) as c:
            geom = c.schema['geometry']
        return geom
    except IOError:
        logger.error('%s does not exist' % shp)
        return


def CopyRst(inRst, outRst):
    """
    Copies a raster and converts it to an idrisi-raster
    Uses gdal_translate
    :param inRst: input raster
    :param outRst: outputraster (extension must be .rst!)
    :return: Nothing
    """
    if os.path.isfile(outRst):
        delete_rst(outRst)

    cmd_str = 'gdal_translate -q -of RST "%s" "%s"' % (inRst, outRst)
    msg = 'Failed to copy %s' % inRst
    execute_subprocess(cmd_str, msg)
    return


def CopyShp(inShp, outShp):
    """
    Copies a shapefile to another location
    Uses ogr2ogr
    :param inShp: path + name of the shapefile to be copied
    :param outShp: path + name of the destination shapefile
    :return: nothing
    """
    cmd_str = 'ogr2ogr "%s" "%s"' % (inShp, outShp)
    msg = 'Failed to copy %s' % inShp
    execute_subprocess(cmd_str, msg)
    return


def Tiff2Rst(intif, outrst, Type):
    """
    Converts a GeoTif to an Idrisi RST
    Uses gdal_translate
    :param intif: str, path of the input tif
    :param outRst: str, path of the destination rst
    :param Type: str, type of the destination rst (integer or float)
    :return: nothing
    """

    msg = 'Failed to convert %s to .RST' % intif
    if Type == 'integer':
        cmd_str = 'gdal_translate -q -ot Int16 -of RST "%s" "%s"' % (intif, outrst)
        execute_subprocess(cmd_str, msg)
    elif Type == 'float':
        cmd_str = 'gdal_translate -q -ot Float32 -of RST "%s" "%s"' % (intif, outrst)
        execute_subprocess(cmd_str, msg)
    else:
        msg = 'Saving tif as rst. Type must be "integer" or "float"'
        logger.error(msg)
        raise CNWSException(msg)
    return


def GetFeatureCount(shp):
    """
    Counts the amount of features in a shapefile
    :param shp: path of the shapefile
    :return: int, number of features in the shapefile
    """
    with fiona.open(shp) as c:
        NrFeature = len(c)
    return NrFeature


def MergeShps(inList, outShp, inEPSG):
    """
    Merge a list of shapefiles to one shapefile
    Uses ogr2ogr
    :param inList: list, containing paths of all shapefiles to be merged
    :param outShp: path + name of the destination shapefile
    :param inEPSG: the EPSG code of the input shapefiles
    :return: nothing
    """
    for i, shp in enumerate(inList):
        if i == 0:
            cmd_str = 'ogr2ogr -a_srs %s -f "ESRI Shapefile" "%s" "%s"' % (inEPSG, outShp, shp)
        else:
            cmd_str = 'ogr2ogr -update -a_srs %s ' % inEPSG
            cmd_str += '-append "%s" "%s" -nln %s' % (outShp, shp, os.path.split(outShp)[1][:-4])

        msg = 'Failed to merge shapefiles'
        execute_subprocess(cmd_str, msg)
    return


def delete_rst(inrst):
    cmd_str = 'gdalmanage delete "%s"' % inrst
    msg = 'failed to delete %s' % inrst
    execute_subprocess(cmd_str, msg)
    return


def clipRst(inRst, outRst, Cnst, resampling='near'):
    """
    Clips a raster to a certain bounding box with a given resolution
    Uses gdalwarp
    :param inRst: path to in input raster
    :param outRst: path to the destination raster
    :param Cnst: a dictionary with following keys:
        - 'epsg': the EPSG-code of the inRst
        - 'res' (int, the resolution)
        - 'nodata' (int, the nodata flag)
        - 'minmax' (a list with xmin, ymin, xmax, ymax)
    :param resampling: bool
    :return: nothing
    """
    if os.path.isfile(outRst):
        delete_rst(outRst)

    logger.info('Clipping %s...' % os.path.split(inRst)[1])
    cmd_str = 'gdalwarp -q -s_srs %s ' % Cnst['epsg']
    cmd_str += '-t_srs %s ' % Cnst['epsg']
    cmd_str += '-te '
    for oor in Cnst['minmax']:
        cmd_str += '%s ' % str(oor)
    cmd_str += '-tr %s %s ' % (Cnst['res'], Cnst['res'])
    cmd_str += '-r %s ' % resampling
    cmd_str += '''"%s" "%s"''' % (inRst, outRst)

    msg = 'Failed to clip %s' % inRst
    execute_subprocess(cmd_str, msg)
    return


def GetExtent(shp):
    """
    Gets the bounding box coordinates of a shapefile
    :param shp: path of the input shapefile
    :return: tuple with xmin, ymin, xmax, ymax
    """
    with fiona.open(shp) as c:
        extent = c.bounds
    return extent  # xmin, ymin, xmax, ymax


def clipShp(inShp, outShp, clipShp):
    """
    Clip a shapefile by another shapefile
    Uses ogr2ogr
    :param inShp: the shapefile to be clipped
    :param outShp: the destination shapefile
    :param clipShp: the clip boundary
    :return: nothing
    """
    logger.info('Clipping %s...' % os.path.basename(inShp))
    ext = GetExtent(clipShp)
    cmd_str = 'ogr2ogr -spat %s %s %s %s ' % (ext[0], ext[1], ext[2], ext[3])
    cmd_str += '-skipfailures '
    cmd_str += '-clipsrc "%s" "%s" "%s"' % (clipShp, outShp, inShp)
    msg = 'Failed to clip %s' % inShp
    execute_subprocess(cmd_str, msg)
    return


def Shp2Rst_field(inShp, outRst, Cnst, field, allTouched=True):
    """
    Rasterizes a shapefile by a given attribute field
    Uses gdal_rasterize
    :param inShp: the shapefile to be rasterized
    :param outRst: the destination rst
    :param Cnst: a dictionary with following keys:
        - 'res' (int, the resolution)
        - 'nodata' (int, the nodata flag)
        - 'minmax' (a list with xmin, ymin, xmax, ymax)
    :param field: the field of the shapefile containing the values for the raster
    :param allTouched: bool, default true
    :return: nothing
    """
    if os.path.isfile(outRst):
        delete_rst(outRst)

    cmd_str = 'gdal_rasterize -q -a_nodata %s ' % Cnst['nodata']
    if allTouched:
        cmd_str += '-at '
    cmd_str += '-a %s ' % field
    cmd_str += '-l %s ' % os.path.basename(inShp)[:-4]
    cmd_str += '-of GTiff '
    cmd_str += '-te '
    for oor in Cnst['minmax']:
        cmd_str += '%s ' % str(oor)
    cmd_str += '-tr %s %s ' % (Cnst['res'], Cnst['res'])
    cmd_str += '"%s" "%s"' % (inShp, outRst[:-4]+'.tif')
    msg = 'Failed to rasterize %s' % inShp
    execute_subprocess(cmd_str, msg)
    return


def Shp2Rst_val(inShp, outRst, rstval, Cnst, allTouched=True):
    """
    Rasterizes a shapefile by a given constant value
    Uses gdal_rasterize
    :param inShp: the shapefile to be rasterized
    :param outRst: the destination rst
    :param rstval: the value all features of the shapefile will get in the raster
    :param Cnst: a dictionary with following keys:
        - 'res' (int, the resolution)
        - 'nodata' (int, the nodata flag)
        - 'minmax' (a list with xmin, ymin, xmax, ymax)
    :param allTouched, bool, default True
    :return: nothing
    """
    if os.path.isfile(outRst):
        delete_rst(outRst)

    cmd_str = 'gdal_rasterize -q -a_nodata %s ' % Cnst['nodata']
    if allTouched:
        cmd_str += '-at '
    cmd_str += '-burn %s ' % rstval
    cmd_str += '-l %s ' % os.path.basename(inShp)[:-4]
    cmd_str += '-of GTiff '
    cmd_str += '-te '
    for oor in Cnst['minmax']:
        cmd_str += '%s ' % str(oor)
    cmd_str += '-tr %s %s ' % (Cnst['res'], Cnst['res'])
    cmd_str += '"%s" "%s"' % (inShp, outRst[:-4]+'.tif')

    msg = 'Failed to rasterize %s' % inShp
    execute_subprocess(cmd_str, msg)
    return


def reclassRst(inRst, outRst, reclassdf):
    """
    Reclass of a raster based on pandas dataframe
    Uses saga_cmd
    :param inRst: input raster
    :param outRst: output raster (.sdat)
    :param reclassdf: pandas dataframe with current values (RST_VAL) en new value (NEWVAL)
    :return: nothing
    """
    reclassdf['MAX'] = reclassdf['RST_VAL']+1
    csv_naam = outRst[:-5] + '_reclasstable.csv'
    reclassdf[['RST_VAL', 'MAX', 'NEWVAL']].to_csv(csv_naam, index=False)
    cmd_str = 'saga_cmd -f=s grid_tools 15 -INPUT=%s ' % inRst
    cmd_str += '-RESULT=%s ' % outRst
    cmd_str += '-METHOD=3 '
    cmd_str += '-RETAB_2=%s ' % csv_naam
    cmd_str += '-F_MIN=RST_VAL '
    cmd_str += '-F_MAX=MAX '
    cmd_str += '-F_CODE=NEWVAL '
    cmd_str += '-TOPERATOR=0 '
    cmd_str += '-RESULT_NODATA_CHOICE=0'

    msg = 'Failed to reclassify %s' % inRst
    execute_subprocess(cmd_str, msg)
    return


def Lines2Points(inShp, pointShp, distance):
    """
    Converts a shapefile with lines to a shapefile with points.
    Uses saga
    :param inShp: the path to the input shapefile with lines
    :param pointShp: the path to the output shapefile with points
    :param distance: the distance between two points
    :return: nothing
    """
    cmd_str = 'saga_cmd -f=s shapes_points 5 '
    cmd_str += '-LINES "%s" ' % inShp
    cmd_str += '-POINTS "%s" ' % pointShp
    cmd_str += '-ADD 1 '
    cmd_str += '-METHOD_INSERT 0 '
    cmd_str += '-DIST %s ' % distance
    cmd_str += '-ADD_POINT_ORDER 1'

    msg = 'failed to convert lines to points'
    execute_subprocess(cmd_str, msg)
    return


def CreateSpatialIndex(inshp):
    """
    Creates a qix-file for a given shapefile
    :param inshp: input shapefile
    :return: nothing
    """
    if os.path.isfile(inshp):
        cmd_str = 'ogrinfo "%s" ' % inshp
        cmd_str += '-sql "CREATE SPATIAL INDEX ON %s"' % os.path.split(inshp)[1][:-4]

        msg = 'failed to convert lines to points'
        execute_subprocess(cmd_str, msg)
    else:
        msg = "Shapefile does not exist! (%s)" % inshp
        logger.error(msg)
        raise CNWSException(msg)
    return


def loadRaster(fname, flist=False):
    """
    load rasters from fname

    Input:
        fname [string]: name of file, .rst raster

    Output:
        raster [numpy array]: nrows*ncols raster
        praster [pandas dataframe]: list format of raster in x and y's and accompanying cell value
        profile: rasterio object holding properties image
    ------------
    """
    # load
    raster = {}
    with rasterio.open(fname) as src:
        raster = src.read()[0]
        profile = src.profile

    if flist:
        lraster = rasterToList(raster, profile)
    else:
        lraster = None
    return raster, lraster, profile


def writeRaster(raster, fname, profile, dfcol='', dfformat=np.float32):
    # if raster is pandas dataframe list object, then transform back
    if isinstance(raster, pd.DataFrame):
        raster = listToRaster(raster,profile,dfcol,dfformat)
    with rasterio.open(fname, 'w', **profile) as dst:
        dst.write(raster, 1)


def rasterToList(raster, profile):
    nrows = profile["height"]
    ncols = profile["width"]
    # convert to list
    lraster = np.empty([nrows * ncols, 3], dtype=np.float32)
    lraster[:, 0] = raster.flatten()
    lraster[:, 2] = np.tile(np.arange(1, ncols + 1, 1), nrows)
    lraster[:, 1] = np.repeat(np.arange(1, nrows + 1, 1), ncols)
    lraster = pd.DataFrame(lraster, columns=['val', 'row', 'col'])

    return lraster

def listToRaster(raster, profile,dfcol,dfformat):

    nrows = profile["height"]
    ncols = profile["width"]
    raster[dfcol] = raster[dfcol].astype(dfformat)
    raster = raster.sort_values(["row", "col"])
    raster = np.reshape(raster[dfcol].values, (nrows, ncols))
    return raster


def pickleDump(instance, fname):
    try:
        import cPickle as pickle
    except ModuleNotFoundError:
        import pickle
    f = open(fname,"wb")
    pickle.dump(instance,f)
    f.close()

def pickleLoad(fname):
    try:
        import cPickle as pickle
    except ModuleNotFoundError:
        import pickle
    pkl_file = open(fname,"rb")
    instance = pickle.load(pkl_file)
    pkl_file.close()
    return instance

class CNWSException(Exception):
    """
    Exception from CNWS pre- and postprocessing scripts
    """


class Catchment:

    def __init__(self, naam, shp):
        """
        Construct a new Bekken object
        :param naam: string, name of the catchment
        :param shp: string, path of the shapefile containing the polygon delimiting the catchment
        """
        self.naam = str(naam)
        self.logger = logging.getLogger(__name__)
        
        try:
            c = fiona.open(shp)
            c.close()
        except IOError as e:
            self.logger.exception('I/O ERROR: {0} ({1})'.format(e.strerror, shp))
            msg = 'could not open shapefile of catchment'
            raise CNWSException(msg)
        else:
            self.shp = shp
            
        self.folder = None
        self.shpfolder = None
        self.rstfolder = None

        self.RasterProp = {}
        self.rstparams = None
        self.binarr = None
                
        self.K = None
        self.P = None
        self.DTM = None
        self.LandUse = None
        self.CNbodem = None

        self.infrpoly = None
        self.infrline = None
        self.infrTif = None

        self.waterLine = None
        self.waterLinePoints = None
        self.adj_edges = None
        self.up_egdes = None
        self.waterPoly = None

        self.segmentkrt = None
        self.waterlopenkrt = None
        self.riverroutingkrt = None

        self.percelen = {}
        self.grasstrookpercelen = {}

        self.ini_bekken = None

    def CreateBekkenIni(self):
        """
        Creates an ini-file for the catchment
        sets the self.ini_bekken argument

        - experimental - not yet in use
        :return: Nothing
        """

        self.logger.info('Creating ini-file catchment...')
        self.ini_bekken = os.path.join(self.folder, 'ini_%s.ini' % self.naam)
        Cfg = configparser.ConfigParser()

        f = open(self.ini_bekken, 'w')
        Cfg.write(f)
        f.close()
        return

    def GetCatchmentArea(self):
        """
        Calculates the total area of the catchment
        :return: the total area of the catchment (float)
        """
        gdf = gpd.read_file(self.shp)
        return gdf.area.sum()
    
    def GetLengthRiverSegm(self):
        """
        Calculates the total length of all riversegments in the catchment
        :return: the total length of the riviersegments in the catchment (float)
        """
        gdf = gpd.read_file(self.waterLine)
        return gdf.length.sum()

    def GetParcelsArea(self, year):
        """
        Calculates the total area of all agricultural fields in the catchment in the given year
        :param year: the year to be checked
        :return:
        """
        gdf = gpd.read_file(self.percelen[year])
        return gdf.area.sum()

    def SetConstants(self, nodata=-9999, epsg='EPSG:31370', res=20):
        """
        Sets spatial constants
        :param nodata: the nodata-value (optional, int), default = -9999
        :param epsg: the epsg-code (optional, str), default = 'EPSG:31370'
        :param res: the resolution (optional, int), default = 20
        :return: nothing
        """
        self.RasterProp['nodata'] = nodata
        self.RasterProp['epsg'] = epsg
        self.RasterProp['res'] = res
        return

    def PrepareRasterProperties(self, dtm):
        """
        Determines, based on the extent of the catchment and the grid-system of the DEM,
        the coordinates of the corners of the rasters needed for the CNWS-model, the amount of rows and columns
        :param dtm: the input digital elevation model to be used in the model calculations
        :return: nothing
        """
        self.logger.info('Voorbereiden raster properties...')
        # Read the extent and resolution of the dtm
        with rasterio.open(dtm) as ds:
            bb = [ds.bounds.left, ds.bounds.bottom, ds.bounds.right, ds.bounds.top]

        self.ReadExtentCatchm(bb=bb)

        return

    def ReadExtentCatchm(self,bb=None,fmap=None):

        #(SG) check of er een dump van de RasterProp aanwezig is in de mappenstructuur

        if fmap!=None:
            self.RasterProp = pickleLoad(os.path.join(fmap,"RasterProp.pkl"))
        else:
            # Read the extent of the catchment
            with fiona.open(self.shp) as c:
                Nr = len(c)
                if Nr == 0:
                    raise CNWSException('ERROR: no polygons in catchment shapefile')
                elif Nr > 1:
                    raise CNWSException('ERROR: multiple polygons in catchment shapefile')
                else:
                    # Get Spatial Extent of catchment
                    extent = c.bounds
                    minmax = [round(extent[0])-100,round(extent[1])-100,round(extent[2])+100,round(extent[3])+100]  # xmin ymin xmax ymax

                    # snapping van minmax aan rastercellen van dtm
                    # xmin en ymin

                    for i in range(4):
                        if i < 2: # xmin en ymin
                            rest = ((minmax[i] - bb[i])%self.RasterProp['res'])
                            if rest != 0:
                                minmax[i] = minmax[i] - rest
                        else: # xmax en ymax
                            rest = ((minmax[i] - bb[i-2])%self.RasterProp['res'])
                            if rest != 0:
                                minmax[i] = minmax[i] - rest + self.RasterProp['res']

                    self.RasterProp['minmax'] = minmax
                    self.RasterProp['ncols'] = int(round((minmax[2]-minmax[0])/self.RasterProp['res']))
                    self.RasterProp['nrows'] = int(round((minmax[3]-minmax[1])/self.RasterProp['res']))

                    self.rstparams = {}
                    self.rstparams['driver'] = 'GTiff'
                    self.rstparams['height'] = int(round((minmax[3]-minmax[1])/self.RasterProp['res']))
                    self.rstparams['width'] = int(round((minmax[2]-minmax[0])/self.RasterProp['res']))
                    self.rstparams['crs'] = self.RasterProp['epsg']
                    self.rstparams['transform'] = rasterio.Affine(self.RasterProp['res'], 0, minmax[0],
                                                    0, -self.RasterProp['res'], minmax[3])
                    self.rstparams['count'] = 1
                    self.rstparams['nodata'] = self.RasterProp['nodata']
                    self.rstparams['compress'] = "DEFLATE"
                    # self.rstparams['minmax'] = minmax
        return

    def setdata(self, waterLine=None, waterPoly=None, infrLine=None, infrPoly=None, parcels={}):
        """
        This function sets some arguments of the catchmentclass. It also checks if the inputdata is valid for the argument
        If one of the inputparameters does not comply with the expected format, a CNWSException is raised
        :param waterLine: a shapefile with lines representing the river segments
        :param waterPoly: a shapefile with polygons representing the water bodies (not used yet)
        :param infrLine: a shapefile with lines representing the road network
        :param infrPoly: a shapefile with polygons representing infrastructure (roads, buildings,...)
        :param parcels: a dictionary with keys representing a year and the arguments a shapefile with polygons representing the parcels
        :return: nothing
        """
        if waterLine is not None:
            if GetGeometryType(waterLine) != 'LineString':
                msg = 'File with river lines does not contain LineStrings, but %s!' % GetGeometryType(waterLine)
                raise CNWSException(msg)
            else:
                self.waterLine = waterLine

        if waterPoly is not None:
            geom = GetGeometryType(waterPoly)
            if geom != 'Polygon':
                msg = 'File with water polygon does not contain polygons, but %s!' % geom
                raise CNWSException(msg)
            else:
                self.waterPoly = waterPoly

        if infrLine is not None:
            geom = GetGeometryType(infrLine)
            if geom != 'LineString':
                msg = 'File with infrastructure lines does not contain LineStrings, but %s!' % geom
                raise CNWSException(msg)
            else:
                self.infrline = infrLine

        if infrPoly is not None:
            geom = GetGeometryType(infrPoly)
            if geom != 'Polygon':
                msg = 'File with infrastructure polygons does not contain polygons, but %s!' % geom
                raise CNWSException(msg)
            else:
                self.infrpoly = infrPoly

        if len(parcels.keys()) != 0:
            for key in parcels.keys():
                geom = GetGeometryType(parcels[key])
                if geom != 'Polygon':
                    msg = 'File with agriculture parcels does not contain polygons, but %s!' % geom
                    raise CNWSException(msg)
                else:
                    self.percelen = parcels
        return

    def PrepareWaterShps(self):
        """
        Numbers every linesegment of the rivers (1 to n, int, attribute 'NR') and converts the lines to points
        Every point is numbered (1 to n, attribute 'PTNR'). The points are stored in self.waterLinePoints
        :return: Nothing
        """
        self.logger.info('Voorbereiden waterlopen...')

        if self.waterLine is not None:
            # nummering segmenten toevoegen
            self.logger.info('Toevoegen segmentnummering...')
            try:
                gdf = gpd.read_file(self.waterLine)
            except Exception:
                msg = 'could not open %s ' %self.waterLine
                self.waterLine = None
            else:
                if gdf.shape[0] != 0:
                    if 'NR' not in gdf.columns:
                        gdf['NR'] = range(1, gdf.shape[0]+1)
                        gdf.to_file(self.waterLine)
                else:
                    self.waterLine = None

        if self.waterLinePoints is None:
            if self.waterLine is not None:
                self.waterLinePoints = self.waterLine[:-4] + '_points.shp'
                self.waterLinePoints = os.path.split(self.waterLinePoints)[1]
                self.waterLinePoints = os.path.join(self.shpfolder, self.waterLinePoints)

                if not os.path.exists(self.waterLinePoints):
                    Lines2Points(self.waterLine, self.waterLinePoints, self.RasterProp['res'] / 2)
                    gdf = gpd.read_file(self.waterLinePoints)
                    gdf['PTNR'] = range(1, gdf.shape[0] + 1)
                    gdf.to_file(self.waterLinePoints)

        return

    def PrepareWaterRsts(self):
        """
        Creates thee rasters with river pixels
        - River segment map (self.segmentkrt), every segment gets a different id (attribute 'NR')
        - River routing map (self.riverroutingkrt), every pixel gets a different id, highest id is the most downstream
        pixel of a segment
        - River map (self.waterlopenkrt), all river pixels get value -1
        :return nothing
        """
        self.logger.info('Aanmaken waterlopenrasters...')
        os.chdir(self.rstfolder)
        self.segmentkrt = os.path.join(self.rstfolder, 'Riversegmentmap_%s.rst' % self.naam)
        segm_temp = os.path.join(self.rstfolder, 'Riversegmentmap_temp_%s.tif' % self.naam)
        segm_temp2 = os.path.join(self.rstfolder, 'Riversegmentmap_%s.tif' % self.naam)

        self.waterlopenkrt = os.path.join(self.rstfolder, 'Waterlopen_%s.tif' % self.naam)

        routing_temp = os.path.join(self.rstfolder, 'Rivierrouting_temp_%s.tif' % self.naam)
        routing_temp2 = os.path.join(self.rstfolder, 'Rivierrouting_%s.tif' % self.naam)
        self.riverroutingkrt = os.path.join(self.rstfolder, 'Rivierrouting_%s.rst' % self.naam)

        if self.waterLinePoints is not None:
            if not os.path.exists(self.segmentkrt):
                # verrasteren van segmenten
                Shp2Rst_field(self.waterLinePoints, segm_temp, self.RasterProp, 'NR', allTouched=False)
                arr_lijn = readRstAsArr(segm_temp)
                Segmentmap = arr_lijn.copy()
                Segmentmap = np.where(Segmentmap == self.rstparams['nodata'], 0, Segmentmap)

                # waterlopenmap
                Waterlopenmap = arr_lijn.copy()
                Waterlopenmap = np.where(Waterlopenmap != self.rstparams['nodata'], -1, Waterlopenmap).copy()

                write_arr_as_rst(Waterlopenmap, self.waterlopenkrt, 'float64', self.rstparams)
                write_arr_as_rst(Segmentmap, segm_temp2, 'float64', self.rstparams)

                Tiff2Rst(segm_temp2, self.segmentkrt, 'integer')
                delete_rst(segm_temp)

                # verrasteren van routing
                Shp2Rst_field(self.waterLinePoints, routing_temp, self.RasterProp, 'PTNR', allTouched=False)
                arr_routing = readRstAsArr(routing_temp)
                routingmap = arr_routing.copy()
                routingmap = np.where(routingmap == self.rstparams['nodata'], 0, routingmap)

                write_arr_as_rst(routingmap, routing_temp2, 'float64', self.rstparams)
                Tiff2Rst(routing_temp2, self.riverroutingkrt, 'integer')
        else:
            self.segmentkrt = None
            self.waterlopenkrt = None
            self.riverroutingkrt = None
        return

    def TopologizeRivers(self):
        """
        Creates text files with topology of river segments. Sets the attributes:
        - self.adj_edges
        - self.up_edges
        :return:
        """
        self.logger.info('preparing river topology...')
        if self.waterLine is not None:
            topolwaterLine = self.waterLine[:-4] + '_topology.shp'

            topolwaterLine = os.path.split(topolwaterLine)[1]
            topolwaterLine = os.path.join(self.shpfolder, topolwaterLine)

            if not os.path.isfile(topolwaterLine):
                cmd_str = 'saga_cmd -f=s topology 0 '
                cmd_str += '-INPUTLINES "%s" ' % self.waterLine
                cmd_str += '-OUTPUTLINES "%s" ' % topolwaterLine
                msg = 'failed to topologize rivers part 1'
                execute_subprocess(cmd_str, msg)

            self.waterLine = topolwaterLine

            self.adj_edges = os.path.split(self.waterLine)[1][:-4] + '_adj_edges.txt'
            self.adj_edges = os.path.join(self.shpfolder, self.adj_edges)
            self.up_edges = os.path.split(self.waterLine)[1][:-4] + '_up_edges.txt'
            self.up_edges = os.path.join(self.shpfolder, self.up_edges)

            if not os.path.isfile(self.adj_edges) or not os.path.isfile(self.up_edges):
                cmd_str = 'saga_cmd -f=s topology 1 '
                cmd_str += '-INPUTLINES "%s" ' % topolwaterLine
                cmd_str += '-ADJECTANT_EDGES "%s" ' % self.adj_edges
                cmd_str += '-UPSTREAM_EDGES "%s" ' % self.up_edges
                cmd_str += '-ZERO_BASED=0'
                msg = 'failed to topologize rivers part 2'
                execute_subprocess(cmd_str, msg)
        return

    def CreateInfraTif(self):
        """
        Creates infrastructuur.tif with all infrastructures != nodata
        value -7: non paved road (only if 'paved' attribute is present in self.infrline)
        value -2: infrastructure
        :return: nothing
        """
        self.logger.info('Aanmaken infrastructuurraster...')
        os.chdir(self.rstfolder)
        if self.infrline is not None or self.infrpoly is not None:
            self.infrTif = 'infrastructuur_%s.tif' % self.naam
            self.infrTif = os.path.join(self.rstfolder, self.infrTif)

            if not os.path.exists(self.infrTif):
                with rasterio.open(self.infrTif, 'w', dtype="float64", **self.rstparams) as out:
                    out_arr = out.read(1)
                    if self.infrline is not None:
                        gdf_line = gpd.read_file(self.infrline)
                        if not 'paved' in gdf_line.columns:
                            gdf_line['paved'] = -2
                        shapes = ((geom, paved) for geom, paved in zip(gdf_line.geometry, gdf_line.paved))
                    else:
                        shapes = None
                        
                    if self.infrpoly is not None and GetFeatureCount(self.infrpoly) != 0:
                        gdf_poly = gpd.read_file(self.infrpoly)
                        shapes_poly = ((geom, -2) for geom in gdf_poly.geometry)
                        if shapes is not None:
                            shapes = itertools.chain(shapes, shapes_poly)
                        else:
                            shapes = shapes_poly
                    
                    if shapes is not None:
                        burned = rasteriofeatures.rasterize(shapes=shapes, all_touched=True,
                                                    out=out_arr, transform=out.transform)
                        out.write_band(1, burned)                    
        return

    def ExtractGrasPercelen(self, jaar):
        """
        Haalt uit de percelenshapefile van een jaar de grasstroken op basis van
        het Thinnes criterium (>0.3).

        EXPERIMENTAL - NOT IN USE YET

        :param jaar:
        :return: nothing
        """
        workdir = os.path.join(self.shpfolder, str(jaar))
        grasstroken = 'graspercelen_%s_%s.shp' % (jaar, self.naam)
        grasstroken = os.path.join(workdir, grasstroken)
        d = {}

        try:
            gdf = gpd.read_file(self.percelen[jaar])
        except Exception:
            self.grasstrookpercelen[jaar] = d
            msg = 'could not open %s ' % self.percelen[jaar]
        else:
            gdf = gdf.loc[gdf['Grasbuffer'] == 1]
            gdf['opp'] = gdf.area
            gdf['perimeter'] = gdf.length

            gdf = gdf.loc[gdf.length ** 2 >= 16 * gdf.area]
            gdf['BREEDTE'] = (gdf.length - np.sqrt((gdf.length ** 2) - (16 * gdf.area))) / 4

            gdf['Aspect'] = (gdf.BREEDTE**2)/gdf.area
            gdf['Thinnes'] = 4*np.pi*gdf.area/(gdf.length**2)
            gdf = gdf.loc[gdf['Thinnes'] < 0.5]
            gdf = gdf.loc[gdf['BREEDTE'] < 25]
            gdf = gdf.loc[gdf['Aspect'] < 0.25]
            if gdf.shape[0] != 0:
                d['gras'] = grasstroken
                gdf.to_file(grasstroken)
            self.grasstrookpercelen[jaar] = d
        return

    def PrepareP(self):
        """
        Creates a binary raster (self.P) and binary array (self.binarr) of the catchment.
        """
        self.logger.info('Aanmaken P-factor raster...')
        os.chdir(self.rstfolder)

        self.P = 'P_%s.rst' % self.naam
        self.P = os.path.join(self.rstfolder, self.P)
        
        Ptemp = 'P_%s.tif' % self.naam
        if not os.path.isfile(Ptemp):
            rstprop = self.RasterProp.copy()
            rstprop['nodata'] = 0
            Shp2Rst_val(self.shp, Ptemp, 1, rstprop)  # raster met 1 en 0
        if not os.path.isfile(self.P):
            Tiff2Rst(Ptemp, self.P, 'float')

        self.binarr = readRstAsArr(Ptemp)
        return

    def PrepareDTM(self, dtm):
        """
        Clips the DEM and converts it to an idrisi-raster
        Sets self.DTM
        :param dtm: the path to the input DEM
        :return: nothing
        """
        if dtm is not None:
            os.chdir(self.rstfolder)

            dtm_naam = os.path.split(dtm)[1][:-4] + '_%s' % self.naam

            self.DTM = os.path.join(self.rstfolder, dtm_naam + '.rst')
            dtm_tif = dtm_naam + '.tif'

            if not os.path.isfile(self.DTM):
                clipRst(dtm, dtm_tif, self.RasterProp)
                Tiff2Rst(dtm_tif, self.DTM, 'float')

        else:
            msg = 'ERROR: No dhm available for model!'
            raise CNWSException(msg)
        return

    def PrepareK(self, Kfact):
        """
        Clips the input Kfactor map, converts it to idrisi-raster in the correct grid-system and sets self.K
        :param Kfact: path to input map
        :return: nothing
        """
        if Kfact is not None:
            os.chdir(self.rstfolder)

            Knaam = os.path.split(Kfact)[1][:-4] + '_%s' % self.naam
            self.K = os.path.join(self.rstfolder, Knaam + '.rst')
            K_tif = Knaam + '.tif'
            if not os.path.isfile(self.K):
                clipRst(Kfact, K_tif, self.RasterProp, resampling='mode')
                arr = readRstAsArr(K_tif)
                arr = np.where(self.binarr == 1, arr, self.RasterProp['nodata']).astype('int32')
                write_arr_as_rst(arr, K_tif, 'int32', self.rstparams)
                Tiff2Rst(K_tif, self.K, 'integer')
        else:
            msg = 'ERROR: No K-factor map available for model!'
            raise CNWSException(msg)
        return

    def PrepareLandGebruik(self, landuse):
        """
        Clips and converts landuse map to the desired grid system. Sets the self.LandUse argument
        :param landuse: path to the input raster
        :return: nothing
        """
        if landuse is not None:
            os.chdir(self.rstfolder)

            self.LandUse = os.path.split(landuse)[1][:-4] + '_%s.tif' % self.naam
            self.LandUse = os.path.join(self.rstfolder, self.LandUse)

            if not os.path.isfile(self.LandUse):
                clipRst(landuse, self.LandUse, self.RasterProp, resampling='mode')
                # buiten modelgebied op nodata zetten
                arr = readRstAsArr(self.LandUse)
                arr = np.where(self.binarr == 1, arr, self.RasterProp['nodata'])
                write_arr_as_rst(arr, self.LandUse, 'int16', self.rstparams)
        return

    def prep_CNsoil(self, CNbodem_source):
        """
        Clips and converts CN soil map to the desired grid system. Sets the self.CNbodem argument
        :param CNbodem_source: path to the input raster
        :return: nothing
        """
        if CNbodem_source is not None:
            os.chdir(self.rstfolder)

            self.CNbodem = os.path.split(CNbodem_source)[1][:-4] + '_%s.tif' % self.naam
            self.CNbodem = os.path.join(self.rstfolder, self.CNbodem)
            if not os.path.isfile(self.CNbodem):
                clipRst(CNbodem_source, self.CNbodem, self.RasterProp, resampling='mode')
        else:
            msg = 'ERROR: No CN-soil map available for model!'
            raise CNWSException(msg)
        return

    def CreateFolderStructure(self, homefolder, years):
        """
        Creates a fixed folder structure for the modeldata:
        Homefolder
            - bekkenfolder
                - Rsts
                - Shps
                - year1
                - year2
        :param homefolder: the folder where the folder structur for the catchmen must be made
        :param years: a list with years (int)
        """
        self.logger.info('Aanmaken folderstructuur bekken...')
        if not os.path.exists(homefolder):
            os.makedirs(homefolder)

        self.folder = os.path.join(homefolder, self.naam)
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)
        else:
            self.logger.warning('bekken bestaat al!')

        basedata = os.path.join(self.folder, 'Data_Bekken')
        if not os.path.exists(basedata):
            os.makedirs(basedata)

        self.rstfolder = os.path.join(basedata, 'Rsts')
        if not os.path.exists(self.rstfolder):
            os.makedirs(self.rstfolder)
        
        self.shpfolder = os.path.join(basedata, 'Shps')
        if not os.path.exists(self.shpfolder):
            os.makedirs(self.shpfolder)

        for year in years:
            if not os.path.exists(os.path.join(self.shpfolder, str(year))):
                os.makedirs(os.path.join(self.shpfolder, str(year)))

        return


class Scenario:
    """
    Scenario-class contains all tools to make the necessary inputdata for a scenario or modelrun
    """

    def __init__(self, catchm, nr):
        """
        Construct a new Scenario object.
        :param catchm: Catchment object
        :param nr: number of the scenario
        """

        self.catchm = catchm
        self.nr = nr

        self.logger = logging.getLogger(__name__)

        self.sfolder = None
        self.infolder = None
        self.outfolder = None
        self.ppfolder = None

        self.versie = None
        self.EBMOptions = {}
        self.ModelOptions = {}
        self.Variables = {}
        self.Output = {}

        self.jaren = []
        self.seizoenen = [[]]
        self.grasshp = {}
        self.grasrst = {}
        self.buffershp = None
        self.grachtenshp = None
        self.gdammenshp = None
        self.teelttechnshp = {}
        self.percelenshp = {}
        self.poelenshp = None
        self.BankGrassStrips = None

        self.ExtraData = {}

        self.forcedroutingdata = None

        self.prckrt = {}
        self.prcrst = {}
        self.prctable = {}
        self.perceelsdf = {}
        self.Ckaarten = {}
        self.CNkaarten = {}
        self.Pkaart = None
        self.Kkaart = None
        self.bufferkaart = None
        self.bufferData = None
        self.DTMkaart = None
        self.riviersegmkrt = None
        self.grachtenkaart = None
        self.gdammenkaart = None

        self.riverrouting = None
        self.up_edges = None
        self.adj_edges = None

        self.outletmap = None
        self.ini = None

    def CreateFolderStructure(self):
        """
        Creates a folder structure for a scenario whithin the catchment-folder.
        Catchment
            - Scenario_(self.nr)
                - year 1
                - year 2
                - modelinput
                - modeloutput
                - postprocessing

        If a folder with the scenario-number already exists within the catchment folder, the function will look for the
        next integer scenario number wich does not yet exist.

        The function also copies the DEM, K-factor, P-factor and segment map (if needed) to the modelinput folder.
        :return: nothing
        """
        if self.catchm.folder is None:
            msg = 'ERROR: No bekken-folder was found'
            raise CNWSException(msg)
        
        homefolder = self.catchm.folder
        self.sfolder = os.path.join(homefolder, 'scenario_%s' % self.nr)

        # als scenarionr al bestaat, zoeken naar hoogste nr en dat+1 doen voor nieuw scenarionr
        if os.path.exists(self.sfolder):
            imax = 0
            self.logger.warning('scenario %s bestaat al' % self.nr)
            for name in os.listdir(homefolder):
                path = os.path.join(homefolder, name)
                if os.path.isdir(path) and name.startswith('scenario_'):
                    i = int(name[9:])  # nr van een scenario dat al bestaat
                    if i > imax:
                        imax = i
            self.logger.warning('scenarionummer wordt veranderd in %s' % str(imax + 1))
            self.nr = imax + 1
            self.sfolder = os.path.join(homefolder, 'scenario_%s' % self.nr)

        os.makedirs(self.sfolder)

        for i, jaar in enumerate(self.jaren):
            jaarfolder = os.path.join(self.sfolder, str(jaar))
            os.makedirs(jaarfolder)
            # Kopieren van perceelsinformatie van bekkendata naar scenariofolder
            if self.catchm.percelen[jaar] is not None:
                self.percelenshp[jaar] = 'percelen_%s_scenario_%s_%s.shp' % (self.catchm.naam, str(self.nr), str(jaar))
                self.percelenshp[jaar] = os.path.join(jaarfolder, self.percelenshp[jaar])
                CopyShp(self.percelenshp[jaar], self.catchm.percelen[jaar])
            else:
                self.percelenshp[jaar] = None

        self.infolder = os.path.join(self.sfolder, 'modelinput')
        os.makedirs(self.infolder)
        self.outfolder = os.path.join(self.sfolder, 'modeloutput')
        os.makedirs(self.outfolder)
        self.ppfolder = os.path.join(self.sfolder, 'postprocessing')
        os.makedirs(self.ppfolder)
        
        # kopieren van K en DTM naar modelinputfolder
        if self.catchm.K is not None:
            self.Kkaart = os.path.split(self.catchm.K)[1]
            self.Kkaart = os.path.join(self.infolder, self.Kkaart)
            CopyRst(self.catchm.K, self.Kkaart)
        else:
            self.logger.warning('K-kaart bekken is niet aangemaakt')
 
        if self.catchm.DTM is not None:
            self.DTMkaart = os.path.split(self.catchm.DTM)[1]
            self.DTMkaart = os.path.join(self.infolder, self.DTMkaart)
            CopyRst(self.catchm.DTM, self.DTMkaart)
        else:
            self.logger.warning('DTM bekken is niet aangemaakt')

        if self.catchm.P is not None:
            self.Pkaart = os.path.split(self.catchm.P)[1]
            self.Pkaart = os.path.join(self.infolder, self.Pkaart)
            CopyRst(self.catchm.P, self.Pkaart)
        else:
            self.logger.warning('P-kaart bekken is niet aangemaakt')

        if self.ModelOptions['River Routing'] == 1:
            self.Output['OutputPerSegment'] = 1
            if self.catchm.riverroutingkrt is not None:
                # kopieren van rivierroutingkaart naar modelinputfolder
                self.riverrouting = os.path.split(self.catchm.riverroutingkrt)[1]
                self.riverrouting = os.path.join(self.infolder, self.riverrouting)
                CopyRst(self.catchm.riverroutingkrt, self.riverrouting)
            else:
                self.ModelOptions['River Routing'] = 0
                self.riverrouting = ''

        if self.Output['OutputPerSegment'] == 1:
            if self.catchm.segmentkrt is not None:
                # Kopieren van riviersegmentenkaart naar modelinputfolder
                riviersegm_bekken = self.catchm.segmentkrt
                self.riviersegmkrt = os.path.split(riviersegm_bekken)[1]
                self.riviersegmkrt = os.path.join(self.infolder, self.riviersegmkrt)
                CopyRst(riviersegm_bekken, self.riviersegmkrt)
            else:
                self.Output['OutputPerSegment'] = 0
                self.riviersegmkrt = ''
        else:
            self.riviersegmkrt = ''

        if 'Rainfall_file' in self.Variables.keys():
            if self.Variables['Rainfall_file'] != '':
                temp = self.Variables['Rainfall_file']
                self.Variables['Rainfall_file'] = os.path.split(self.Variables['Rainfall_file'])[1]
                self.Variables['Rainfall_file'] = os.path.join(self.infolder, self.Variables['Rainfall_file'])
                shutil.copy2(temp, self.Variables['Rainfall_file'])
        return

    def SetEBMOptions(self, Opts, defaultini=None):
        """
        Sets the erosion control options for the scenario. The possible keys are
        - 'UseBuffers': use bufferbasins
        - 'UseGras': use grasstrips
        - 'UseGeleidendeDammen': use dams
        - 'UseGrachten': use ditches
        - 'UseTeelttechn': use culture specific measures (like strip till,...
        :param Opts: dictionary with keys (see above) and values (0 or 1)
        :param defaultini: optional ini-file with section 'EBMOptions' and some of the above mentioned keys as parameters
        :return: none
        """
        MandatoryKeys = ['UseBuffers', 'UseGras', 'UseGeleidendeDammen',
                         'UseGrachten', 'UseTeelttechn']

        for key in MandatoryKeys:
            try:
                self.EBMOptions[key] = Opts[key]
            except KeyError:
                # read key from default values
                if defaultini is not None:
                    defaultvalue = GetItemFromIni(defaultini, 'EBMOptions', key, 'int')
                    msg = '%s is not given in the EBM options, default value (%s) is used' % (key, defaultvalue)
                    self.logger.warning(msg)
                else:
                    defaultvalue = None

                if defaultvalue is not None:
                    self.EBMOptions[key] = defaultvalue
                else:
                    msg = 'No defaultvalue for EBMOption %s given' %key
                    raise CNWSException(msg)

    def SetModelVersie(self, version):
        """
        Sets the modelversion to be used in the scenario. The model-version will be stored in the variables
        :param version: str, 'WS' or 'CNWS'
        :return: nothing
        """
        if version == 'WS':
            self.versie = version
            self.Variables['Simple'] = 1
        elif version == 'CNWS':
            self.versie = version
            self.Variables['Simple'] = 0
        elif version == 'RoutingOnly':
            self.versie = 'RoutingOnly'
            self.Variables['Simple'] = 1
        else:
            msg = 'geen correcte modelversie gekozen, kies "WS" of "CNWS"'
            raise CNWSException(msg)
        return

    def SetLSmodel(self, L='Desmet1996_Vanoost2003', S='Nearing1997'):
        """
        Sets the L and S-model for the scenario. The chosen values will be stored in the ModelOptions.
        :param L: default 'Desmet1996_Vanoost2003' (or 'Desmet1996_McCool')
        :param S:  default 'Nearing1997' (or 'Desmet1996')
        :return: nothing
        """
        Lmodels = ['Desmet1996_Vanoost2003', 'Desmet1996_McCool']
        Smodels = ['Nearing1997', 'Desmet1996']
        if L in Lmodels:
            self.ModelOptions['L model'] = L
        else:
            msg = 'No correct L model was given! please choose %s or %s' %(Lmodels[0], Lmodels[1])
            self.logger.error(msg)

        if S in Smodels:
            self.ModelOptions['S model'] = S
        else:
            msg = 'No correct S model was given! please choose %s or %s' % (Smodels[0], Smodels[1])
            self.logger.error(msg)
        return

    def SetModelOptions(self, Opts, defaultini=None):
        """
        Sets the ModelOptions for the scenario. The possible keys are
        - 'ConvertOutput'
        - 'CreateKTIL'
        - 'EstimClay'
        - 'ManualOutlet'
        - 'Calibrate'
        - 'Adjusted Slope'
        - 'Buffer reduce Area'
        - 'Force Routing'
        - 'FilterDTM'
        - 'River Routing'
        - 'BankGrassStrips'
        When the modelversion is 'WS' one can also define 'UseR' as a key.
        :param Opts: dictionary with keys (see above) and values (0 or 1)
        :param defaultini: optional ini-file with section 'Options' and some of the above mentioned keys as parameters
        :return: none
        """
        MandatoryKeys = ['ConvertOutput', 'ManualOutlet', 'BankGrassStrips',
                         'Adjusted Slope', 'Buffer reduce Area', 'Force Routing', 'FilterDTM', 'River Routing']

        xtrkeys = ['EstimClay', 'CreateKTIL', 'Calibrate']

        if self.versie == 'WS':
            MandatoryKeys.append('UseR')

        if self.versie != 'RoutingOnly':
            MandatoryKeys = MandatoryKeys + xtrkeys

        for key in MandatoryKeys:
            try:
                self.ModelOptions[key] = Opts[key]
            except KeyError:
                if defaultini is not None:
                    # read key from default values
                    defaultvalue = GetItemFromIni(defaultini, 'Options', key, 'int')
                else:
                    defaultvalue = None

                if defaultvalue is not None:
                    msg = '%s is not given in the model options, default value (%s) is used' % (key, defaultvalue)
                    self.logger.warning(msg)
                    self.ModelOptions[key] = defaultvalue
                else:
                    msg = 'No default value for Model Option %s given' %key
                    raise CNWSException(msg)

        if self.versie == 'RoutingOnly':
            for key in xtrkeys:
                self.ModelOptions[key] = 0
        return

    def SetModelVariables(self, Vars, defaultini=None):
        """
        Sets the ModelVariables for the scenario. The possible keys are:
        'Bulkdensity', 'ktc_limit', 'Parcel connectivity cropland', 'Parcel connectivity forest', 'Parcel trapping efficiency cropland',
        'Parcel trapping efficiency forest', 'Parcel trapping efficiency pasture', 'begin_jaar', 'begin_maand', 'Max_kernel', 'Max_kernel_river'

        When the CN-module is used, some additional variables are needed:
        'Alpha', 'Beta', 'velocity', 'CNTable'

        When Rainfalldata is used (i.e. WS and UseR=0 or CNWS), some additional variables are needed:
        '5dayRainfall', 'Timestep', 'Endtime', 'Timestep_output', 'Rainfall_file'

        If no Rainfalldata is used (i.e. WS using a general R-factor) we need to define 'R'.

        When calibrating the WS model, we need 'KTcHigh_lower', 'KTcHigh_upper', 'KTcLow_lower', 'KTcLow_upper' and 'steps'
        Not calibrating 'ktc_low', 'ktc_high' are needed

        When the ModelOption['CreateKTIL'] is 1, we need 'ktil_default' and 'ktil_threshold'

        When the ModelOption['EstimClay'] is 1, we need 'ClayContent'.

        When the ModelOption['ManualOutlet']  is 1, we need 'Outletshp'

        When the ModelOption['Force Routing'] is 1, we need 'Routingshp'

        :param Vars: dictionary with keys (see above) and values
        :param defaultini: optional ini-file with section 'Variables' and some of the above mentioned keys as parameters
        :return: none
        """
        MandatoryKeys = ['Parcel connectivity cropland',
                         'Parcel connectivity forest',
                         'Parcel trapping efficiency cropland',
                         'Parcel trapping efficiency forest',
                         'Parcel trapping efficiency pasture',
                         'begin_jaar', 'begin_maand',
                         'Max_kernel', 'Max_kernel_river',
                         'Bulkdensity']

        CNKeys = ['Alpha', 'Beta', 'velocity', 'CNTable']

        Rainfallkeys = ['5dayRainfall', 'Timestep', 'Endtime', 'Timestep_output', 'Rainfall_file']

        if self.ModelOptions['Calibrate'] == 1:
            KtcKeys = ['KTcHigh_lower', 'KTcHigh_upper',
                       'KTcLow_lower', 'KTcLow_upper', 'steps', 'ktc_limit']
        else:
            KtcKeys = ['ktc_low', 'ktc_high', 'ktc_limit']

        MandatoryKeys = MandatoryKeys + KtcKeys

        if self.ModelOptions['CreateKTIL'] == 1:
            ktilKeys = ['ktil_default', 'ktil_threshold']
            MandatoryKeys = MandatoryKeys + ktilKeys

        if self.versie == 'CNWS':
            MandatoryKeys = MandatoryKeys + CNKeys
            MandatoryKeys = MandatoryKeys + Rainfallkeys
        elif self.versie == 'WS':  # WS-model
            if self.ModelOptions['UseR'] == 1:
                MandatoryKeys.append('R')
            else:
                MandatoryKeys = MandatoryKeys + Rainfallkeys

        if self.ModelOptions['EstimClay'] == 1:
            MandatoryKeys.append('ClayContent')

        if self.ModelOptions['ManualOutlet'] == 1:
            MandatoryKeys.append('Outletshp')

        if self.ModelOptions['Force Routing'] == 1:
            MandatoryKeys.append('Routingshp')

        if self.ModelOptions['BankGrassStrips'] == 1:
            MandatoryKeys.append('Width BankGrassStrips')

        for key in MandatoryKeys:
            try:
                self.Variables[key] = Vars[key]
            except KeyError:
                if defaultini is not None:
                    if key in ['ktc_limit', 'ktil_threshold', 'Alpha', 'Beta', 'velocity']:
                        T = 'float'
                    elif key in ['Outletshp', 'Routingshp', 'Rainfall_file', 'CNTable']:
                        T = 'get'
                    else:
                        T = 'int'

                    # read key from default values
                    defaultvalue = GetItemFromIni(defaultini, 'Variables', key, T)
                    msg = '%s is not given in the model variables, default value (%s) is used' % (key, defaultvalue)
                    self.logger.warning(msg)
                else:
                    defaultvalue = None

                if defaultvalue is not None:
                    self.Variables[key] = defaultvalue
                else:
                    msg = 'No value for variable %s given in default values' % key
                    raise CNWSException(msg)
        return

    def SetOutput(self, Output, defaultini=None):
        """
        Sets the modeloutput for the scenario. The possible keys are:
        WriteAspect, WriteLS, WriteRUSLE, WriteSedExport, WriteSlope, WriteTillageErosion, WriteTotalRunof,
        WriteUpstreamArea, WriteWaterErosion

        :param Vars: dictionary with keys (see above) and values (0 or 1)
        :param defaultini: optional ini-file with section 'Output' and some of the above mentioned keys as parameters
        :return: nothing
        """

        MandatoryKeys = ['WriteAspect', 'WriteLS', 'WriteSlope',
                         'WriteUpstreamArea', 'OutputPerSegment', 'WriteRouting']

        WSKeys = ['WriteRUSLE', 'WriteSedExport', 'WriteTillageErosion', 'WriteWaterErosion']
        CNKeys = ['WriteRainExcess', 'WriteTotalRunoff',]

        if self.versie == 'WS' or self.versie == 'CNWS':
            MandatoryKeys = MandatoryKeys + WSKeys
        else:  # RoutingOnly
            for key in WSKeys:
                self.Output[key] = 0

        if self.versie == 'CNWS':
            MandatoryKeys = MandatoryKeys + CNKeys
        else:  # WS en RoutingOnly
            for key in CNKeys:
                self.Output[key] = 0

        for key in MandatoryKeys:
            try:
                self.Output[key] = Output[key]
            except KeyError:
                if defaultini is not None:
                    # read key from default values
                    msg = '%s is not given in the model options, default value is used' % key
                    self.logger.warning(msg)
                    defaultvalue = GetItemFromIni(defaultini, 'Output', key, 'int')
                else:
                    defaultvalue = None

                if defaultvalue is not None:
                    self.Output[key] = defaultvalue
                else:
                    msg = 'No value for Model Option given in default values'
                    raise CNWSException(msg)
        return

    def CalculateTimeSettings(self):
        """
        Calculates for which years and seasons the scenario needs data. It uses the variables 'begin_jaar'
        'begin_maand' and, in case of CNWS, 'Endtime'.
        :return: nothing
        """

        if (self.versie == 'WS' and self.ModelOptions['UseR'] == 1) or self.versie=='RoutingOnly':
                self.jaren = [self.Variables['begin_jaar']]
                self.seizoenen = [['spring']]
        else:  # TO DO!!
             begintijd = datetime.date(self.Variables['begin_jaar'], 
                                       self.Variables['begin_maand'], 1)
             tijdsduur = datetime.timedelta(minutes=self.Variables['Endtime'])
             eindtijd = begintijd + tijdsduur

             # TO DO: verder uitwerken
             self.jaren = [self.Variables['begin_jaar']]
             self.seizoenen = [['spring']]
#             for maand in maanden:
#                 if maand == 1 and jaar2 == True:
#                     Seizoen = SeizoenenJaar2
#                 elif maand == 1 or maand == 2 or maand == 12:
#                     Seizoen['winter'] = True
#                 elif maand == 3 or maand == 4 or maand == 5:
#                     Seizoen['spring'] = True
#                 elif maand == 6 or maand == 7 or maand == 8:
#                     Seizoen['summer'] = True
#                 elif maand == 9 or maand == 10 or maand == 11:
#                     Seizoen['fall'] = True
        return

    def CreateForceRoutingData(self):
        """
        Makes a geodataframe containing the coordinates of the points where the model needs to 'jump'.
        Sets the scenario.forcedroutingdata argument. Requires scenario.Variables['Routingshp']
        :return: nothing
        """
        forceshp = self.Variables['Routingshp']
        try:
            gdf = gpd.read_file(forceshp)
        except Exception:
            msg = 'could not open %s, model will ignore option force routing'% forceshp
            self.logger.warning(msg)
            self.ModelOptions['Force Routing'] = 0
        else:
            with fiona.open(self.catchm.shp) as catch:
                d = catch[0]['geometry']
                geom_catchm = shapely.geometry.asShape(d)
                gdf['incatch'] = gdf.geometry.within(geom_catchm)
                gdf = gdf.loc[(gdf['incatch'] == True)]
                if gdf.shape[0] == 0:
                    msg = 'No forced routings present within modeldomain, model will ignore option'
                    self.logger.warning(msg)
                    self.ModelOptions['Force Routing'] = 0
                else:
                    gdf['NR'] = range(1, gdf.shape[0] + 1)
                    # extract X an Y form first and last vertex
                    gdf['fromX'] = gdf.apply(lambda x: x.geometry.coords[0][0], axis=1)
                    gdf['fromY'] = gdf.apply(lambda x: x.geometry.coords[0][1], axis=1)
                    gdf['nrVertex'] = gdf.apply(lambda x: len(x.geometry.coords), axis=1)
                    gdf['toX'] = gdf.apply(lambda x: x.geometry.coords[x.nrVertex - 1][0], axis=1)
                    gdf['toY'] = gdf.apply(lambda x: x.geometry.coords[x.nrVertex - 1][1], axis=1)
                    # terugrekenen naar gridcoordinaten
                    gdf['fromcol'] = round((gdf.fromX - self.catchm.RasterProp['minmax'][0])/self.catchm.RasterProp['res'])
                    gdf['fromrow'] = abs(round((gdf.fromY - self.catchm.RasterProp['minmax'][3])/self.catchm.RasterProp['res']))
                    gdf['tocol'] = round((gdf.toX - self.catchm.RasterProp['minmax'][0])/self.catchm.RasterProp['res'])
                    gdf['torow'] = abs(round((gdf.toY - self.catchm.RasterProp['minmax'][3])/self.catchm.RasterProp['res']))

                    self.forcedroutingdata = gdf
        return

    def filterDTM(self, year):
        """
        Filters the DTM using a 3x3 moving window within the borders of a parcel. sets the scenario.DTMkaart argument.
        uses saga
        :param year: int, the year of the parcel borders that will be used
        :return: nothing
        """

        if self.prcrst[year] is None:
            msg = 'no boundaries of parcels available, filtering dhm in parcels not possible'
            self.logger.warning(msg)
        else:
            dtm_filtered = os.path.join(self.sfolder, 'dtm_filtered_%s.sgrd' %year)
            self.DTMkaart = os.path.join(self.infolder, 'dtm_filtered_%s.rst' %year)

            cmd_str = 'saga_cmd -f=s watem 2 '
            cmd_str += '-DEM "%s" ' % self.catchm.DTM
            cmd_str += '-PRC "%s" ' % self.prcrst[year]
            cmd_str += '-DEM_FILTER "%s"' % dtm_filtered
            msg = 'failed to filter dtm'
            execute_subprocess(cmd_str, msg)

            dtm_filtered = dtm_filtered[:-4] + 'sdat'
            Tiff2Rst(dtm_filtered, self.DTMkaart, 'float')
        return

    def CreateOutletMap(self):
        """
        Makes an idrisi-raster with the outlets based on the scenario.Variables['Outletshp'] value.
        Sets the scenario.outletmap argument.
        :return: nothing
        """
        outletshp = self.Variables['Outletshp']
        try:
            gdf = gpd.read_file(outletshp)
        except Exception:
            msg = 'could not open shapefile with outlets, model will use lowest pixel as outlet'
            self.logger.warning(msg)
            self.ModelOptions['ManualOutlet'] = 0
            self.outletmap = os.path.join(self.infolder, 'Outlet.rst')
        else:
            if gdf.shape[0] == 0:
                msg = 'No outlets present in shapefile with outlets, model will use lowest pixel as outlet'
                self.logger.warning(msg)
                self.ModelOptions['ManualOutlet'] = 0
                self.outletmap = os.path.join(self.infolder, 'Outlet.rst')
            else:
                # check if features outletshp are within modeldomain
                # check if field NR is available in outletshp
                with fiona.open(self.catchm.shp) as catch:
                    d = catch[0]['geometry']
                geom_catchm = shapely.geometry.asShape(d)
                gdf['incatch'] = gdf.geometry.within(geom_catchm)
                gdf = gdf.loc[(gdf['incatch'] == True)]
                if gdf.shape[0] == 0:
                    msg = 'No outlets present within modeldomain, model will use lowest pixel as outlet'
                    self.logger.warning(msg)
                    self.ModelOptions['ManualOutlet'] = 0
                    self.outletmap = os.path.join(self.infolder, 'Outlet.rst')
                else:
                    gdf = gdf.drop(columns=['incatch'])
                    if not 'NR' in gdf.columns:
                        gdf['NR'] = range(1, gdf.shape[0] + 1)
                    outletshp_scen = 'outlets_%s_scenario_%s.shp' % (self.catchm.naam, self.nr)
                    gdf.to_file(outletshp_scen)

                    outlettmp = os.path.join(self.sfolder, 'outlet_scenario_%s.tif' % self.nr)
                    self.outletmap = os.path.join(self.infolder, 'outlets_scenario_%s.rst' % self.nr)
                    rstprop = self.catchm.RasterProp.copy()
                    rstprop['nodata'] = 0
                    Shp2Rst_field(outletshp_scen, outlettmp, rstprop, 'NR')
                    Tiff2Rst(outlettmp, self.outletmap, 'integer')
        return

    def AddBankGrassStrips(self, width):
        if self.catchm.waterLine is not None:
            gdf = gpd.read_file(self.catchm.waterLine)
            if gdf.shape[0] == 0:
                msg = 'No features in waterlines, model will not create bank grass strips'
                self.logger.warning(msg)
                self.ModelOptions['BankGrassStrips'] == 0
            else:
                gdf['buffer'] = gdf.buffer(width)
                gdf = gdf[['NR', 'buffer']]
                gdf['BREEDTE'] = width
                gdf.rename(columns={'buffer': 'geometry'}, inplace=True)
                gdf = gpd.GeoDataFrame(gdf, geometry='geometry')
                outshp = os.path.join(self.sfolder, 'BankGrassStrips_%sm.shp' %str(width))
                gdf.to_file(outshp)
                self.BankGrassStrips = {'gras':outshp}
        else:
            self.ModelOptions['BankGrassStrips'] == 0
        return

    def AddExtraEBMData(self, extrashp, datatype):
        """
        Adds extra shapefiles with Erosion Control features. Every shapefile can only contain one type of measures
        :param extrashp: list, containting shapes with extra Erosion Control measures
        :param datatype: list, containing the types of the shapefiles of extrashp (i.e. 'buffer', 'gras', 'teelttechn')
        :return: nothing
        """
        try:
            gdf = gpd.read_file(extrashp)
        except Exception:
            msg = 'could not open %s, model will ignore it' % extrashp
            self.logger.warning(msg)
        else:
            if gdf.shape[0] == 0:
                msg = 'No featuers present in %s, model will ignore it' % extrashp
                self.logger.warning(msg)
            else:
                # check if features outletshp are within modeldomain
                # check if field NR is available in outletshp
                with fiona.open(self.catchm.shp) as catch:
                    d = catch[0]['geometry']
                geom_catchm = shapely.geometry.asShape(d)
                gdf['incatch'] = gdf.geometry.within(geom_catchm)
                gdf = gdf.loc[(gdf['incatch'] == True)]
                if gdf.shape[0] == 0:
                    msg = 'No features of %s present within modeldomain, model will ignore shp' % extrashp
                    self.logger.warning(msg)
                else:
                    if datatype == 'gras':
                        gdf['AvWidth'] = (gdf.length - np.sqrt((gdf.length**2)-(16*gdf.area)))/4
                        if not ('BREEDTE' in gdf.columns):
                            self.logger.warning('Breedtes extra grasbufferstroken zijn niet gedefinieerd')
                            gdf['BREEDTE'] = gdf['AvWidth']
                        else:
                            gdf['BREEDTE'] = np.where(gdf['BREEDTE'] == 0.0, gdf['AvWidth'], gdf['BREEDTE'])
                            gdf['BREEDTE'] = np.where(gdf['BREEDTE'].isnull(), gdf['AvWidth'], gdf['BREEDTE'])

                    elif datatype == 'buffer':
                        verplichtevelden = ['HDam', 'HKnijp', 'DKnijp',
                                            'QCoef', 'Boverl', 'Eff', 'Buffercap']
                        stdwaarde = [1.0, 0, 0.03, 0.6, 7, 75]
                        for j, veld in enumerate(verplichtevelden):
                            if not (veld in gdf.columns):
                                self.logger.warning('%s is voor extra buffers niet gedefinieerd' % veld)
                                self.logger.warning('Veld %s wordt toegevoegd met standaardwaarde' % veld)
                                if veld == 'Buffercap':
                                    gdf[veld] = gdf.area*1.0  # we gaan er van uit dat de dam 1m hoog is
                                else:
                                    gdf[veld] = stdwaarde[j]

                    gdf = gdf.drop(columns=['incatch'])
                    gdf.to_file(extrashp)
                    self.ExtraData[datatype] = extrashp
        return

    def MergeEBM(self, year, SrcDicts, typelist=None):
        """
        Merges all Erosion control data for a given year
        :param year: int
        :param SrcDicts: a list with EBM Dicts (of the given year) is given
        :param typelist: list with types of EBM to be merged
        :return:
        """

        self.logger.info("Samenvoegen EBM's...")
        folder = os.path.join(self.sfolder, str(year))
        os.chdir(folder)
        SrcDicts.append(self.ExtraData)

        if typelist is None:
            typelist = ['poelen']
            if self.EBMOptions['UseBuffers'] == 1:
                typelist.append('buffer')
            else:
                self.bufferData = []

            if self.EBMOptions['UseGras'] == 1:
                typelist.append('gras')
            else:
                self.grasshp[year] = None

            if self.EBMOptions['UseGeleidendeDammen'] == 1:
                typelist.append('gdammen')
            else:
                self.gdammenshp = None

            if self.EBMOptions['UseGrachten'] == 1:
                typelist.append('grachten')
            else:
                self.grachtenshp = None

            if self.EBMOptions['UseTeelttechn'] == 1:
                typelist.append('teelttechn')
            else:
                self.teelttechnshp[year] = None
        else:
            if 'gras' not in typelist:
                self.grasshp[year] = None

            if 'buffer' not in typelist:
                self.bufferData = []

            if 'grachten' not in typelist:
                self.grachtenshp = None

            if 'teelttechn' not in typelist:
                self.teelttechnshp[year] = None

        for Type in typelist:
            mergedShps = '%s_%s_%s_scenario_%s.shp' % (Type, self.catchm.naam, year, self.nr)
            mergedShps = os.path.join(folder, mergedShps)
            mergelist = []

            for dict_ in SrcDicts:
                if Type in dict_.keys():
                    mergelist.append(dict_[Type])
            if len(mergelist) != 0:
                MergeShps(mergelist, mergedShps, self.catchm.RasterProp['epsg'])
                if GetFeatureCount(mergedShps) == 0:
                    mergedShps = None
            else:
                mergedShps = None

            if Type == 'gras':
                self.grasshp[year] = mergedShps
            elif Type == 'buffer':
                self.buffershp = mergedShps
            elif Type == 'grachten':
                self.grachtenshp = mergedShps
            elif Type == 'gdammen':
                self.gdammenshp = mergedShps
            elif Type == 'teelttechn':
                self.teelttechnshp[year] = mergedShps
            elif Type == 'poelen':
                self.poelenshp = mergedShps
        return

    def PrepareGrasEBM(self, year):
        """
        Numbers all grasstrips and converts the grasstrips to a raster. The raster will be stored in
        scenario.grasrst[year].
        :param year: the year to be processed (int)
        :return: nothing
        """
        grasshp = self.grasshp[year]
        try:
            gdf = gpd.read_file(grasshp)
        except OSError:
            msg = 'could not open %s ' % grasshp
            self.logger.warning(msg)
            self.grasrst[year] = None
        except AttributeError: # raised if grasshp is none
            self.logger.warning('No grasstrips for %s available in scenario!' % year)
            self.grasrst[year] = None
        else:
            gdf['NR'] = range(1, gdf.shape[0] + 1)
            gdf = gdf[['NR', 'BREEDTE', 'geometry']]
            gdf.to_file(grasshp)
            self.grasrst[year] = 'grasstripsID_%s_%s_scenario_%s.tif' % (self.catchm.naam, year, self.nr)
            Shp2Rst_field(grasshp, self.grasrst[year], self.catchm.RasterProp, 'NR')
            # uitzoeken welk perceel afwatert naar de grasstrook
            # perceelnummer van het afwaterende perceel opslaan in de attributentabel
        return

    def AddTeelttechn2Percelen(self, year):
        """
        Adds to scenario.percelenshp[year] the attributes with crop-technical information, based on
        self.teelttechnshp[year].
        :param year: int, the year to be processed.
        :return:
        """
        def nearly_identical(geoms, p):
            nearly = (geoms.intersection(p).area / p.area) > 0.75
            # return index values where nearly is True
            return pd.Series(nearly.index[nearly])

        if self.teelttechnshp[year] is not None and self.percelenshp[year] is not None:
            df = gpd.read_file(self.percelenshp[year])
            df_teelttech = gpd.read_file(self.teelttechnshp[year])
            matches = df.geometry.apply(lambda x: nearly_identical(df_teelttech, x))
            matches2 = matches.unstack().reset_index(0, drop=True).dropna()
            df_teelttech_matched = df_teelttech.reindex(index=matches2.values)
            df_teelttech_matched.index = matches2.index
            df_teelttech_matched = df_teelttech_matched[['Type']]

            df_merged = pd.merge(df, df_teelttech_matched, how='left', left_index=True, right_index=True)
            df_merged.loc[df_merged['Type'] == 'ntkerend', "ntkerend"] = 1
            df_merged.loc[df_merged['Type'] == 'drempels', "drempels"] = 1
            df_merged.loc[df_merged['Type'] == 'contourzaaien', "contour"] = 1
            df_merged.loc[df_merged['Type'] == 'gewasrest', "gewasrest"] = 1
            df_merged.loc[df_merged['Type'] == 'groenbedekker', "groenbedek"] = 1
            df_merged.drop('Type', axis=1, inplace=True) 
            df_merged.to_file(self.percelenshp[year])
        return

    def ConvertGrasStrips2Fields(self, year):
        def nearly_identical(geoms, p):
            nearly = (geoms.intersection(p).area / p.area) > 0.75
            # return index values where nearly is True
            return pd.Series(nearly.index[nearly])

        if self.grasshp[year] is not None and self.percelenshp[year] is not None:
            df_prc = gpd.read_file(self.percelenshp[year])
            df_gras = gpd.read_file(self.grasshp[year])
            matches = df_prc.geometry.apply(lambda x: nearly_identical(df_gras, x))
            matches2 = matches.unstack().reset_index(0, drop=True).dropna()
            df_gras_matched = df_gras.reindex(index=matches2.values)
            df_gras_matched.index = matches2.index
            df_gras_matched['grasstrip'] = 1
            # df_gras_matched.drop('geometry', axis=1, inplace=True)
            # df_gras_matched.drop_duplicates(inplace=True)
            df_gras_matched = df_gras_matched[['grasstrip']]

            df_merged = pd.merge(df_prc, df_gras_matched, how='left', left_index=True, right_index=True)
            df_merged.loc[df_merged['grasstrip'] == 1, "Lndgbrk"] = -9999
            df_merged.loc[df_merged['grasstrip'] == 1, "GWSCOD_H"] = 9999
            df_merged.to_file(self.percelenshp[year])
        return

    def RemoveKnownGrasStrips(self, year):
        """
        It is possible that scenario.percelenshp[year] contains some parcels that are also present in
        self.grasshp[year]. This function removes these parcels in scenario.percelenshp[year] if the overlap between
        both features is at least 75%.
        :param year: int, the year to be processed.
        :return: Nothing
        """
        def nearly_identical(geoms, p):
            nearly = (geoms.intersection(p).area / p.area) > 0.75
            # return index values where nearly is True
            return pd.Series(nearly.index[nearly])

        if self.grasshp[year] is not None and self.percelenshp[year] is not None:

            df_gras = gpd.read_file(self.grasshp[year])
            df_prc = gpd.read_file(self.percelenshp[year])
            matches = df_prc.geometry.apply(lambda x: nearly_identical(df_gras, x))
            matches2 = matches.unstack().reset_index(0, drop=True).dropna()
            df_prc = df_prc.drop(df_prc.index[matches2.index])
            df_prc.to_file(self.percelenshp[year])
        return

    def CreatePerceelskaart(self, year):
        """
        Creates the Parcel map for a given year. This raster is an inputraster for the CNWS-model.
        Sets the scenario.prckrt[year] variable.
        :param year: int, the year to be processed.
        :return: Nothing
        """
        self.logger.info('Aanmaken percelenkaart...')
        Cnst = self.catchm.RasterProp
        tempfolder = os.path.join(self.sfolder, str(year))
        os.chdir(tempfolder)

        volg = []
        extrnaam = '_%s_%s_scenario_%s.tif' % (self.catchm.naam, year, self.nr)
        # waterlopen
        waterlopenRst = self.catchm.waterlopenkrt
        if waterlopenRst is not None:
            volg.append(waterlopenRst)

        # poelen
        # if self.poelenshp is not None:
           # naam = 'poelen%s' % extrnaam
           # Shp2Rst_val(self.poelenshp, naam, -5, Cnst)
           # volg.append(naam)

        # infrastructuur
        infrastructuurRst = self.catchm.infrTif
        if infrastructuurRst is not None:
            volg.append(infrastructuurRst)

        # gras omzetten naar rst
        if self.grasshp[year] is not None:
            naam = 'gras%s' % extrnaam
            Shp2Rst_val(self.grasshp[year], naam, -6, Cnst)
            volg.append(naam)

        # Percelenshp omzetten naar rasters
        if self.percelenshp[year] is not None:
            prc_df = gpd.read_file(self.percelenshp[year])
            maxPrcID = prc_df.NR.max()

            # percelen met een bepaald landuse (-2, -4) worden verrasterd naar lndgbrk_parcels
            if len(prc_df.Lndgbrk.unique()) == 1: # if there is only one landuse class
                if prc_df.Lndgbrk.unique()[0] != self.catchm.RasterProp['nodata']:
                    lndgbrk_parcels = 'lnduse_parcels%s' % extrnaam
                    Shp2Rst_field(self.percelenshp[year], lndgbrk_parcels, Cnst, 'Lndgbrk')
                    volg.append(lndgbrk_parcels)
                # else: would result in a nodata raster (useless...)
            else:
                lndgbrk_parcels = 'lnduse_parcels%s' % extrnaam
                Shp2Rst_field(self.percelenshp[year], lndgbrk_parcels, Cnst, 'Lndgbrk')
                volg.append(lndgbrk_parcels)

            # alle percelen worden verrasterd met het ID. Dit raster wordt bewaard in self.prcrst
            percelenname = 'percelen%s' % extrnaam
            Shp2Rst_field(self.percelenshp[year], percelenname, Cnst, 'NR')
            volg.append(percelenname)
            self.prcrst[year] = os.path.join(tempfolder, percelenname)
        else:
            maxPrcID = 0
            self.prcrst[year] = None
            
        # voeg een extra perceel met perceelid i toe aan de perceeltstabel
        i = maxPrcID + 1
        data = {'NR': i,
                'GWSCOD_H': 9999,
                'GWSCOD_N': 9999,
                'GWSCOD_V': 9999,
                'drempels': 0,
                'contour': 0,
                'gewasrest': 0,
                'groenbedek': 0,
                'ntkerend': 0,
                'Lndgbrk': None}
        adddf = pd.DataFrame.from_dict(data, orient='index')
        adddf = adddf.transpose()
        if self.percelenshp[year] is not None:
            prc_df = prc_df.append(adddf, ignore_index=True)
            prc_df = prc_df.drop('geometry', 1)
        else:
            prc_df = adddf.copy()
        
        prctable = 'percelen%s.csv' % extrnaam[:-4]
        self.prctable[year] = os.path.join(tempfolder, prctable)
        prc_df.to_csv(self.prctable[year], sep=';', index=False, header=True, encoding='utf-8')

        # landgebruik
        try:
            land_arr = readRstAsArr(self.catchm.LandUse)
        except Exception:
            land_arr = np.full((self.catchm.RasterProp['nrows'],
                                self.catchm.RasterProp['ncols']), 10).astype('int16')
        finally:
            land_arr = np.where(land_arr == 10, i, land_arr).astype('int16')
            nolanduse = np.logical_and(land_arr == Cnst['nodata'], self.catchm.binarr == 1)
            land_arr = np.where(nolanduse, i, land_arr).astype('int16')

        tmp_LandUse = 'landgebruik_reclass%s' % extrnaam
        write_arr_as_rst(land_arr, tmp_LandUse, 'int16', self.catchm.rstparams)
        volg.append(tmp_LandUse)

        # perceelkaart aanmaken
        if len(volg) != 0:
            for i in range(len(volg)):
                arr = readRstAsArr(volg[i])
                if i == 0:
                    perceelskaart_arr = arr.copy()
                else:
                    if arr.shape != perceelskaart_arr.shape:
                        msg = 'inputrasters voor perceelskaart hebben niet dezelfde dimensies'
                        raise CNWSException(msg)
                    reclass = np.where(perceelskaart_arr == Cnst['nodata'], arr, perceelskaart_arr)
                    perceelskaart_arr = reclass.copy()
        else:  # er zijn geen inputkaarten
            # array met waarde i aanmaken
            self.logger.warning('geen inputdata voor perceelskaart!')
            self.logger.warning('Perceelskaart wordt een perceel met gewascode 9999')
            perceelskaart_arr = np.full((self.catchm.RasterProp['nrows'],
                                         self.catchm.RasterProp['ncols']), i)

        perceelskaart_arr = np.where(perceelskaart_arr == -7, -2, perceelskaart_arr)  # aardewegen infstructuur maken
        rstarr = np.where(self.catchm.binarr == 1, perceelskaart_arr, 0).astype('float64')

        prcmap = os.path.join(tempfolder, 'perceelskaart%s' % extrnaam)
        write_arr_as_rst(rstarr, prcmap, 'float64', self.catchm.rstparams)

        prcmap_rst = os.path.split(prcmap)[1][:-4] + '.rst'
        prcmap_rst = os.path.join(self.infolder, prcmap_rst)
        Tiff2Rst(prcmap, prcmap_rst, 'float')
        self.prckrt[year] = prcmap_rst
        return

    def CreateTeeltData(self, gwsdata, year):
        """
        Creates a dataframe containing all parcel specific-information. The function joins a
        table (gwsdata) with scenario.prctable[year] on the field 'GWSCOD'. The created dataframe
        is stored in scenario.perceelsdf[year]
        :param gwsdata: path to csv table (delimiter = ';')
        :param year: int, the year to be processed
        :return:
        """
        df_percelen = pd.read_csv(self.prctable[year], sep=';')
        if gwsdata is not None:
            df_gwscodes = pd.read_csv(gwsdata, sep=';', encoding='cp1250')
            df_gwscodes.drop(columns=['Lndgbrk'], inplace=True)
            join = pd.merge(df_percelen, df_gwscodes, how='left', left_on='GWSCOD_H', right_on='GWSCOD')
        else:
            msg = 'gwscodes niet gekend, std-waarden voor C-factor, CNtype_id en HydroCond'
            self.logger.warning(msg)
            join = df_percelen.copy()
            join['C_factor'] = 0.37
            join['CNtype_id'] = 4
            join['HydroCond'] = 'Poor'

        if self.versie == 'WS':
            join = join[['NR', 'GWSCOD_H', 'GWSCOD_N', 'GWSCOD_V',
                         'contour', 'drempels', 'gewasrest', 'groenbedek',
                         'ntkerend', 'C_factor', 'Lndgbrk']]
        else:
            join = join[['NR', 'GWSCOD_H', 'GWSCOD_N', 'GWSCOD_V',
                         'contour', 'drempels', 'gewasrest', 'groenbedek',
                         'ntkerend', 'C_factor', 'CNtype_id', 'HydroCond', 'Lndgbrk']]

        self.perceelsdf[year] = join
        return

    def CreateCkaart(self, year, season):
        """
        Creates the C-factor map for a given year and season.
        Sets the scenario.Ckaarten[year][season]-argument
        :param year: int, the year to be processed
        :param season: str, the seson to be processed
        :return: nothing
        """
        self.logger.info('Aanmaken C-factorkaart...')
        if self.perceelsdf[year] is None:
            msg = 'ERROR: perceelsdf niet aangemaakt!'
            raise CNWSException(msg)
        elif self.prckrt[year] is None:
            msg = 'ERROR: Geen perceelskaart voor C-kaart aan te maken'
            raise CNWSException(msg)
        else:
            # first, basic reclass --> fixed landuse classes
            lnduse_arr = readRstAsArr(self.prckrt[year])

            C_arr = np.zeros(lnduse_arr.shape).astype('float32')
            C_arr = C_arr + self.catchm.RasterProp['nodata']  # alles op nodata zetten
            nd = self.catchm.RasterProp['nodata']

            # waterlopen
            waterlopenRst = self.catchm.waterlopenkrt
            if waterlopenRst is not None:
                water_arr = readRstAsArr(waterlopenRst)
                water_arr = np.where(water_arr != nd, 0, nd)
                C_arr = np.where(C_arr == nd, water_arr, C_arr)

            # infrastructuur
            infrastructuurRst = self.catchm.infrTif
            if infrastructuurRst is not None:
                infra_arr = readRstAsArr(infrastructuurRst)
                infra_arr = np.where(infra_arr == -2, 0, infra_arr)
                infra_arr = np.where(infra_arr == -7, 0.1, infra_arr)
                C_arr = np.where(C_arr == nd, infra_arr, C_arr)

            # grasstrips
            if self.grasshp[year] is not None:
                df_grasstrips = gpd.read_file(self.grasshp[year])
                res = self.catchm.RasterProp['res']
                df_grasstrips['BREEDTE'] = np.where(df_grasstrips.BREEDTE >= res, res, df_grasstrips.BREEDTE)
                df_grasstrips['C_fctr_upstr'] = 0.37
                df_grasstrips['C_factor'] = (0.01 * (df_grasstrips.BREEDTE / res)) + (df_grasstrips.C_fctr_upstr * ((res - df_grasstrips.BREEDTE) / res))
                grasarr = readRstAsArr(self.grasrst[year])

                for row in df_grasstrips.itertuples():
                    rstval = getattr(row, 'NR')
                    C_factor = getattr(row, 'C_factor')
                    grasarr = np.where(grasarr == rstval, C_factor, grasarr)
                C_arr = np.where(C_arr == nd, grasarr, C_arr)

            # parcels
            if self.prcrst[year] is not None:
                prc_arr = readRstAsArr(self.prcrst[year])

                df = self.perceelsdf[year].copy()

                if self.ModelOptions['Calibrate'] == 1:
                    df['C_factor'] = 0
                    df['C_factor'] = np.where(df.Lndgbrk == -3, 0.001, df.C_factor)
                    df['C_factor'] = np.where(df.Lndgbrk == -4, 0.01, df.C_factor)
                    df['C_factor'] = np.where(df.Lndgbrk == -6, 0.01, df.C_factor)
                    df['C_factor'] = np.where(df.Lndgbrk == nd , 0.37, df.C_factor)
                else:
                    # waar geen C-factor is, die op 0.37 zetten
                    df['C_factor'] = np.where(df['C_factor'].isnull(), 0.37, df['C_factor'])

                    # correctie niet-kerende bodembewerking: C wordt met 80% verminderd
                    if 'ntkerend' in df.columns:
                        df['C_factor'] = np.where(df['ntkerend'] == 1, df['C_factor'] * 0.2, df['C_factor'])

                    # TO DO: reductie drempels
                    # TO DO: reductie contourploegen
                    # TO DO: reductie groenbedekker
                    # TO DO: correctie C-factor voor grasstroken

                for row in df.itertuples():
                    rstval = getattr(row, 'NR')
                    C_factor = getattr(row, 'C_factor')
                    prc_arr = np.where(prc_arr == rstval, C_factor, prc_arr)
                C_arr = np.where(C_arr == nd, prc_arr, C_arr)

            # other landuse
            if self.catchm.LandUse is not None:
                land_arr = readRstAsArr(self.catchm.LandUse)
                # reclass landarr to C-factors
                land_arr = np.where(land_arr == -1, 0, land_arr)
                land_arr = np.where(land_arr == -2, 0, land_arr)
                land_arr = np.where(land_arr == -3, 0.001, land_arr)
                land_arr = np.where(land_arr == -4, 0.01, land_arr)
                land_arr = np.where(land_arr == -5, 0, land_arr)
                land_arr = np.where(land_arr == -6, 0.01, land_arr)
                land_arr = np.where(land_arr == 10, 0.37, land_arr)
                C_arr = np.where(C_arr == nd, land_arr, C_arr)

            # last posible pixels
            binarr = np.where(self.catchm.binarr == 1, 0.37, 0)
            C_arr = np.where(C_arr == nd, binarr, C_arr)
            C_arr = C_arr.astype('float32')

            tempfolder = os.path.join(self.sfolder, str(year))
            os.chdir(tempfolder)

            CmapTemp = 'Ckaart_%s_%s_%s_scenario_%s.tif' % (self.catchm.naam, year, season, self.nr)
            write_arr_as_rst(C_arr, CmapTemp, 'float32', self.catchm.rstparams)

            if len(self.Ckaarten.keys()) == 0:
                self.Ckaarten[year] = {}
            elif not (year in self.Ckaarten.keys()):
                self.Ckaarten[year] = {}

            self.Ckaarten[year][season] = os.path.join(self.infolder, CmapTemp[:-4] + '.rst')
            CmapTemp = os.path.join(tempfolder, CmapTemp)
            Tiff2Rst(CmapTemp, self.Ckaarten[year][season], 'float')
        return

    def CreateCNkaart(self, year, season, CNnumbers):
        """
        Creates the CN-map for a given year and season. It sets the CNkaarten[year][season]-argument.
        :param year: int, the year to be processed
        :param season: str, the season to be processed
        :param CNnumbers: csv-file, delimeter ';'
        :return: nothing
        """
        if CNnumbers is not None:
            self.logger.info('Aanmaken CN-kaart...')
            if self.perceelsdf[year] is None:
                msg = 'ERROR: perceelsdf niet aangemaakt!'
                raise CNWSException(msg)
            elif self.prckrt[year] is None:
                msg = 'ERROR: Geen perceelskaart voor CN-kaart aan te maken'
                raise CNWSException(msg)
            elif self.catchm.CNbodem is None:
                msg = 'ERROR: CNBodemkaart is nog niet aangemaakt!'
                raise CNWSException(msg)
            else:
                df = self.perceelsdf[year].copy()
            
                # maak een contour_id per perceel op basis van het CN-type (1-11) en de contouroptie van de percelen
                df['contour_id'] = 0  # standaardwaarde
                sel = df.CNtype_id.isin([2, 3, 4]) & df.contour.isin([0])  # CNtype 2,3 of 4 en geen contourploegen
                df['contour_id'] = np.where(sel == True, 1, df.contour_id)  # contour_id krijgt 1 (SR)
                sel = df.CNtype_id.isin([2, 3, 4]) & df.contour.isin([1])  # CNtype 2,3 of 4 en contourploegen
                df['contour_id'] = np.where(sel == True, 2, df.contour_id)  # contour_id krijgt 2 (C)
                
                # hydrologische toestand van de bodem op het perceel
                df['HydroCond_id'] = 0  # standaardwaarde
                df['HydroCond_id'] = np.where(df.HydroCond == 'Poor', 1, df.HydroCond_id)
                df['HydroCond_id'] = np.where(df.HydroCond == 'Fair', 2, df.HydroCond_id)
                df['HydroCond_id'] = np.where(df.HydroCond == 'Good', 3, df.HydroCond_id)
        
                # gewasrest
                df['gewasrest_id'] = np.where(df.gewasrest == 1, 1, 0)
        
                # zet alle percelen waar geen gegevens (onbekende teelt) voor zijn op onbekend
                sel = df['CNtype_id'].isnull()
                df['CNtype_id'] = np.where(sel == True, 4, df.CNtype_id)
                df['HydroCond_id'] = np.where(sel == True, 1, df.HydroCond_id)
                df['contour_id'] = np.where(sel == True, 1, df.contour_id)
        
                df['CNmaxID'] = (df.CNtype_id.astype(int).map(str) + 
                                 df.contour_id.map(str) +
                                 df.HydroCond_id.map(str) +
                                 df.gewasrest_id.map(str))
                df['CNmaxID'] = df['CNmaxID'].astype(int)
                
                df_CN = pd.read_csv(CNnumbers, sep=";")
                df = pd.merge(df, df_CN, how='left', on='CNmaxID')
        
                # correctiefactoren cover en verslemping
                # geen gegevens, dus 0 --> geen correctie bij berekening CN
                df['cover'] = 0
                df['verslemp'] = 0

                bodemklasmap = readRstAsArr(self.catchm.CNbodem)
                
                bodemklassen = np.unique(bodemklasmap).tolist()  # de bodemklassen aanwezig in het modelgebied
        
                # voor elk perceel wordt voor elke bodemklasse de CN-waarde berekend
                c2 = 3
                for bodemklasse in bodemklassen:
                    CNmin = 'CNmin_%s' % int(bodemklasse)
                    CNmax = 'CNmax_%s' % int(bodemklasse)
                    c1 = 'c1_%s'%int(bodemklasse)
                    CNmodel = 'CNmodel_%s' % int(bodemklasse)
        
                    df[CNmin] = (4.2*df[CNmax]/(10-0.058*df[CNmax]))
                    df[c1] = df[CNmax] - df[CNmin]
                    df[CNmodel] = df[CNmax] - ((df.cover/100)*df[c1]) + ((df.verslemp/5)*c2)
                    df[CNmodel] = np.where(df.ntkerend == 1, df[CNmodel]-1, df[CNmodel])  # correctie niet kerende bodembewerking
        
                os.chdir(self.sfolder)
                # df.to_csv('df_CN_reclass.csv', sep = ';')  # for debugging

                perc_arr = readRstAsArr(self.prckrt[year])
                
                # reclass perceelskaart --> kan beter! df.apply?
                for row in df.itertuples():
                    rstval = getattr(row, 'NR')
                    for bodemklasse in bodemklassen:
                        newval = getattr(row, 'CNmodel_%s' % int(bodemklasse))
                        CNmap = np.where(np.logical_and(perc_arr == rstval, bodemklasmap == bodemklasse),
                                         newval, perc_arr)
                        perc_arr = CNmap.copy()
        
                CNmap = np.where(CNmap == -1, 99, CNmap)
                CNmap = np.where(CNmap == -2, 99, CNmap)  # wegen en water krijgen CN 99
                CNmap = np.where(CNmap == -5, 0, CNmap)  # vijvers en poelen krijgen CN 0
        
                # bossen, weiden en grasland die niet op de landbouwperceelskaart staan
                ids = {-3: 9030, -4: 5020, -6: 6000}
                for lnduse in ids:
                    z = df_CN.loc[df_CN.CNmaxID == ids[lnduse]].copy()
                    for bodemklasse in bodemklassen:
                        newval = int(z['CNmax_%s' % int(bodemklasse)])
                        CNmap = np.where(np.logical_and(CNmap == lnduse, bodemklasmap == bodemklasse), 
                                         newval, CNmap)
        
                CNkrt = 'CNmap_%s_%s_%s_scenario_%s.tif' % (self.catchm.naam, year, season, self.nr)
                write_arr_as_rst(CNmap, CNkrt, 'float32', self.catchm.rstparams)

                CN_rst = os.path.join(self.infolder, CNkrt[:-4] + '.rst')
                CNkrt = os.path.join(self.sfolder, CNkrt)
                Tiff2Rst(CNkrt, CN_rst, 'float')
        
                if len(self.CNkaarten.keys()) == 0:
                    self.CNkaarten[year] = {}
                elif not (year in self.CNkaarten.keys()):
                    self.CNkaarten[year] = {}
                self.CNkaarten[year][season] = CN_rst
        else:
            msg = 'ERROR: file with CN-values is missing!'
            raise CNWSException(msg)
        return

    def CreateBuffermap(self):
        """
        Makes a raster with all pixels representing a buffer-item. All pixels of each item have the same id, except the
        lowest  pixel of the item. The lowest pixel contains the ExtensionID (100* ID). The path of the raster is stored
        in self.bufferkaart. If no raster is made (no buffers present in the modeldomain) self.bufferkaart will contain
        an empty string and the UseBuffers key in EBMOptions is set to 0.
        :return: Nothing
        """
        self.logger.info('Aanmaken bufferkaart...')
        self.bufferData = []

        try:
            df = gpd.read_file(self.buffershp)
        except OSError:
            msg = 'failed to open %s ' %self.buffershp
            raise CNWSException(msg)
        except AttributeError:  # raised if self.buffershp is None
            self.logger.warning('No buffers available in scenario!')
            self.EBMOptions['UseBuffers'] = 0
            return
        else:
            tot_buffers = df.shape[0]
            # 1. creating bufferdata list en id's of the buffers
            if tot_buffers == 0:
                self.EBMOptions['UseBuffers'] = 0
            else:
                self.bufferData = []
                df['BUF_ID'] = range(1, df.shape[0]+1)
                df['BUF_EXID'] = df['BUF_ID']*100
                df.to_file(self.buffershp)
                
                for Buffer in df.itertuples():
                    Volume = getattr(Buffer, 'Buffercap')
                    Hoogte_dam = getattr(Buffer, 'HDam')
                    Hoogte_opening = getattr(Buffer, 'HKnijp')
                    Opp_opening = getattr(Buffer, 'DKnijp')
                    Discharge_coef = getattr(Buffer, 'QCoef')
                    Breedte_dam = getattr(Buffer, 'Boverl')
                    Trapping_eff = getattr(Buffer, 'Eff')
                    buf_id = getattr(Buffer, 'BUF_ID')
                    buf_exid = getattr(Buffer, 'BUF_EXID')
        
                    a = [buf_id, Volume, Hoogte_dam, Hoogte_opening, Opp_opening, 
                         Discharge_coef, Breedte_dam, Trapping_eff,  buf_exid]
                    self.bufferData.append(a)
                    
                # 2. create a raster with the extension ID's and a raster with the buffer ID's
                Cnst = self.catchm.RasterProp
                os.chdir(self.sfolder)
                rst_extID = 'buf_extensionID_scenario_%s.tif' % self.nr
                Shp2Rst_field(self.buffershp, rst_extID, Cnst, 'BUF_EXID')  # zet de extenison-ID's in een raster
                
                # convert rasters to numpy arrays
                bufferRstArr = readRstAsArr(rst_extID)
                dtm_arr = readRstAsArr(self.DTMkaart)
                
                for Buffer in range(0,tot_buffers):
                    Ext_ID = self.bufferData[Buffer][8]  # het extension-ID van de buffer
                    Buf_ID = self.bufferData[Buffer][0]  # de buffer-ID van de buffer
                    
                    dtm_buf = np.where(bufferRstArr == Ext_ID, dtm_arr, 99999999)  # waar de bufferID voorkomt, de waarde van het dtm zetten. anders heel hoge waarde zetten
                    minH = dtm_buf.min()  # bepalen van de hoogte van de laagste pixel binnen een buffer
                    x = np.where(dtm_buf == minH)[0][0]  # x-indice van de eerste pixel met de min-waarde
                    y = np.where(dtm_buf == minH)[1][0]  # y-indice van de eerste pixel met de min-waarde
                    bufferRstArr[x][y] = Buf_ID  # zet de eerste pixel met de min-hoogtewaarde gelijk aan Buf_ID

                bufferRstArr = np.where(bufferRstArr == Cnst['nodata'], 0, bufferRstArr).astype('int16')
                bufferTif = os.path.join(self.sfolder, 'buffers_%s_scenario_%s.tif' % (self.catchm.naam, self.nr))
                write_arr_as_rst(bufferRstArr, bufferTif, 'int16', self.catchm.rstparams)

                self.bufferkaart = os.path.join(self.infolder, 'buffers_%s_scenario_%s.rst' % (self.catchm.naam, self.nr))
                Tiff2Rst(bufferTif, self.bufferkaart, 'integer')
        return   

    def CreateGrachtenOrGDammen(self, Type):
        """
        Creates a raster containing the routing next to a ditch or dam.
        Sets scenario.grachtenkaart or scenario.gdammenkaart.
        :param Type: str, can contain 'grachten' (ditches) or 'gdammen' (dams).
        :return: Notthing
        """
        if Type == 'grachten':
            inShp = self.grachtenshp
            keyword = 'UseGrachten'
        elif Type == 'gdammen':
            inShp = self.gdammenshp
            keyword = 'UseGeleidendeDammen'
        else:
            msg = 'No valid Type chosen! Type must be grachten or gdammen.'
            raise CNWSException(msg)

        try:
            gdf = gpd.read_file(inShp)
        except OSError:
            msg = 'Could not open %s ' %inShp
            msg += '%s will be ignored'
            self.logger.warning(msg)
            self.EBMOptions[keyword] = 0
        except AttributeError:  # raised if inShp is none
            self.logger.warning('No %s available in scenario!' % Type)
            self.EBMOptions[keyword] = 0
            return
        else:
            if gdf.shape[0] == 0:
                self.logger.warning('No %s available in scenario!' % Type)
                self.EBMOptions[keyword] = 0
            else:
                if 'NR' not in gdf.columns:
                    gdf['NR'] = range(1, gdf.shape[0] + 1)
                gdf.to_file(inShp)

                pointshp = '%s_points_scenario_%s.shp' % (Type, self.nr)
                pointshp = os.path.join(self.sfolder, pointshp)
                distance = self.catchm.RasterProp['res'] / 2

                Lines2Points(inShp, pointshp, distance)

                gdf = gpd.read_file(pointshp)
                gdf['PTNR'] = range(1, gdf.shape[0] + 1)
                gdf.to_file(pointshp)

                points2rst = 'rasterizedpoints_%s_scenario_%s.tif' % (Type, self.nr)
                points2rst = os.path.join(self.sfolder, points2rst)
                Shp2Rst_field(pointshp, points2rst, self.catchm.RasterProp, 'PTNR', False)

                arr = readRstAsArr(points2rst)
                val = arr.flatten()
                values = np.unique(val).tolist()
                values.remove(self.catchm.RasterProp['nodata'])
                newarr = np.zeros((self.catchm.RasterProp['nrows'], self.catchm.RasterProp['ncols']))

                for i in range(0, len(values) - 1):
                    pos_from = np.argwhere(arr == values[i])
                    pos_to = np.argwhere(arr == values[i + 1])
                    from_row = pos_from[0][0]
                    from_col = pos_from[0][1]
                    to_row = pos_to[0][0]
                    to_col = pos_to[0][1]

                    # np array begint links boven te tellen!
                    if from_row + 1 == to_row:
                        if from_col == to_col:
                            # punt er onder
                            newval = 5
                        elif from_col - 1 == to_col:
                            # punt links onder
                            newval = 6
                        elif from_col + 1 == to_col:
                            # rechts onder
                            newval = 4
                        else:
                            newval = 0
                    elif from_row == to_row:
                        if from_col - 1 == to_col:
                            # punt links naast
                            newval = 7
                        elif from_col + 1 == to_col:
                            # punt rechts naast
                            newval = 3
                        else:
                            newval = 0
                    elif from_row - 1 == to_row:
                        if from_col == to_col:
                            # punt er boven
                            newval = 1
                        elif from_col - 1 == to_col:
                            # punt links boven
                            newval = 8
                        elif from_col + 1 == to_col:
                            # rechts boven
                            newval = 2
                        else:
                            newval = 0
                    else:  # pixel springt! kan niet --> gracht stopt
                        newval = 0

                    newarr = np.where(arr == values[i], newval, newarr)

                outRst_tmp = 'routing_%s_scenario_%s.tif' % (Type, self.nr)
                outRst_tmp = os.path.join(self.sfolder, outRst_tmp)
                write_arr_as_rst(newarr, outRst_tmp, 'float64', self.catchm.rstparams)
                outRst = outRst_tmp[:-4] + '.rst'
                outRst = os.path.split(outRst)[1]
                outRst = os.path.join(self.infolder, outRst)
                Tiff2Rst(outRst_tmp, outRst, 'integer')

                if Type == 'grachten':
                    self.grachtenkaart = outRst
                elif Type == 'gdammen':
                    self.gdammenkaart = outRst
        return

    def CreateIni(self):
        """
        Creates an ini-file for the scenario
        sets the self.ini argument
        :return: Nothing
        """

        self.logger.info('Aanmaken ini-file...')
        self.ini = os.path.join(self.infolder, 'ini_%s_scenario_%s.ini' % (self.catchm.naam, self.nr))
        Cfg = configparser.ConfigParser()
        Cfg.add_section('Working directories')
        Cfg.set('Working directories', 'Input directory', self.infolder)
        Cfg.set('Working directories', 'Output directory', self.outfolder)
        
        Cfg.add_section('Files')
        Cfg.set('Files', 'DTM filename', os.path.split(self.DTMkaart)[1])
        Cfg.set('Files', 'P factor map filename', os.path.split(self.Pkaart)[1])
        Cfg.set('Files', 'shapefile catchment', self.catchm.shp)

        if len(self.jaren) == 1:
            jaar = self.jaren[0]
            seizoen = self.seizoenen[0][0]
            Cfg.set('Files', 'Parcel filename', os.path.split(self.prckrt[jaar])[1])
            if self.versie != 'RoutingOnly':
                Cfg.set('Files', 'C factor map filename', os.path.split(self.Ckaarten[jaar][seizoen])[1])
        else:
            for i, jaar in enumerate(self.jaren):
                Cfg.set('Files', 'Parcel filename %s' % (i + 1), os.path.split(self.prckrt[jaar])[1])
                if self.versie != 'RoutingOnly':
                    for seizoen in ['spring', 'summer', 'fall', 'winter']:
                        Ckrt = self.Ckaarten[jaar][seizoen]
                        if Ckrt is None:
                            Ckrt = ''
                        else:
                            Ckrt = os.path.split(Ckrt)[1]
                        Cfg.set('Files', 'C factor map %s %s' % (seizoen, i + 1), Ckrt)
        
        Cfg.add_section('User Choices')
        Cfg.set('User Choices', 'Max kernel', str(self.Variables['Max_kernel']))
        Cfg.set('User Choices', 'Max kernel river', str(self.Variables['Max_kernel_river']))

        Cfg.add_section('Output maps')
        Cfg.set('Output maps', 'Write aspect', str(self.Output['WriteAspect']))
        Cfg.set('Output maps', 'Write LS factor', str(self.Output['WriteLS']))
        Cfg.set('Output maps', 'Write rainfall excess', str(self.Output['WriteRainExcess']))
        Cfg.set('Output maps', 'Write RUSLE', str(self.Output['WriteRUSLE']))
        Cfg.set('Output maps', 'Write sediment export', str(self.Output['WriteSedExport']))
        Cfg.set('Output maps', 'Write slope', str(self.Output['WriteSlope']))
        Cfg.set('Output maps', 'Write tillage erosion', str(self.Output['WriteTillageErosion']))
        Cfg.set('Output maps', 'Write total runoff', str(self.Output['WriteTotalRunoff']))
        Cfg.set('Output maps', 'Write upstream area', str(self.Output['WriteUpstreamArea']))
        Cfg.set('Output maps', 'Write water erosion', str(self.Output['WriteWaterErosion']))
        Cfg.set('Output maps', 'Write routing table', str(self.Output['WriteRouting']))

        Cfg.add_section('Variables')
        Cfg.set('Variables', 'Parcel connectivity cropland', str(int(self.Variables['Parcel connectivity cropland'])))
        Cfg.set('Variables', 'Parcel connectivity forest', str(int(self.Variables['Parcel connectivity forest'])))
        Cfg.set('Variables', 'Parcel trapping efficiency cropland', str(int(self.Variables['Parcel trapping efficiency cropland'])))
        Cfg.set('Variables', 'Parcel trapping efficiency forest', str(int(self.Variables['Parcel trapping efficiency forest'])))
        Cfg.set('Variables', 'Parcel trapping efficiency pasture', str(int(self.Variables['Parcel trapping efficiency pasture'])))

        if self.versie == 'CNWS':
            Cfg.set('User Choices', 'Simplified model version', '0')
            Cfg.set('Variables', "Alpha", str(self.Variables['Alpha']))
            Cfg.set('Variables', "Beta", str(self.Variables['Beta']))
            Cfg.set('Variables', "Stream velocity", str(self.Variables['velocity']))
            if len(self.jaren) == 1:
                jaar = self.jaren[0]
                seizoen = self.seizoenen[0][0]
                Cfg.set('Files', 'CN map filename', os.path.split(self.CNkaarten[jaar][seizoen])[1])
            else:
                for i, jaar in enumerate(self.jaren):
                    for seizoen in ['spring', 'summer', 'fall', 'winter']:
                        CNkrt = self.CNkaarten[jaar][seizoen]
                        if CNkrt is None:
                            CNkrt = ''
                        else:
                            CNkrt = os.path.split(CNkrt)[1]
                        Cfg.set('Files', 'CN map %s %s' % (seizoen, i + 1), CNkrt)
        else: # WS-model
            Cfg.set('User Choices', 'Simplified model version', '1')
            if self.versie == 'RoutingOnly':
                Cfg.set('User Choices', 'Only Routing', '1')

        # output per rivier segment?
        Cfg.set('User Choices', 'Output per VHA river segment', str(self.Output['OutputPerSegment']))
        if self.Output['OutputPerSegment'] == 1:
            Cfg.set('Files', 'River segment filename', os.path.split(self.riviersegmkrt)[1])

        # manual outlet
        Cfg.set('User Choices', 'Manual outlet selection', str(self.ModelOptions['ManualOutlet']))
        if self.ModelOptions['ManualOutlet'] == 1:
            Cfg.set('Files', 'Outlet map filename', self.outletmap)

        if self.versie != 'RoutingOnly':
            Cfg.set('Files', 'K factor filename', os.path.split(self.Kkaart)[1])
            Cfg.set('Variables', "Bulk density", str(int(self.Variables['Bulkdensity'])))
            Cfg.set('User Choices', 'Use R factor', str(self.ModelOptions['UseR']))

            # R-factor of regenvalfile?
            if self.versie == 'WS' and self.ModelOptions['UseR'] == 1:
                Cfg.set('Variables', "R factor", str(self.Variables['R']))
                Cfg.set('Variables', 'Endtime model', '0')  # Aan te passen in model?
            else:  # regenvalfile
                Cfg.set('Files', 'Rainfall filename', os.path.split(self.Variables['Rainfall_file'])[1])
                Cfg.set('Variables', "5-day antecedent rainfall", str(self.Variables['5dayRainfall']))
                Cfg.set('Variables', 'Desired timestep for model', str(self.Variables['Timestep']))
                Cfg.set('Variables', 'Endtime model', str(self.Variables['Endtime']))
                if self.ModelOptions['ConvertOutput'] == 1:
                    Cfg.set('User Choices', 'Convert output', str(self.ModelOptions['ConvertOutput']))
                    Cfg.set('Variables', 'Final timestep output', str(self.Variables['Timestep_output']))

            # KTC and calibration KTC
            if self.ModelOptions['Calibrate'] == 1:
                Cfg.add_section('Calibration')
                Cfg.set('Calibration', 'Calibrate', str(self.ModelOptions['Calibrate']))
                Cfg.set('Calibration', 'KTcHigh_lower', str(self.Variables['KTcHigh_lower']))
                Cfg.set('Calibration', 'KTcHigh_upper', str(self.Variables['KTcHigh_upper']))
                Cfg.set('Calibration', 'KTcLow_lower', str(self.Variables['KTcLow_lower']))
                Cfg.set('Calibration', 'KTcLow_upper', str(self.Variables['KTcLow_upper']))
                Cfg.set('Calibration', 'steps', str(self.Variables['steps']))
                Cfg.set('Variables', "ktc limit", str(self.Variables['ktc_limit']))
            else:
                Cfg.set('User Choices', 'Create ktc map', '1')
                Cfg.set('Variables', "ktc low", str(self.Variables['ktc_low']))
                Cfg.set('Variables', "ktc high", str(self.Variables['ktc_high']))
                Cfg.set('Variables', "ktc limit", str(self.Variables['ktc_limit']))

            # KTIL
            Cfg.set('User Choices', 'Create ktil map', str(self.ModelOptions['CreateKTIL']))
            if self.ModelOptions['CreateKTIL'] == 1:
                Cfg.set('Variables', "ktil default", str(self.Variables['ktil_default']))
                Cfg.set('Variables', "ktil threshold", str(self.Variables['ktil_threshold']))

            # Estimating clay content in model?
            Cfg.set('User Choices', 'Estimate clay content', str(self.ModelOptions['EstimClay']))
            if self.ModelOptions['EstimClay'] == 1:
                Cfg.set('Variables', 'Clay content parent material', str(int(self.Variables['ClayContent'])))

        # Grachten gebruiken?
        Cfg.set('User Choices', 'Include ditches', str(self.EBMOptions['UseGrachten']))
        if self.EBMOptions['UseGrachten'] == 1:
            Cfg.set('Files', 'Ditch map filename', os.path.split(self.grachtenkaart)[1])

        # Geleidende dammen gebruiken?
        Cfg.set('User Choices', 'Include dams', str(self.EBMOptions['UseGeleidendeDammen']))
        if self.EBMOptions['UseGeleidendeDammen'] == 1:
            Cfg.set('Files', 'Dam map filename', os.path.split(self.gdammenkaart)[1])

        # Using buffers in model?
        Cfg.set('User Choices', 'Include buffers', str(self.EBMOptions['UseBuffers']))
        if self.EBMOptions['UseBuffers'] == 1:
            Cfg.set('Files', 'Buffer map filename', os.path.split(self.bufferkaart)[1])
            Cfg.set('Variables', "Number of buffers", str(len(self.bufferData)))
            if len(self.bufferData) != 0:
                for buffer_ in self.bufferData:
                    sectie = 'Buffer %s' % buffer_[0]
                    Cfg.add_section(sectie)
                    Cfg.set(sectie, "Volume", str(buffer_[1]))
                    Cfg.set(sectie, "Height dam", str(buffer_[2]))
                    Cfg.set(sectie, "Height opening", str(buffer_[3]))
                    Cfg.set(sectie, "Opening area", str(buffer_[4]))
                    Cfg.set(sectie, "Discharge coefficient", str(buffer_[5]))
                    Cfg.set(sectie, "Width dam", str(buffer_[6]))
                    Cfg.set(sectie, "Trapping efficiency", str(buffer_[7]))
                    Cfg.set(sectie, "Extension ID", str(buffer_[8]))

        # force routing
        Cfg.set('User Choices', 'Force Routing', str(self.ModelOptions['Force Routing']))
        if self.ModelOptions['Force Routing'] == 1:
            Cfg.set('Variables', 'Number of Forced Routing', str(len(self.forcedroutingdata)))
            for row in self.forcedroutingdata.itertuples():
                sectie = 'Forced Routing %s' % getattr(row, 'NR')
                Cfg.add_section(sectie)
                Cfg.set(sectie, 'from col', str(int(getattr(row, 'fromcol'))))
                Cfg.set(sectie, 'from row', str(int(getattr(row, 'fromrow'))))
                Cfg.set(sectie, 'target col', str(int(getattr(row, 'tocol'))))
                Cfg.set(sectie, 'target row', str(int(getattr(row, 'torow'))))

        # river routing
        if self.ModelOptions['River Routing'] == 1:
            Cfg.set('User Choices', 'river routing', str(self.ModelOptions['River Routing']))
            Cfg.set('Files', 'adjectant segments', os.path.split(self.adj_edges)[1])
            Cfg.set('Files', 'upstream segments', os.path.split(self.up_edges)[1])
            Cfg.set('Files', 'river routing filename', os.path.split(self.riverrouting)[1])

        # Advanced settings
        if 'L model' in self.ModelOptions.keys():
            Cfg.set('User Choices', 'L model', self.ModelOptions['L model'])
        if 'S model' in self.ModelOptions.keys():
            Cfg.set('User Choices', 'S model', self.ModelOptions['S model'])

        Cfg.set('User Choices', 'Adjusted Slope', str(self.ModelOptions['Adjusted Slope']))
        Cfg.set('User Choices', 'Buffer reduce Area', str(self.ModelOptions['Buffer reduce Area']))

        f = open(self.ini, 'w')
        Cfg.write(f)
        f.close()
        return

    def CreateModelInput(self, gwsdata, EBMDicts):
        """
        Creates, based on scenario.options and scenario.EBMoptions the needed modelinput for a scenario.
        :param gwsdata: path to csv-file containing crop-specific data (e.g. C-factors)
        :param EBMDicts: Dict with keys for different years. Every key refers to a list containing different EBMdicts.
        :return: nothing
        """
        self.logger.info('Aanmaken van alle nodige modelinput...')
        for i, jaar in enumerate(self.jaren):
            self.MergeEBM(jaar, EBMDicts[jaar])
            if self.EBMOptions['UseTeelttechn'] == 1:
                self.AddTeelttechn2Percelen(jaar)

            if self.EBMOptions['UseGras'] == 1:
                self.PrepareGrasEBM(jaar)

            if self.ModelOptions['ManualOutlet'] == 1:
                self.CreateOutletMap()
            else:
                self.outletmap = os.path.join(self.infolder, 'Outlet.rst')

            if self.ModelOptions['Force Routing'] == 1:
                self.CreateForceRoutingData()

            self.CreatePerceelskaart(jaar)

            if self.versie != 'RoutingOnly':
                self.CreateTeeltData(gwsdata, jaar)
                for seizoen in self.seizoenen[i]:
                    self.CreateCkaart(jaar, seizoen)

                    if self.versie == 'CNWS':
                        self.CreateCNkaart(jaar, seizoen, self.Variables['CNTable'])
                    else:
                        if not (jaar in self.CNkaarten.keys()):
                            self.CNkaarten[jaar] = {}
                        self.CNkaarten[jaar][seizoen] = ''

        if self.versie != 'RoutingOnly':
            if self.ModelOptions['Calibrate'] == 1:
                for key in ['WriteRUSLE', 'WriteSedExport', 'WriteTillageErosion',
                            'WriteWaterErosion', 'OutputPerSegment']:
                    self.Output[key] = 0

        if self.EBMOptions['UseGrachten'] == 1:
            self.CreateGrachtenOrGDammen('grachten')

        if self.EBMOptions['UseGeleidendeDammen'] == 1:
            self.CreateGrachtenOrGDammen('gdammen')

        if self.EBMOptions['UseBuffers'] == 1:
            self.CreateBuffermap()

        if self.ModelOptions['FilterDTM'] == 1:
            self.filterDTM(self.jaren[0])

        if self.ModelOptions['River Routing'] == 1:
            self.Output['OutputPerSegment'] = 1
            # self.catchm.TopologizeRivers()
            if self.catchm.adj_edges is not None:
                self.adj_edges = os.path.split(self.catchm.adj_edges)[1]
                self.adj_edges = os.path.join(self.infolder, self.adj_edges)
                shutil.copy2(self.catchm.adj_edges, self.adj_edges)
            if self.catchm.up_edges is not None:
                self.up_edges = os.path.split(self.catchm.up_edges)[1]
                self.up_edges = os.path.join(self.infolder, self.up_edges)
                shutil.copy2(self.catchm.up_edges, self.up_edges)

        self.CreateIni()
        return
    
    def RunModel(self):
        """
        Run the CN-WS model
        :return: Nothing
        """
        self.logger.info('Modeleren van scenario %s' % self.nr)
        try:
            cmd_str = 'CN_WSmodel "%s"' % self.ini
            subprocess.check_call(cmd_str)
            self.logger.info('Modelrun finished!')
        except subprocess.CalledProcessError as e:
            msg = 'Failed to run CNWS-model'
            self.logger.error(msg)
            self.logger.error(e.cmd)
            raise CNWSException(msg)
        return
    

class PostProcessScenario:
    """
    PostProcessScenario contains all tools for postprocessing modeloutput
    """
    def __init__(self, scenario):
        """
        Construct a new PostProcessScenario object
        :param scenario: instance of scenario-class
        """
        self.scenario = scenario
        self.folder = scenario.ppfolder

        self.logger = logging.getLogger(__name__)

        self.segmShp = None
        self.jumpShp = None
        self.rusleRST = None
        self.upareaRST = None
        self.waterosRST = None
        self.tilerosRST = None
        self.sedExportRST = None
        self.sedInRST = None
        self.sedOutRST = None
        self.subcatchmRST = None
        self.subcatchmShp = None
        self.slope = None
        self.remap = None
        self.totalrunoff = None
        self.routingshp = None
        self.totalresults = None
        self.outletresults = None
        self.cumulativeRST = None

        self.sinksRST = None
        self.subcatchmsinksRST = None
        self.subcatchmsinksShp = None
        self.routingsedoutshp = None

        self.perceelRST = {}

    def SourceSinksAlgorithm(self, percentage=20):
        # (SG) identify sinks from cumulative sedimend load
        self.identifySinks(percentage)

        # (DR) remove routing in river
        routingfile = self.RemoveRiverRouting()

        # (SG) identify subcatchments via generated sinks
        self.subcatchmsinksRST, self.subcatchmsinksShp = self.DefineSubcatchments_saga(self.sinksRST, routingfile, fname='subcatchments_sources_%s' % percentage)

        # (SG) aggregate sediment output per parcel
        SediOutParcelLST, profile = self.identifyExportParcel()

        subcatchmentsRST = readRstAsArr(self.subcatchmsinksRST)
        subcatchmentsLST = rasterToList(subcatchmentsRST, profile)

        # (SG) identify unique id's present in subcatchments
        # (SG) expection: val not equal to -99999. (loaded through gdal)
        nodata = self.scenario.catchm.RasterProp['nodata']

        subcatchmentsLST = subcatchmentsLST[(subcatchmentsLST["val"] != nodata) & subcatchmentsLST["val"] != 0]
        subcatchmentsLST = subcatchmentsLST.merge(SediOutParcelLST[["col", "row", "id_source"]], on=["col", "row"],
                                                  how="left")
        unique_ids = subcatchmentsLST.loc[~subcatchmentsLST["id_source"].isnull(), "id_source"].unique()
        SediOutParcelLST.loc[~SediOutParcelLST["id_source"].isin(unique_ids), "SediOut"] = profile["nodata"]
        # (SG) write to disk
        fname = os.path.join(self.folder, "SedoutSinks.rst")
        writeRaster(SediOutParcelLST, fname, profile, dfcol="SediOut")
        return

    def RemoveRiverRouting(self):
        if self.routingshp is None:
            self.MakeRoutingshps_saga()

        gdf = gpd.read_file(self.routingshp)
        gdf['keep'] = np.where((gdf['lnduSource'] == -1), 0, 1)
        gdf = gdf[gdf['keep'] == 1]
        gdf.drop(columns=['lnduSource', 'lnduTarg', 'jump', 'geometry', 'keep', 'targetX', 'targetY', 'sourceX', 'sourceY'], inplace=True)

        routing = 'RoutingWithoutRivers.txt'
        routing = os.path.join(self.folder, routing)
        gdf.to_csv(routing, sep='\t', index=False)
        return routing

    def identifySinks(self, percentage):
        # (SG) load SediExport_kg file
        if self.sedExportRST is None:
            self.sedExportRST = os.path.join(self.scenario.outfolder, "SediExport_kg.rst")

        sedExportRST, sedExportLST, profile = loadRaster(self.sedExportRST,flist=True)

        # (SG) sort and select points
        self.analyseCumulativeSediExport(sedExportLST, profile, percentage, fplot=True)
        return

    def analyseCumulativeSediExport(self, df, profile, percentage, fplot=False):

        # (SG) sort according to values of sediment load into river
        df = df.sort_values("val", ascending=False)
        # (SG) calculate cumulative sum, in percentage
        cond = (df["val"] != profile["nodata"]) & (df["val"] != 0.)
        df.loc[cond, 'cum_sum'] = df.loc[cond, "val"].cumsum()
        df.loc[cond, 'cum_perc'] = 100 * df.loc[cond, "cum_sum"] / df.loc[cond, "val"].sum()
        # (SG) identify location where cum perc is above percentage threshold
        npoints = np.sum([df.loc[cond, 'cum_perc'] < percentage])

        if fplot:
            # (SG) plot cumulative sum accoring to rank
            fig, ax = plt.subplots()
            ax.plot(np.arange(0, len(df.loc[cond, "cum_perc"]), 1), df.loc[cond, "cum_perc"], color=[0.2] * 3,
                    label=r"Cumulative percentage of sediment load (%)")
            ax.plot([npoints, npoints], [0, percentage], ls=":", color=[0.2] * 3)
            ax.plot([0, npoints], [percentage, percentage], ls=":", color=[0.2] * 3,
                    label=r"First ranked pixels %i deliver %i perc. sediment load" % (int(npoints), int(percentage)))
            ax.set_xlabel("Rank of point (-)")
            ax.set_ylabel("Cumulative sediment load (%)")
            ax.legend()
            plt.savefig(os.path.join(self.folder, "cumulativeload.png"), bbox_inches="tight")
            plt.close()

        # (SG) prepare ids for subcatchment delineation
        df["id"] = profile["nodata"]
        cond = (df["cum_perc"] < percentage) & (~df["cum_perc"].isnull())
        df.loc[cond, "id"] = np.arange(1, np.sum(cond) + 1, 1)
        fname = os.path.join(self.folder, "sinks.rst")
        self.sinksRST = fname
        writeRaster(df, fname, profile, dfcol="id", dfformat=np.float32)
        return

    def identifyExportParcel(self):

        # (SG) couple sediment out to routing file
        self.coupleSedoutRouting()

        # (SG) couple parcel id to routing sedi out
        # temp1,lParcel,_ = self.coupleParcelIdRouting(self.routingsedoutshp)
        temp1, lParcel, profile = self.coupleParcelIdRouting()

        # (SG) filter out pixels which route to a different id
        # (SG) only consider positive ids (others are land use)
        # (SG) remove largest parcel id as these are distributed over catchment and unknowns
        temp1 = temp1.loc[temp1['id_source'] != temp1["id_target"]]
        temp1 = temp1.loc[(temp1["id_source"] > 0) & (temp1["id_source"] != np.max(temp1["id_source"]))]

        # (SG) write temp1 file to a routing file
        temp1.to_file(os.path.join(self.folder, "routingOutOfParcel.shp"))

        # (SG) aggregate Parcel
        lSediExportParcel = self.aggregateParcel(temp1, lParcel, profile)

        return lSediExportParcel, profile

    def coupleSedoutRouting(self):
        if self.routingsedoutshp is None:
            # (SG) generate a shape of the routing.txt file and load it
            if self.routingshp is None:
                self.MakeRoutingshps_saga()
            routing = gpd.read_file(self.routingshp)
            # (SG) load sedOut
            if self.sedOutRST is None:
                self.sedOutRST = os.path.join(self.scenario.outfolder, "SediOut_kg.rst")
            _, sedOutLST, profile = loadRaster(self.sedOutRST,flist=True)
            # (SG) merge sediout to routing
            routing = routing.merge(sedOutLST, on=["col", "row"], how="left")
            routing["SediOut"] = deepcopy(routing["val"])
            # (SG) pump out routing again
            self.routingsedoutshp = os.path.join(self.folder, "routing_sediout.shp")
            routing = routing.drop(['val'], axis=1)

            # (DR) multiply SediOut with part1 to get correct amount of sediment in vector
            routing['SediOut'] = routing['SediOut']*routing['part1']

            # (DR) Write cumulative percentage (descending)
            routing = routing.sort_values('SediOut', ascending=False)
            routing['cum_sum'] = routing.SediOut.cumsum()
            routing['cum_perc'] = (routing.cum_sum/routing.SediOut.sum())*100

            routing.to_file(self.routingsedoutshp)
            CreateSpatialIndex(self.routingsedoutshp)
        return

    def coupleParcelIdRouting(self):

        # (SG) flatten parcel file
        _, lParcel, profile = loadRaster(self.scenario.prckrt[self.scenario.jaren[0]],flist=True)

        # (SG) load routing file
        routing = gpd.read_file(self.routingsedoutshp)
        # routing = fname

        # (SG) fix types if necessary
        for i in ["col", "row", "target1row", "target1col"]:
            routing[i] = routing[i].astype(np.float64)
        for i in ["col", "row"]:
            lParcel[i] = lParcel[i].astype(np.float64)
        # (SG) couple parcel ID to routing file, on source and on target
        # (SG) perform some fixes in columns names along the way

        routing = routing.merge(lParcel, on=["col", "row"], how="left")
        routing["id_source"] = deepcopy(routing["val"])
        routing = routing.drop(["val"], axis=1)

        routing = routing.merge(lParcel, left_on=["target1col", "target1row"], right_on=["col", "row"], how="left")
        routing["id_target"] = deepcopy(routing["val"])
        routing = routing.drop(["val"], axis=1)
        routing = routing.drop(["col_x", "row_x"], axis=1)
        routing = routing.rename(columns={"col_y": "col", "row_y": "row"})

        return routing, lParcel, profile

    def aggregateParcel(self, routing, lParcel, profile):

        # (SG) aggregate sediout of routing to parcel scale
        routing = routing.groupby(["id_source"]).aggregate({"SediOut": np.sum}).reset_index()
        # (SG) merge to lParcel
        routing["id_source"] = routing["id_source"].astype(np.float64)
        lParcel = lParcel.merge(routing[["SediOut", "id_source"]], left_on="val", right_on="id_source", how="left")
        lParcel.loc[lParcel["SediOut"].isnull(), "SediOut"] = profile["nodata"]
        lParcel = lParcel.drop(["val"], axis=1)

        return lParcel

    def plotCalibrationFile(self):
        """
        makes a plot of the calibration file

        DOES NOT WORK
        :return: nothing
        """
        if self.scenario.ModelOptions['Calibrate'] == 1:
            calfile = os.path.join(self.scenario.outfolder, 'calibration.txt')

            KtcLow_min = self.scenario.Variables['KTcLow_lower']
            KtcLow_max = self.scenario.Variables['KTcLow_upper']
            KtcHigh_min = self.scenario.Variables['KTcHigh_lower']
            KtcHigh_max = self.scenario.Variables['KTcHigh_upper']
            steps = self.scenario.Variables['steps']

            caldf_template = pd.DataFrame()
            x = np.arange(KtcHigh_min, KtcHigh_max, ((KtcHigh_max - KtcHigh_min)/steps))
            y = np.arange(KtcLow_min, KtcLow_max, ((KtcLow_max - KtcLow_min)/steps))
            X, Y = np.meshgrid(x, y)
            caldf_template['ktc_high'] = X.flatten()
            caldf_template['ktc_low'] = Y.flatten()

            df = pd.read_csv(calfile, sep=';')
            df.drop_duplicates(subset=['ktc_low', 'ktc_high'], inplace=True)

            df = pd.merge(df, caldf_template, how='right', on=['ktc_low', 'ktc_high'])

            arr = np.reshape(df.outlet_1.values, (len(np.unique(df.ktc_low)), len(np.unique(df.ktc_high))))
            extent = (KtcHigh_min, KtcHigh_max, KtcLow_min, KtcLow_max)

            xlabels = np.arange(KtcHigh_min, KtcHigh_max, round(steps / 10))
            ylabels = np.arange(KtcLow_min, KtcLow_max, round(steps / 10))
            plt.xticks(xlabels)
            plt.yticks(ylabels)
            plt.xlabel('ktc_high')
            plt.ylabel('ktc_low')

            plt.imshow(arr, extent=extent)

            Title = 'Calibration %s %s' %(self.scenario.catchm.naam, self.scenario.nr)
            plt.title(Title)
            os.chdir(self.folder)
            plt.savefig(Title + '.png')
            plt.close()
        else:
            msg = 'function not callable if not in calibration mode!'
            self.logger.warning(msg)
        return

    def readSedimentTxt(self):
        """
        Reads Total sediment.txt as pandas DataFrames.
        self.totalresults contains first part of Total sediment.txt
        self.outletresults contains second part of Total sedimen.txt (table with results per outlet)
        :return:
        """
        file = os.path.join(self.scenario.outfolder, 'Total sediment.txt')
        d = {}
        with open(file, 'r') as f:
            for line in f:
                if line.startswith('Total erosion'):
                    d['Total erosion'] = abs(float(line[15:-6]))
                elif line.startswith('Total deposition'):
                    d['Total deposition'] = float(line[18:-6])
                elif line.startswith('Sediment leaving the catchment, via'):
                    d['Sed via river'] = float(line[47:-6])
                elif line.startswith('Sediment leaving the catchment, not'):
                    d['Sed not via river'] = float(line[51:-6])
                elif line.startswith('Sediment trapped in buffers'):
                    d['Sed in buffers'] = float(line[29:-6])
                elif line.startswith('Sediment trapped in open water'):
                    d['Sed in open water'] = float(line[32:-6])

        self.totalresults = pd.DataFrame.from_dict(d, orient='index')
        self.totalresults.rename(columns={0: self.scenario.nr}, inplace=True)

        self.outletresults = pd.read_csv(file, sep='\t', skiprows=9)
        return

    def SetNoData(self, inRst, outRst):
        """
        Set all pixels of a raster outside the modeldomain equal to NoData
        :param inRst: input raster
        :param outRst: output raster
        :return: nothing
        """
        rstarr = readRstAsArr(inRst)
        rstarr = np.where(self.scenario.catchm.binarr == 1, rstarr, self.scenario.catchm.RasterProp['nodata'])

        write_arr_as_rst(rstarr, outRst, 'float32', self.scenario.catchm.rstparams)
        return

    def PlotTimeSeries(self, Type):
        """
        Plot timeseries. All timeseries are stored in the postprocesfolder as a png-file.
        :param Type: string,
                'Q': plot with discharge and rainfall vs time
                'Sedigram': plot with sediment concentration vs time
                'Sediment': plot with total sediment mass vs time
        :return: nothing
        """
        if Type == 'Q':
            baseName = 'Discharge'  # TO DO: ook neerslag!
            baseTitle = 'Debiet in '
            ylable = 'Debiet [m^3/s]'
        elif Type == 'Sedigram':
            baseName = 'Sediment concentration'
            baseTitle = 'Sedigram '
            ylable = 'Sediment concentratie [g/l]'
        elif Type == 'Sediment':
            baseName = 'Sediment'
            baseTitle = 'Sediment aanvoer in '
            ylable = 'Sedimentvracht [kg]'
        else:
            msg = 'No valable type defined for plotting timeseries'
            self.logger.warning(msg)
            return

        if self.scenario.ModelOptions['ConvertOutput'] == 1:
            indexcol = 'Time (min)'
        else:
            indexcol = 'Time (sec)'

        outlfile = os.path.join(self.scenario.outfolder, '%s.txt' % baseName)
        if os.path.isfile(outlfile):
            outl_df = pd.read_csv(outlfile, sep='\t', header=1)
            outl_df.index = outl_df[indexcol]
            outl_df = outl_df.drop(indexcol, 1)
        else:
            msg = '%s.txt not found in modelresults!' % baseName
            raise CNWSException(msg)

        if self.scenario.Output['OutputPerSegment'] == 1:
            segm_file = os.path.join(self.scenario.outfolder, '%s_VHA.txt') % baseName
            if os.path.isfile(segm_file):
                segm_df = pd.read_csv(segm_file, sep='\t', header=1)
                segm_df.index = segm_df[indexcol]
                segm_df = segm_df.drop(indexcol, 1)
            else:
                msg = '%s_VHA.txt not found in modelresults!' % baseName
                raise CNWSException(msg)
            df = pd.concat([outl_df, segm_df], axis=1, join='inner')
        else:
            df = outl_df.copy()

        for col in df.columns:
            if col.startswith('Unnamed'):
                df = df.drop(col, 1)

        os.chdir(self.folder)
        for i, col in enumerate(df.columns):
            plt.figure(i)
            plt.ylabel(ylable)
            if col.startswith('VHA '):
                Title = baseTitle + 'segment %s' % col[12:]
            else:
                Title = baseTitle + 'outlet %s' % col[7:]
            plt.title(Title)
            df[col].plot()
            plt.savefig(Title + '.png')
            plt.close(i)
        return

    def addSediment2SubCatchments(self):
        """
        Adds the sediment input of every river segment to the corresponding subcatchment.
        For every subcatchment the attribute sedar is calculated. Sedar is calculated as sedimentinput/area subcatchment
        :return: nothing
        """
        self.logger.info('toevoegen resultaten aan subcatchments...')
        results = os.path.join(self.scenario.outfolder, 'Total sediment VHA.txt')

        try:
            df = pd.read_csv(results, sep='\t', skiprows=2, names=['NR', 'Sediment'])
        except Exception:
            msg = 'could not open %s ' % results
            self.logger.error(msg)
        else:
            try:
                shp_df = gpd.read_file(self.subcatchmShp)
            except Exception:
                msg = 'could not open %s' % self.subcatchmShp
                self.logger.error(msg)
            else:
                shp_df['NR'] = shp_df['VALUE']
                shp_df = shp_df.merge(df, on='NR', how='left')
                shp_df['area'] = shp_df.area
                shp_df['sedar'] = shp_df['Sediment']/shp_df['area']
                shp_df['sedar_ha'] = shp_df['sedar']*10000.0
                shp_df.to_file(self.subcatchmShp)
        return

    def addSegmentResults2Shp(self):
        """
        Adds the sedimentinput to every riversegment and calculates the sedlen)-argument.
        Sedlen is calculated as sedimentinput/length river segment.

        The resulting shapfile is stored in self.segmShp.

        :return: nothing
        """
        self.logger.info('toevoegen resultaten aan segmenten...')
        results = os.path.join(self.scenario.outfolder, 'Total sediment VHA.txt')
        if os.path.isfile(results) and self.scenario.catchm.waterLine is not None:
            # omzetten van tab-delimited txt naar csv for join met ogr
            df = pd.read_csv(results, sep='\t', skiprows=2, names=['NR', 'Sediment'])
            shp_df = gpd.read_file(self.scenario.catchm.waterLine)
            shp_df = shp_df.merge(df, on='NR', how='left')
            shp_df['len'] = shp_df.length
            shp_df['sedlen'] = shp_df['Sediment']/shp_df['len']

            self.segmShp = 'Sedimentexport2Segments_%s_scenario_%s.shp' % (self.scenario.catchm.naam, self.scenario.nr)
            self.segmShp = os.path.join(self.folder, self.segmShp)
            shp_df.to_file(self.segmShp)
        else:
            msg = 'Total sediment VHA.txt or shapefile waterlines does not exist!'
            self.logger.error(msg)
        return

    def DefineSubcatchments_saga(self, inRst, routingfile, fname=None):
        """

        :param inRst:
        :param routingfile:
        :param fname:
        :return: nothing
        """
        # (SG) adjusted code to work with generic points to route up subcatchments
        self.logger.info('Afbakenen deelbekkens...')
        # txt = os.path.join(self.scenario.outfolder, 'routing.txt')
        os.chdir(self.folder)
        if fname is None:
            subcatchmRST = 'subcatchments_%s_%s' % (self.scenario.catchm.naam, self.scenario.nr)
        else:
            subcatchmRST = '%s_%s_%s' % (fname, self.scenario.catchm.naam, self.scenario.nr)
        subcatchmRST = os.path.join(self.folder, subcatchmRST)
        subcatchmShp = subcatchmRST + '.shp'

        cmd_str = 'saga_cmd -f=s topology 3 -ROUTING="%s" ' % routingfile
        cmd_str += '-SEGMENTS="%s" ' % inRst
        cmd_str += '-CATCH="%s"' % (subcatchmRST + ".sgrd")
        msg = 'Failed to define subcatchments'
        execute_subprocess(cmd_str, msg)

        cmd_str = 'saga_cmd -f=s shapes_grid 6 -GRID="%s" ' % (subcatchmRST + ".sgrd")
        cmd_str += '-POLYGONS="%s" -CLASS_ALL=1 -SPLIT=0' % subcatchmShp
        msg = 'Failed to convert subcatchments to shapefile'
        execute_subprocess(cmd_str, msg)
        return subcatchmRST + ".sdat", subcatchmShp

    def MakeRoutingshps_saga(self,extent=None,tile_number=None):
        #(SG) add option to only analyze part of spatial results (in cases domain is too large, or resolution to fine)
        self.logger.info('Aanmaken shapefile met routingvectoren')
        outshp = 'routing_%s_scenario_%s.shp' % (self.scenario.catchm.naam, self.scenario.nr)
        txt = os.path.join(self.scenario.outfolder, 'routing.txt')
        if os.path.isfile(txt):
            with open(txt) as f:
                first_line = f.readline()
                # check if file is tab seperated
            seperator = ';' if '\t' not in first_line else "\t"
            #(SG) if only a part is desidered (for memory reasons), clip routing  file to seleted extent
            if extent != None:
                df = pd.read_csv(txt,sep=seperator)
                #(SG) get xmin and ymin from rasterproperties
                xmin,ymin = self.scenario.catchm.RasterProp["minmax"][0],self.scenario.catchm.RasterProp["minmax"][1]
                #(SG) compile x and y's of source points in df
                df["x"] =  xmin+df["col"]*self.scenario.catchm.RasterProp["res"]
                df["y"] =  ymin+(self.scenario.catchm.RasterProp["nrows"]-df["row"])*self.scenario.catchm.RasterProp["res"]
                #(SG) condition
                cond  = (df["x"]>extent[0]) & (df["x"]<extent[2]) &  (df["y"]>extent[1]) &  (df["y"]<extent[3])
                df = df.loc[cond]
                #(SG) rename files (add tile number if desired)
                txt = txt[:txt.index(".")]+"_selected_%i.txt"%(0 if tile_number==None else tile_number)
                outshp_ = outshp[:outshp.index(".")]+"_selected_%i.shp"%(0 if tile_number==None else tile_number)
                df.to_csv(txt,sep='\t',index=False)
            else:
                outshp_ =outshp
                if '\t' not in first_line:
                    df = pd.read_csv(txt, sep=seperator)
                    df.to_csv(txt, sep='\t', index=False)
            if len(df)>0:
                cmd_str = 'saga_cmd -f=s topology 2 '
                cmd_str += '-ROUTING "%s" ' % txt
                cmd_str += '-LANDUSE "%s" ' % self.scenario.prckrt[self.scenario.jaren[0]]
                cmd_str += '-OUTPUTLINES "%s"' % os.path.join(self.folder, outshp_)
                msg = 'failed to convert routing txt to shapefile with saga'
                execute_subprocess(cmd_str, msg)
                self.routingshp = os.path.join(self.folder, outshp_)
                CreateSpatialIndex(self.routingshp)
            else:
                msg='ongeldige extent om routing te knippen meegegeven '
                self.logger.warning(msg)
        else:
            msg = 'routing.txt does not exist!'
            self.logger.error(msg)
            raise CNWSException(msg)
        return

    def IndentifySinksInRouting(self):
        self.logger.info('Zoeken naar sinks...')
        txt = os.path.join(self.scenario.outfolder, 'routing.txt')
        if os.path.isfile(txt):
            # check if file is tab seperated
            with open(txt) as f:
                first_line = f.readline()
            if '\t' not in first_line:
                df = pd.read_csv(txt, sep=';')  # old model runs used ; as seperator in routing file
            else:
                df = pd.read_csv(txt, sep='\t')

            Cnst = self.scenario.catchm.RasterProp
            df = df.loc[(df.target1row != -99) & (df.target2row != -99)].copy()  # punten -99 zijn buiten modeldomein
            df = df.loc[(df.part1 == 0) & (df.part2 == 0)].copy()
            if not df.empty:
                df['sourceX'] = (Cnst['minmax'][0] + (Cnst['res']/2)) + Cnst['res']*(df['col'] - 1)
                df['sourceY'] = (Cnst['minmax'][1] + (Cnst['res']/2)) + Cnst['res']*(Cnst['nrows'] - df['row'])
                df['geometry'] = df.apply(lambda x: shapely.geometry.Point(float(x.sourceX), float(x.sourceY)), axis=1)
                CRS = {'init': Cnst['epsg']}
                df = gpd.GeoDataFrame(df, geometry='geometry', crs=CRS)
                os.chdir(self.folder)
                outshp = 'sinks_%s_scen_%s.shp' % (self.scenario.catchm.naam, self.scenario.nr)
                df.to_file(outshp)
            else:
                self.logger.info('No Sinks in model area')
        else:
            msg = 'routing.txt does not exist!'
            self.logger.error(msg)
        return

    def DoPostprocessing(self):
        self.logger.info('Start Postprocessing...')
        for f in os.listdir(self.scenario.outfolder):
            if f.endswith('.rst'):
                infile = os.path.join(self.scenario.outfolder, f)
                outfile = os.path.join(self.scenario.ppfolder, f[:-4] + '_nodata.tif')

                if f == 'RUSLE.rst':
                    self.rusleRST = infile
                elif f == 'UPAREA.rst':
                    self.upareaRST = infile
                elif f == 'WATEREROS (kg per gridcel).rst':
                    self.waterosRST = infile
                elif f == 'TILEROS.rst':
                    self.tilerosRST = infile
                elif f == 'SediExport_kg.rst':
                    rstarr = readRstAsArr(infile)
                    riverarr = readRstAsArr(self.scenario.catchm.waterlopenkrt)
                    nodata = self.scenario.catchm.RasterProp['nodata']
                    rstarr = np.where(riverarr != nodata, rstarr, nodata)
                    write_arr_as_rst(rstarr, outfile, 'float32', self.scenario.catchm.rstparams)
                    self.sedExportRST = outfile
                elif f == 'cumulative.rst':
                    rstarr = readRstAsArr(infile)
                    riverarr = readRstAsArr(self.scenario.catchm.waterlopenkrt)
                    nodata = self.scenario.catchm.RasterProp['nodata']
                    rstarr = np.where(riverarr != nodata, rstarr, nodata)
                    write_arr_as_rst(rstarr, outfile, 'float32', self.scenario.catchm.rstparams)
                    self.cumulativeRST = outfile
                elif f == 'SediIn_kg.rst':
                    self.sedInRST = infile
                elif f == 'SediOut_kg.rst':
                    self.sedOutRST = infile
                elif f == 'SLOPE.rst':
                    self.slope = infile
                elif f == 'Remap.rst':
                    self.remap = infile
                elif f == 'Total runoff.rst':
                    self.totalrunoff = infile

        for jaar in self.scenario.jaren:
            self.perceelRST[jaar] = os.path.split(self.scenario.prckrt[jaar])[1]
            self.perceelRST[jaar] = self.perceelRST[jaar][:-4] + '_nodata.tif'
            self.perceelRST[jaar] = os.path.join(self.folder, self.perceelRST[jaar])
            self.SetNoData(self.scenario.prckrt[jaar], self.perceelRST[jaar])

        self.readSedimentTxt()
        if self.scenario.Output['OutputPerSegment'] == 1:
            self.addSegmentResults2Shp()

        if self.scenario.Output['WriteRouting'] == 1:
            self.MakeRoutingshps_saga()
            self.IndentifySinksInRouting()

            routingfile = os.path.join(self.scenario.outfolder, 'routing.txt')
            if self.scenario.Output['OutputPerSegment'] == 1:
                self.subcatchmRST, self.subcatchmShp = self.DefineSubcatchments_saga(self.scenario.riviersegmkrt, routingfile)
                self.addSediment2SubCatchments()
            else:
                self.subcatchmRST, self.subcatchmShp = self.DefineSubcatchments_saga(self.scenario.outletmap, routingfile)

            self.coupleSedoutRouting()
            self.SourceSinksAlgorithm(10)
            self.SourceSinksAlgorithm(20)
            self.SourceSinksAlgorithm(30)
            self.SourceSinksAlgorithm(40)
            self.SourceSinksAlgorithm(50)

        self.MakeFacts()

        if self.scenario.versie == 'CNWS':
            self.PlotTimeSeries('Q')
            self.PlotTimeSeries('Sedigram')
            self.PlotTimeSeries('Sediment')
        # self.CreateQGS(stylefolder)
        return

    def CalculateAreasPrcMap(self):
        """
        Calculates the areas and relative areas of all landuse classes in the parcelmap
        :return: filename with results
        """
        os.chdir(self.folder)
        for year in self.scenario.jaren:
            prc_arr = readRstAsArr(self.scenario.prckrt[year])

            res = self.scenario.catchm.RasterProp['res']
            prc_arr = np.where(prc_arr >= 1, 1, prc_arr)
            vals, counts = np.unique(prc_arr, return_counts=True)
            areas = counts * res * res
            mask = np.where(vals == 0, True, False)
            vals = np.ma.array(vals, mask=mask)
            areas = np.ma.array(areas, mask=mask)
            total_area = areas.sum()
            rel_areas = areas / total_area

            df = pd.DataFrame()
            df['lnduse_class'] = vals
            df['area'] = areas
            df['rel_area'] = rel_areas * 100
            f = 'opp_perceelskaart_%s_%s_%s.csv' %(self.scenario.catchm.naam, self.scenario.nr, year)
            df.to_csv(f, sep=';')
        return f

    def MakeFacts(self):
        os.chdir(self.folder)
        f = open('facts_scenario_%s_%s.csv' %(self.scenario.nr, self.scenario.catchm.naam), 'w')
        f.write(';%s\n' % self.scenario.catchm.naam)
        oppCatch = self.scenario.catchm.GetCatchmentArea()
        oppCatch_ha = oppCatch/10000.0
        f.write('Oppervlakte bekken;%s\n' % oppCatch_ha)
        for jaar in self.scenario.jaren:
            # facts percelen
            if self.scenario.percelenshp[jaar] is not None:
                parceldf = gpd.read_file(self.scenario.percelenshp[jaar])
                nParcels = parceldf.shape[0]
                f.write('Aantal landbouwpercelen %s;%s\n' % (jaar, nParcels))

                parceldf['opp'] = parceldf.area
                oppParcels = parceldf.opp.sum()
                oppParcels_ha = oppParcels/10000.0
                f.write('Oppervlakte landbouwpercelen %s;%s\n' % (jaar, oppParcels_ha))

                aandeel_landbouw = (oppParcels_ha/oppCatch_ha)*100
                f.write('relatieve opp landbouwpercelen %s;%s\n' % (jaar, aandeel_landbouw))

                if 'Lndgbrk' in parceldf.columns:
                    Fieldsdf = parceldf[parceldf.Lndgbrk == 0].copy()
                    nFields = Fieldsdf.shape[0]
                    f.write('Aantal akkers %s;%s\n' % (jaar, nFields))

                    oppFields = Fieldsdf.opp.sum()
                    oppFields_ha = oppFields/10000.0
                    f.write('Oppervlakte akkers %s;%s\n' % (jaar, oppFields_ha))

                    aandeel_akker = (oppFields_ha/oppCatch_ha)*100
                    f.write('relatieve opp akkers (tov totale opp catch) %s;%s\n' % (jaar, aandeel_akker))

                nNtKerend = parceldf[parceldf.ntkerend == 1].shape[0]
                f.write('Aantal nt-kerend bewerkte percelen %s;%s\n' % (jaar, nNtKerend))
            # facts grasstroken
            if self.scenario.grasshp[jaar] is not None:
                grasdf = gpd.read_file(self.scenario.grasshp[jaar])
                nGras = grasdf.shape[0]
                f.write('Aantal grasstroken %s;%s\n' % (jaar, nGras))
#                if 'breedte' in grasdf.columns:
#                    meanWidth = grasdf.breedte.mean()
#                elif 'BREEDTE' in grasdf.columns:
#                    meanWidth = grasdf.BREEDTE.mean()
#                elif 'Breedte' in grasdf.columns:
#                    meanWidth = grasdf.BREEDTE.mean()
#                f.write('Gemiddelde breedte grasstrook %s;%s\n' % (jaar, meanWidth))
        # calculation of mean slope
        with rasterio.open(self.slope) as src:
            arr = src.read(1)
            arr_ma = np.ma.masked_where(arr == src.nodata, arr)
        meanSlope = arr_ma.mean()
        meanSlope_deg = (180/3.14)*meanSlope
        f.write('Gemiddelde hellingsgraad;%s\n' % meanSlope_deg)
        f.close()
        return

    def MakeMap(self, stylefolder, jaar):
        try:
            from CNWS_maps import QProject as Maps
        except ImportError as e:
            raise CNWSException(e)

        qgsfile = '%s_scenario_%s.qgs' % (self.scenario.catchm.naam, self.scenario.nr)
        QP = Maps.QProject(self.folder, qgsfile)
        QP.createProject(self.scenario.catchm.RasterProp['epsg'])
        QP.AddRst(self.rusleRST, 'RUSLE', os.path.join(stylefolder, 'RUSLE.qml'))
        QP.AddRst(self.upareaRST, 'UPAREA', os.path.join(stylefolder, 'UPAREA.qml'))
        QP.AddRst(self.waterosRST, 'WATEROS', os.path.join(stylefolder, 'WATEREROS (kg per gridcel).qml'))
        QP.AddRst(self.sedOutRST, 'SediOut', os.path.join(stylefolder, 'SediOut_kg.qml'))
        QP.AddRst(self.perceelRST, 'Perceelskaart', os.path.join(stylefolder, 'perceelskaart.qml'))
        QP.AddShp(self.scenario.percelenshp[jaar], 'Perceelsgrenzen', 'Perceelsgrenzen', os.path.join(stylefolder, 'Percelen.qml'))
        QP.AddShp(self.scenario.catchm.infrLine, 'Infrastructuur', 'Infrastructuur_line', os.path.join(stylefolder, 'InfraLine.qml'))
        QP.AddShp(self.scenario.catchm.infrPoly, 'Infrastructuur', 'Infrastructuur_poly', os.path.join(stylefolder, 'InfraPoly.qml'))
        QP.AddShp(self.scenario.catchm.waterLine, 'Waterlopen', 'Waterlopen', os.path.join(stylefolder, 'Waterlopen.qml'))
        QP.AddShp(self.routingshp, 'Routing (springen)', 'Routing_jump', os.path.join(stylefolder, 'Routering.qml'))
        QP.AddShp(self.routingshp, 'Routing (parts)', 'Routing_parts', os.path.join(stylefolder, 'Routering_parts.qml'))
        QP.AddShp(self.subcatchmShp, 'Deelbekkens', 'Deelbekkens', os.path.join(stylefolder, 'Subcatchments.qml'))
        QP.AddShp(self.subcatchmShp, 'Specifieke sediment productie (ton/ha)', 'SedAr', os.path.join(stylefolder, 'klassen_tonperha.qml'))
        QP.AddShp(self.segmShp, 'Sedimentinput per riviersegment (ton/m)', 'SedLen', os.path.join(stylefolder, 'SedLen.qml'))

        extent = QP.project.mapLayersByName('Deelbekkens')[0].extent()

        # layout1 - uparea
        layers = []
        layers.append(QP.project.mapLayersByName('Infrastructuur')[0])
        layers.append(QP.project.mapLayersByName('Infrastructuur')[1])
        layers.append(QP.project.mapLayersByName('Waterlopen')[0])
        layers.append(QP.project.mapLayersByName('UPAREA')[0])
        title = 'Afstromingspatroon'
        QP.CreateLayout('Uparea', layers, extent, title)
        QP.WriteLayoutToPDF('Uparea', 'uparea.pdf')

        # layout2 - SediOut
        layers = []
        layers.append(QP.project.mapLayersByName('Infrastructuur')[0])
        layers.append(QP.project.mapLayersByName('Infrastructuur')[1])
        layers.append(QP.project.mapLayersByName('Waterlopen')[0])
        layers.append(QP.project.mapLayersByName('SediOut_kg')[0])
        title = 'Sedimentdoorvoer (kg/jaar)'
        QP.CreateLayout('SediOut', layers, extent, title)
        QP.WriteLayoutToPDF('SediOut', 'sediout.pdf')

        # layout3 - WaterEros
        layers = []

        # layout4 - SubCatchments
        layers = []

        QP.saveProject()
        return


class DeltaScenario:
    def __init__(self, pp1, pp2, folder):
        """
        Constructs new DeltaScenario object.
        :param s1: postprocessing object scenario 1
        :param s2: postprocessing object scenario 2
        :param folder: path to folder were results need to be stored
        """
        self.logger = logging.getLogger(__name__)
        if isinstance(pp1, PostProcessScenario):
            self.pp1 = pp1
        else:
            msg = ''
            raise CNWSException(msg)

        if isinstance(pp2, PostProcessScenario):
            self.pp2 = pp2
        else:
            msg = ''
            raise CNWSException(msg)

        self.folder = folder
        self.SediInDiff = None
        self.SediOutDiff = None
        self.SedExportDiff = None
        self.WaterErosDiff = None
        self.TilErosDiff = None
        self.totalresults = None

    def CheckScenarios(self):
        """
        Checks if both scenarios are made with the same spatial characteristics
        :return: Bool
        """
        spatprop1 = self.pp1.scenario.catchm.RasterProp
        spatprop2 = self.pp2.scenario.catchm.RasterProp
        if spatprop1 == spatprop2:
            return True
        else:
            return False

    def DiffSedResults(self):
        """
        creates pandas dataframe with a column for both scenarios and a column difference
        the rows represent the different values of total sediment.txt
        :return: nothing
        """
        d1 = self.pp1.totalresults
        d2 = self.pp2.totalresults

        if d1 is not None or d2 is not None:
            df = pd.concat([d1, d2], axis=1)
            df['diff'] = df[self.pp1.scenario.nr] - df[self.pp2.scenario.nr]
            self.totalresults = df
        else:
            msg = 'total sediment.txt of one scenario is not yet read!'
            raise CNWSException(msg)
        return

    def DiffGrid(self, gridtype):
        """
        calculates the difference of two input grids. The difference grid is stored as a saga-grid
        in self.folder
        :param gridtype: str, can be 'SediOut', 'SediIn', 'SedExport', 'WaterEros'
        :return: nothing
        """
        if gridtype == 'SediOut':
            inRst1 = self.pp1.sedOutRST
            inRst2 = self.pp2.sedOutRST
            outRst = os.path.join(self.folder, 'SediOut_difference.sgrd')
            self.SediOutDiff = outRst
        elif gridtype == 'SediIn':
            inRst1 = self.pp1.sedInRST
            inRst2 = self.pp2.sedOutRST
            outRst = os.path.join(self.folder, 'SediIn_difference.sgrd')
            self.SediInDiff = outRst
        elif gridtype == 'SedExport':
            inRst1 = self.pp1.sedExportRST
            inRst2 = self.pp2.sedExportRST
            outRst = os.path.join(self.folder, 'SedExport_difference.sgrd')
            self.SedExportDiff = outRst
        elif gridtype == 'WaterEros':
            inRst1 = self.pp1.waterosRST
            inRst2 = self.pp2.waterosRST
            outRst = os.path.join(self.folder, 'WaterEros_difference.sgrd')
            self.WaterErosDiff = outRst
        else:
            msg = 'no valid gridtype chosen!'
            raise CNWSException(msg)

        if inRst1 is None:
            msg = '%s scenario %s not (yet) created!' % (gridtype, self.pp1.scenario.nr)
            raise CNWSException(msg)
        elif inRst2 is None:
            msg = '%s scenario %s not (yet) created!' % (gridtype, self.pp2.scenario.nr)
            raise CNWSException(msg)
        else:
            GridDifference(inRst1, inRst2, outRst)
        return

    def MakeMap(self, stylefolder):
        try:
            from CNWS_maps import QProject as Maps
        except ImportError as e:
            raise CNWSException(e)

        qgsfile = '.qgs'
        QP = Maps.QProject(self.folder, qgsfile)
        QP.createProject(self.pp1.catchm.RasterProp['epsg'])

        #TO DO

        QP.saveProject()
        return



class IniToScenarioInstance():
    """"(SG) function to read inifile and generate an instance of scenario and bekken
    Workflow: first read inifile, create scenario and bekken, and fill
    """
    def __init__(self,ini,catchmshape,catchmname,sim_num):
        self.logger = logging.getLogger(__name__)
        self.catchmshape = catchmshape
        self.catchmname = catchmname
        self.sim_num = sim_num
        self.ini = ini
        self.Cfg = None
        self.workingdir = None

    def ReadAndFill(self):

        #(SG) Read ini file
        self.ReadIni()

        #(SG) Initiate catchm and scenario
        self.InitiateScenarioBekken()

        #(SG) Fill scenario with Cfg files and options
        self.FillScenario()

        return self.scenario

    def ReadIni(self):
        """
        Reads ini-file and, if the file exists returns configparser instance.
        If the file does not exist, return None
        :param ini: path to ini-file
        :return: configparser instance or None
        """
        if not os.path.isfile(self.ini):
            msg = 'ini-file "%s" does not exist in "%s"'%(self.ini,os.getcwd())
            self.logger.warning(msg)
            raise CNWSException(msg)
        else:
            msg = 'reading ini-file'
            self.logger.info(msg)
            self.Cfg = configparser.ConfigParser()
            self.Cfg.read(self.ini)
            self.workingdir = dir_path = os.path.dirname(os.path.realpath(self.ini))

    def InitiateScenarioBekken(self):
        # (SG) Start and instance of catchm, and scenario
        catchm = Catchment(self.catchmname, self.catchmshape)
        # (SG) Read extent of simulations
        workingdir = self.workingdir[:self.workingdir.index("scenario")]
        catchm.ReadExtentCatchm(fmap=os.path.join(workingdir, "Data_Bekken"))
        # (SG) Create Scenario Object
        self.scenario = Scenario(catchm, self.sim_num)

    def FillScenario(self):
        self.ReadWorkingDirectories()
        self.ReadInputFiles()
        self.ReadVariables()
        self.ReadUserChoices()
        self.ReadBufferData()
        self.ReadForceRoutingData()

    def CheckSection(self,sectionname):
        """
        checks if section is present in configparser-instance
        :param sectionname: str, name of the section
        :return: if section in Cfg, True, else None
        """
        if sectionname not in self.Cfg.sections():
            self.logger.warning('%s not in config-file' %sectionname)
            return False
        else:
            return True

    def ReadInputFiles(self):
        """
        Reads all input files present in ini-file (section Files)
        :param Cfg: configparser-object
        :param scenario: scenario-object
        :return: nothing, variables of scenario-object are set.
        """

        # utility funcitons within this function

        def checkpath(path, indir, key):
            # 1. Check if path is a valid file
            if os.path.isfile(path):
                path = os.path.abspath(path)
                return path
            else:
                if path == '':
                    msg = 'no path for %s given!' % key
                    self.logger.warning(msg)
                    return None
                else:
                    testpath = os.path.join(indir, os.path.split(path)[1])
                    if os.path.isfile(testpath):
                        return os.path.abspath(testpath)
                    else:
                        msg = 'no valid path for %s given!' % key
                        self.logger.warning(msg)
                        return None

        def checktime(year, season):
            if year not in self.scenario.jaren:
                self.scenario.jaren.append(year)
                self.scenario.seizoenen.append([])
                nr = len(scenario.jaren)
            else:
                for a, jaar in enumerate(self.scenario.jaren):
                    if jaar == year:
                        nr = a
                        break
            if season not in self.scenario.seizoenen[nr]:
                self.scenario.seizoenen[nr].append(season)
            return

        try:
            minmax = self.scenario.catchm.RasterProp['minmax']
            ncols = self.scenario.catchm.RasterProp['ncols']
            nrows = self.scenario.catchm.RasterProp['nrows']

        except KeyError:
            msg = 'spatial properties of catchment are not defined!'
            self.logger.error(msg)
            raise CNWSException(msg)

        section = 'Files'
        if self.CheckSection(section):
            for key in self.Cfg[section]:
                i = self.Cfg[section][key]
                key = key.lower()
                if i == '':
                    msg = '%s in %s in ini-file is not given' % (key, section)
                    self.logger.warning(msg)
                elif key == 'dtm filename':
                    self.scenario.DTMkaart = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.DTMkaart, minmax, ncols, nrows):
                        self.scenario.DTMkaart = None
                elif key == 'k factor filename':
                    self.scenario.Kkaart = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.Kkaart, minmax, ncols, nrows):
                        self.scenario.Kkaart = None
                elif key == 'p factor map filename':
                    self.scenario.Pkaart = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.Pkaart, minmax, ncols, nrows):
                        self.scenario.Pkaart = None
                    else:
                        self.scenario.catchm.binarr = readRstAsArr(self.scenario.Pkaart)
                elif key == 'buffer map filename':
                    self.scenario.bufferkaart = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.bufferkaart, minmax, ncols, nrows):
                        self.scenario.bufferkaart = None
                elif key == 'ditch map filename':
                    self.scenario.grachtenkaart = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.grachtenkaart, minmax, ncols, nrows):
                        self.scenario.grachtenkaart = None
                elif key == 'dam map filename':
                    self.scenario.gdammenkaart = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.gdammenkaart, minmax, ncols, nrows):
                        self.scenario.gdammenkaart = None
                elif key == 'outlet map filename':
                    self.scenario.outletmap = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.outletmap, minmax, ncols, nrows):
                        self.scenario.outletmap = None
                elif key == 'river segment filename':
                    self.scenario.riviersegmkrt = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.riviersegmkrt, minmax, ncols, nrows):
                        self.scenario.riviersegmkrt = None
                elif key == 'rainfall filename':
                    self.scenario.Variables['Rainfall_file'] = checkpath(i, self.scenario.infolder, key)
                elif key == 'parcel filename' or key == 'parcel filename 1':
                    if 1 not in self.scenario.jaren:
                        self.scenario.jaren.append(1)
                        self.scenario.prckrt[1] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.prckrt[1], minmax, ncols, nrows):
                        self.scenario.prckrt[1] = None
                elif key == 'parcel filename 2':
                    if 2 not in self.scenario.jaren:
                        self.scenario.jaren.append(2)
                        self.scenario.prckrt[2] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(scenario.prckrt[2], minmax, ncols, nrows):
                        self.scenario.prckrt[2] = None
                elif key == 'cn map filename' or key == 'cn map spring 1':
                    checktime(1, 'spring')
                    if 1 not in self.scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[1] = {}
                        self.scenario.CNkaarten[1]['spring'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.CNkaarten[1]['spring'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[1]['spring'] = None
                elif key == 'cn map summer 1':
                    checktime(1, 'summer')
                    if 1 not in self.scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[1] = {}
                        self.scenario.CNkaarten[1]['summer'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.CNkaarten[1]['summer'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[1]['summer'] = None
                elif key == 'cn map summer 2':
                    checktime(2, 'summer')
                    if 2 not in self.scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[2] = {}
                        self.scenario.CNkaarten[2]['summer'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(scenario.CNkaarten[2]['summer'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[2]['summer'] = None
                elif key == 'cn map fall 1':
                    checktime(1, 'fall')
                    if 1 not in self.scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[1] = {}
                        self.scenario.CNkaarten[1]['fall'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.CNkaarten[1]['fall'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[1]['fall'] = None
                elif key == 'cn map fall 2':
                    checktime(2, 'fall')
                    if 2 not in self.scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[2] = {}
                        self.scenario.CNkaarten[2]['fall'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.CNkaarten[2]['fall'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[2]['fall'] = None
                elif key == 'cn map winter 1':
                    checktime(1, 'winter')
                    if 1 not in scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[1] = {}
                        self.scenario.CNkaarten[1]['winter'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.CNkaarten[1]['winter'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[1]['winter'] = None
                elif key == 'cn map winter 2':
                    checktime(2, 'winter')
                    if 2 not in self.scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[2] = {}
                        self.scenario.CNkaarten[2]['winter'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.CNkaarten[2]['winter'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[2]['winter'] = None
                elif key == 'cn map spring 2':
                    checktime(2, 'spring')
                    if 2 not in scenario.CNkaarten.keys():
                        self.scenario.CNkaarten[2] = {}
                        self.scenario.CNkaarten[2]['spring'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.CNkaarten[2]['spring'], minmax, ncols, nrows):
                        self.scenario.CNkaarten[2]['spring'] = None
                elif key == 'c factor map filename' or key == 'c factor map spring 1':
                    checktime(1, 'spring')
                    if 1 not in self.scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[1] = {}
                        self.scenario.Ckaarten[1]['spring'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.Ckaarten[1]['spring'], minmax, ncols, nrows):
                        self.scenario.Ckaarten[1]['spring'] = None
                elif key == 'c factor map spring 2':
                    checktime(2, 'spring')
                    if 2 not in self.scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[2] = {}
                        self.scenario.Ckaarten[2]['spring'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.Ckaarten[2]['spring'], minmax, ncols, nrows):
                        self.scenario.Ckaarten[2]['spring'] = None
                elif key == 'c factor map summer 1':
                    checktime(1, 'summer')
                    if 1 not in self.scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[1] = {}
                        self.scenario.Ckaarten[1]['summer'] = checkpath(i, scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.Ckaarten[2]['spring'], minmax, ncols, nrows):
                        self.scenario.Ckaarten[1]['summer'] = None
                elif key == 'c factor map summer 2':
                    checktime(2, 'summer')
                    if 2 not in scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[2] = {}
                        self.scenario.Ckaarten[2]['summer'] = checkpath(i, self.scenario.infolder, key)
                        if not CheckRstDimensions(self.scenario.Ckaarten[2]['summer'], minmax, ncols, nrows):
                            self.scenario.Ckaarten[2]['summer'] = None
                elif key == 'c factor map fall 1':
                    checktime(1, 'fall')
                    if 1 not in self.scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[1] = {}
                        self.scenario.Ckaarten[1]['fall'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(scenario.Ckaarten[1]['fall'], minmax, ncols, nrows):
                        self.scenario.Ckaarten[1]['fall'] = None
                elif key == 'c factor map fall 2':
                    checktime(2, 'fall')
                    if 2 not in self.scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[2] = {}
                        self.scenario.Ckaarten[2]['fall'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(scenario.Ckaarten[2]['fall'], minmax, ncols, nrows):
                        self.scenario.Ckaarten[2]['fall'] = None
                elif key == 'c factor map winter 1':
                    checktime(1, 'winter')
                    if 1 not in self.scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[1] = {}
                        self.scenario.Ckaarten[1]['winter'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.Ckaarten[1]['winter'], minmax, ncols, nrows):
                        self.scenario.Ckaarten[1]['winter'] = None
                elif key == 'c factor map winter 2':
                    checktime(2, 'winter')
                    if 2 not in self.scenario.Ckaarten.keys():
                        self.scenario.Ckaarten[2] = {}
                        self.scenario.Ckaarten[2]['winter'] = checkpath(i, self.scenario.infolder, key)
                    if not CheckRstDimensions(self.scenario.Ckaarten[2]['winter'], minmax, ncols, nrows):
                        self.scenario.Ckaarten[2]['winter'] = None


    def ReadWorkingDirectories(self):
        section = 'Working directories'
        if self.CheckSection(section):
            for key in self.Cfg[section]:
                i = self.Cfg[section][key]
                key = key.lower()
                if key == 'input directory':
                    if os.path.exists(i):
                        self.scenario.infolder = i
                        #(SG) add ppfolder for postprocessing
                        self.scenario.ppfolder = i[:i.index("modelinput")]+"postprocessing"
                    else:
                        msg = 'input directory does not exist!'
                        self.logger.warning(msg)
                elif key == 'output directory':
                    if os.path.exists(i):
                        self.scenario.outfolder = i
                    else:
                        msg = 'output directory does not exist!'
                        self.logger.warning(msg)


    def ReadVariables(self):
        section = 'Variables'
        if self.CheckSection(section):
            for key in self.Cfg[section]:
                i = self.Cfg[section][key]
                key = key.lower()
                if key == '5-day antecedent rainfall':
                    self.scenario.Variables['5dayRainfall'] = self.Cfg.getfloat(section, key)
                elif key == 'r factor':
                    self.scenario.Variables['R'] = self.Cfg.getint(section, key)
                elif key == 'bulk density':
                    self.scenario.Variables['Bulkdensity'] = self.Cfg.getint(section, key)
                elif key == 'stream velocity':
                    self.scenario.Variables['velocity'] = self.Cfg.getfloat(section, key)
                elif key == 'alpha':
                    self.scenario.Variables['Alpha'] = self.Cfg.getfloat(section, key)
                elif key == 'beta':
                    self.scenario.Variables['Beta'] = self.Cfg.getfloat(section, key)
                elif key == 'ktc low':
                    self.scenario.Variables['ktc_low'] = self.Cfg.getint(section, key)
                elif key == 'ktc high':
                    self.scenario.Variables['ktc_high'] = self.Cfg.getint(section, key)
                elif key == 'ktc limit':
                    self.scenario.Variables['ktc_limit'] = self.Cfg.getfloat(section, key)
                elif key == 'ktil default':
                    self.scenario.Variables['ktil_default'] = self.Cfg.getint(section, key)
                elif key == 'ktil threshold':
                    self.scenario.Variables['ktil_threshold'] = self.Cfg.getfloat(section, key)
                elif key == 'clay content parent material':
                    self.scenario.Variables['ClayContent'] = self.Cfg.getfloat(section, key)
                elif key == 'parcel connectivity cropland':
                    self.scenario.Variables['Parcel connectivity cropland'] = self.Cfg.getfloat(section, key)
                elif key == 'parcel conncetivity forest':
                    self.scenario.Variables['Parcel connectivity forest'] = self.Cfg.getfloat(section, key)
                elif key == 'desired timestep for model':
                    self.scenario.Variables['Timestep'] = self.Cfg.getint(section, key)
                elif key == 'endtime model':
                    self.scenario.Variables['Endtime'] = self.Cfg.getfloat(section, key)
                elif key == 'final timestep output':
                    self.scenario.Variables['Timestep_output'] = self.Cfg.getint(section, key)
                elif key == 'parcel trapping efficiency cropland':
                    self.scenario.Variables['Parcel trapping efficiency cropland'] = self.Cfg.getfloat(section, key)
                elif key == 'parcel trapping efficiency forest':
                    self.scenario.Variables['Parcel trapping efficiency forest'] = self.Cfg.getfloat(section, key)
                elif key == 'parcel trapping efficiency pasture':
                    self.scenario.Variables['Parcel trapping efficiency pasture'] = self.Cfg.getfloat(section, key)
                elif key == 'ktchigh_lower':
                    self.scenario.Variables['KTcHigh_lower'] = self.Cfg.getint(section, key)
                elif key == 'ktchigh_upper':
                    self.scenario.Variables['KTcHigh_upper'] = self.Cfg.getint(section, key)
                elif key == 'ktclow_lower':
                    self.scenario.Variables['KTcLow_lower'] = self.Cfg.getint(section, key)
                elif key == 'ktclow_upper':
                    self.scenario.Variables['KTcLow_upper'] = self.Cfg.getint(section, key)
                elif key == 'steps':
                    self.scenario.Variables['steps'] = self.Cfg.getint(section, key)
        return

    def ReadUserChoices(self):
        """
        Reads all values of all keys in the section 'User Choices' of an ini-file of the CNWS-model
        :param self:
        :param scenario:
        :return:
        """
        section = 'User Choices'
        if self.CheckSection(section):
            for key in self.Cfg[section]:
                key = key.lower()
                if key == 'simplified model version':
                    if self.Cfg.getint(section, key) == 1:
                        self.scenario.versie = 'WS'
                    elif self.Cfg.getint(section, key) == 0:
                        self.scenario.versie = 'CNWS'
                elif key == 'use r factor':
                    self.scenario.ModelOptions['UseR'] = self.Cfg.getint(section, key)
                elif key == 'calibrate':
                    self.scenario.ModelOptions['Calibrate'] = self.Cfg.getint(section, key)
                elif key == 'create ktil map':
                    self.scenario.ModelOptions['CreateKTIL'] = self.Cfg.getint(section, key)
                elif key == 'estimate clay content':
                    self.scenario.ModelOptions['EstimClay'] = self.Cfg.getint(section, key)
                elif key == 'include buffers':
                    self.scenario.EBMOptions['UseBuffers'] = self.Cfg.getint(section, key)
                elif key == 'include ditches':
                    self.scenario.EBMOptions['UseGrachten'] = self.Cfg.getint(section, key)
                elif key == 'include dams':
                    self.scenario.EBMOptions['UseGeleidendeDammen'] = self.Cfg.getint(section, key)
                elif key == 'manual outlet selection':
                    self.scenario.ModelOptions['ManualOutlet'] = self.Cfg.getint(section, key)
                elif key == 'convert output':
                    self.scenario.ModelOptions['ConvertOutput'] = self.Cfg.getint(section, key)
                elif key == 'output per vha river segment':
                    self.scenario.Output['OutputPerSegment'] = self.Cfg.getint(section, key)
                elif key == 'write aspect':
                    self.scenario.Output['WriteAspect'] = self.Cfg.getint(section, key)
                elif key == 'write ls factor':
                    self.scenario.Output['WriteLs'] = self.Cfg.getint(section, key)
                elif key == 'write rainfall excess':
                    self.scenario.Output['WriteRainExcess'] = self.Cfg.getint(section, key)
                elif key == 'write rusle':
                    self.scenario.Output['WriteRUSLE'] = self.Cfg.getint(section, key)
                elif key == 'write sediment export':
                    self.scenario.Output['WriteSedExport'] = self.Cfg.getint(section, key)
                elif key == 'write slope':
                    self.scenario.Output['WriteSlope'] = self.Cfg.getint(section, key)
                elif key == 'write tillage erosion':
                    self.scenario.Output['WriteTillageErosion'] = self.Cfg.getint(section, key)
                elif key == 'write upstream area':
                    self.scenario.Output['WriteUpstreamArea'] = self.Cfg.getint(section, key)
                elif key == 'write water erosion':
                    self.scenario.Output['WriteWaterErosion'] = self.Cfg.getint(section, key)
                elif key == 'l model':
                    self.scenario.ModelOptions['L model'] = self.Cfg.get(section, key)
                elif key == 's model':
                    self.scenario.ModelOptions['S model'] = self.Cfg.get(section, key)
                elif key == 'force routing':
                    self.scenario.ModelOptions['Force Routing'] = self.Cfg.get(section, key)
                elif key == 'river routing':
                    self.scenario.ModelOptions['River Routing'] = self.Cfg.get(section, key)
                elif key == 'adjusted slope':
                    self.scenario.ModelOptions['Adjusted Slope'] = self.Cfg.getint(section, key)
                elif key == 'buffer reduce area':
                    self.scenario.ModelOptions['Buffer reduce Area'] = self.Cfg.getint(section, key)
        return

    def ReadBufferData(self):
        # 1. Check if Buffers are included in model-inifile
        section = 'User Choices'
        self.scenario.EBMOptions['UseBuffers'] = 0
        if self.CheckSection(section):
            for key in self.Cfg[section]:
                key = key.lower()
                if key == 'include buffers':
                    self.scenario.EBMOptions['UseBuffers'] = self.Cfg.getint(section, key)

        # 2. Check the number of buffers
        nBuffers = 0
        if self.scenario.EBMOptions['UseBuffers'] == 1:
            section = 'Variables'
            if self.CheckSection(section):
                for key in self.Cfg[section]:
                    key = key.lower()
                    if key == 'number of buffers':
                        nBuffers = self.Cfg.getint(section, key)

        # 3. Read for each buffer all data
        if nBuffers != 0:
            self.scenario.bufferData = []
            for buffer in range(nBuffers):
                bufferlist = []
                buffersection = 'Buffer %s' % (buffer + 1)
                if section in self.Cfg.sections():
                    bufferlist.append(self.Cfg.getint(buffersection, 'extension id'))
                    bufferlist.append(self.Cfg.getfloat(buffersection, 'volume'))
                    bufferlist.append(self.Cfg.getfloat(buffersection, 'height dam'))
                    bufferlist.append(self.Cfg.getfloat(buffersection, 'height opening'))
                    bufferlist.append(self.Cfg.getfloat(buffersection, 'opening area'))
                    bufferlist.append(self.Cfg.getfloat(buffersection, 'discharge coefficient'))
                    bufferlist.append(self.Cfg.getfloat(buffersection, 'width dam'))
                    bufferlist.append(self.Cfg.getfloat(buffersection, 'trapping efficiency'))
                    bufferlist.append(buffer + 1)
                else:
                    msg = '%s not available in ini-file' % section
                    self.logger.warning(msg)
                    self.scenario.bufferData.append(bufferlist)
        return

    def ReadForceRoutingData(self):
        # 1. Check if force routing is included in model-inifile
        section = 'User Choices'
        self.scenario.ModelOptions['Force Routing'] = 0
        if self.CheckSection(section):
            for key in self.Cfg[section]:
                key = key.lower()
                if key == 'force routing':
                    self.scenario.ModelOptions['Force Routing'] = self.Cfg.getint(section, key)

        # 2. Check the number of force routing paths
        nForcedRouting = 0
        if self.scenario.ModelOptions['Force Routing'] == 1:
            section = 'Variables'
            if self.CheckSection(section):
                for key in self.Cfg[section]:
                    key = key.lower()
                    if key == 'number of forced routing':
                        nForcedRouting = self.Cfg.getint(section, key)

        # 3. Read data for each force routing path
        if nForcedRouting != 0:
            d = []
            for ForcedRouting in range(1, nForcedRouting + 1):
                ForcedRoutingsection = 'Forced Routing %s' % ForcedRouting
                if ForcedRoutingsection in self.Cfg.sections():
                    tempdict = {'NR':ForcedRouting}
                    for key in self.Cfg[ForcedRoutingsection].keys():
                        key = key.lower()
                        if key == 'from col':
                            tempdict['fromcol'] = self.Cfg.getint(ForcedRoutingsection, 'from col')
                        if key == 'from row':
                            tempdict['fromrow'] = self.Cfg.getint(ForcedRoutingsection, 'from row')
                        if key == 'target col':
                            tempdict['tocol'] = self.Cfg.getint(ForcedRoutingsection, 'target col')
                        if key == 'target row':
                            tempdict['torow'] = self.Cfg.getint(ForcedRoutingsection, 'target row')
                    d.append(tempdict)
            self.scenario.forcedroutingdata = pd.DataFrame(d, columns=['fromcol', 'fromrow', 'tocol', 'torow', 'NR'])
        return