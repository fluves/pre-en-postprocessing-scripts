<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyAlgorithm="0" minScale="1e+8" simplifyDrawingTol="1" simplifyDrawingHints="1" readOnly="0" version="3.2.0-Bonn" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" simplifyMaxScale="1" simplifyLocal="1" maxScale="0">
  <renderer-v2 forceraster="0" type="graduatedSymbol" enableorderby="0" symbollevels="0" attr="cum_perc" graduatedMethod="GraduatedColor">
    <ranges>
      <range symbol="0" lower="0.000000000000000" render="true" upper="10.000000000000000" label=" 0 - 10 "/>
      <range symbol="1" lower="10.000000000000000" render="true" upper="20.000000000000000" label=" 10 - 20 "/>
      <range symbol="2" lower="20.000000000000000" render="true" upper="30.000000000000000" label=" 20 - 30 "/>
      <range symbol="3" lower="30.000000000000000" render="false" upper="40.000000000000000" label=" 30 - 40 "/>
      <range symbol="4" lower="40.000000000000000" render="false" upper="50.000000000000000" label=" 40 - 50 "/>
      <range symbol="5" lower="50.000000000000000" render="false" upper="60.000000000000000" label=" 50 - 60 "/>
      <range symbol="6" lower="60.000000000000000" render="false" upper="70.000000000000000" label=" 60 - 70 "/>
      <range symbol="7" lower="70.000000000000000" render="false" upper="80.000000000000000" label=" 70 - 80 "/>
      <range symbol="8" lower="80.000000000000000" render="false" upper="90.000000000000000" label=" 80 - 90 "/>
      <range symbol="9" lower="90.000000000000000" render="false" upper="100.000000000000000" label=" 90 - 100 "/>
    </ranges>
    <symbols>
      <symbol alpha="1" name="0" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="1" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="2" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="3" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="4" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="5" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="6" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="7" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="8" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol alpha="1" name="9" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol alpha="1" name="0" type="line" clip_to_extent="1">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="145,82,45,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <colorramp name="[source]" type="gradient">
      <prop k="color1" v="255,255,255,255"/>
      <prop k="color2" v="255,0,0,255"/>
      <prop k="discrete" v="0"/>
      <prop k="rampType" v="gradient"/>
    </colorramp>
    <mode name="equal"/>
    <rotation/>
    <sizescale/>
    <labelformat format=" %1 - %2 " trimtrailingzeroes="false" decimalplaces="0"/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory backgroundColor="#ffffff" rotationOffset="270" minimumSize="0" height="15" width="15" labelPlacementMethod="XHeight" sizeScale="3x:0,0,0,0,0,0" opacity="1" penAlpha="255" maxScaleDenominator="1e+8" scaleDependency="Area" minScaleDenominator="0" enabled="0" lineSizeType="MM" diagramOrientation="Up" lineSizeScale="3x:0,0,0,0,0,0" barWidth="5" backgroundAlpha="255" scaleBasedVisibility="0" sizeType="MM" penColor="#000000" penWidth="0">
      <fontProperties description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" priority="0" linePlacementFlags="18" placement="2" dist="0" zIndex="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <fieldConfiguration>
    <field name="col">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="row">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="target1col">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="target1row">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="part1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="distance1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lnduSource">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lnduTarg">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="jump">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="targetX">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="targetY">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sourceX">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sourceY">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="SediOut">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cum_sum">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cum_perc">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="col"/>
    <alias name="" index="1" field="row"/>
    <alias name="" index="2" field="target1col"/>
    <alias name="" index="3" field="target1row"/>
    <alias name="" index="4" field="part1"/>
    <alias name="" index="5" field="distance1"/>
    <alias name="" index="6" field="lnduSource"/>
    <alias name="" index="7" field="lnduTarg"/>
    <alias name="" index="8" field="jump"/>
    <alias name="" index="9" field="targetX"/>
    <alias name="" index="10" field="targetY"/>
    <alias name="" index="11" field="sourceX"/>
    <alias name="" index="12" field="sourceY"/>
    <alias name="" index="13" field="SediOut"/>
    <alias name="" index="14" field="cum_sum"/>
    <alias name="" index="15" field="cum_perc"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="col"/>
    <default applyOnUpdate="0" expression="" field="row"/>
    <default applyOnUpdate="0" expression="" field="target1col"/>
    <default applyOnUpdate="0" expression="" field="target1row"/>
    <default applyOnUpdate="0" expression="" field="part1"/>
    <default applyOnUpdate="0" expression="" field="distance1"/>
    <default applyOnUpdate="0" expression="" field="lnduSource"/>
    <default applyOnUpdate="0" expression="" field="lnduTarg"/>
    <default applyOnUpdate="0" expression="" field="jump"/>
    <default applyOnUpdate="0" expression="" field="targetX"/>
    <default applyOnUpdate="0" expression="" field="targetY"/>
    <default applyOnUpdate="0" expression="" field="sourceX"/>
    <default applyOnUpdate="0" expression="" field="sourceY"/>
    <default applyOnUpdate="0" expression="" field="SediOut"/>
    <default applyOnUpdate="0" expression="" field="cum_sum"/>
    <default applyOnUpdate="0" expression="" field="cum_perc"/>
  </defaults>
  <constraints>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="col"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="row"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="target1col"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="target1row"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="part1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="distance1"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="lnduSource"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="lnduTarg"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="jump"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="targetX"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="targetY"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="sourceX"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="sourceY"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="SediOut"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="cum_sum"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" notnull_strength="0" field="cum_perc"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" desc="" field="col"/>
    <constraint exp="" desc="" field="row"/>
    <constraint exp="" desc="" field="target1col"/>
    <constraint exp="" desc="" field="target1row"/>
    <constraint exp="" desc="" field="part1"/>
    <constraint exp="" desc="" field="distance1"/>
    <constraint exp="" desc="" field="lnduSource"/>
    <constraint exp="" desc="" field="lnduTarg"/>
    <constraint exp="" desc="" field="jump"/>
    <constraint exp="" desc="" field="targetX"/>
    <constraint exp="" desc="" field="targetY"/>
    <constraint exp="" desc="" field="sourceX"/>
    <constraint exp="" desc="" field="sourceY"/>
    <constraint exp="" desc="" field="SediOut"/>
    <constraint exp="" desc="" field="cum_sum"/>
    <constraint exp="" desc="" field="cum_perc"/>
  </constraintExpressions>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" actionWidgetStyle="dropDown" sortOrder="0">
    <columns>
      <column name="col" width="-1" type="field" hidden="0"/>
      <column name="row" width="-1" type="field" hidden="0"/>
      <column name="target1col" width="-1" type="field" hidden="0"/>
      <column name="target1row" width="-1" type="field" hidden="0"/>
      <column name="part1" width="-1" type="field" hidden="0"/>
      <column name="distance1" width="-1" type="field" hidden="0"/>
      <column name="lnduSource" width="-1" type="field" hidden="0"/>
      <column name="lnduTarg" width="-1" type="field" hidden="0"/>
      <column name="jump" width="-1" type="field" hidden="0"/>
      <column name="targetX" width="-1" type="field" hidden="0"/>
      <column name="targetY" width="-1" type="field" hidden="0"/>
      <column name="sourceX" width="-1" type="field" hidden="0"/>
      <column name="sourceY" width="-1" type="field" hidden="0"/>
      <column name="SediOut" width="-1" type="field" hidden="0"/>
      <column name="cum_sum" width="-1" type="field" hidden="0"/>
      <column name="cum_perc" width="-1" type="field" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
    </columns>
  </attributetableconfig>
  <editform></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="SediOut" editable="1"/>
    <field name="col" editable="1"/>
    <field name="cum_perc" editable="1"/>
    <field name="cum_sum" editable="1"/>
    <field name="distance1" editable="1"/>
    <field name="jump" editable="1"/>
    <field name="lnduSource" editable="1"/>
    <field name="lnduTarg" editable="1"/>
    <field name="part1" editable="1"/>
    <field name="row" editable="1"/>
    <field name="sourceX" editable="1"/>
    <field name="sourceY" editable="1"/>
    <field name="target1col" editable="1"/>
    <field name="target1row" editable="1"/>
    <field name="targetX" editable="1"/>
    <field name="targetY" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="SediOut" labelOnTop="0"/>
    <field name="col" labelOnTop="0"/>
    <field name="cum_perc" labelOnTop="0"/>
    <field name="cum_sum" labelOnTop="0"/>
    <field name="distance1" labelOnTop="0"/>
    <field name="jump" labelOnTop="0"/>
    <field name="lnduSource" labelOnTop="0"/>
    <field name="lnduTarg" labelOnTop="0"/>
    <field name="part1" labelOnTop="0"/>
    <field name="row" labelOnTop="0"/>
    <field name="sourceX" labelOnTop="0"/>
    <field name="sourceY" labelOnTop="0"/>
    <field name="target1col" labelOnTop="0"/>
    <field name="target1row" labelOnTop="0"/>
    <field name="targetX" labelOnTop="0"/>
    <field name="targetY" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <expressionfields/>
  <previewExpression>col</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
