import os
import logging

from qgis.core import *
from qgis.utils import iface
from qgis.gui import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class QProject:
    def __init__(self, folder, name):
        self.folder = folder
        self.name = name
        self.project = None
        self.root = None
        self.canvas = None
        self.epsg = None
        self.logger = logging.getLogger(__name__)

    def createProject(self, epsg):
        os.chdir(self.folder)
        qgsfile = self.name

        if os.path.isfile(qgsfile):
            os.remove(qgsfile)
        self.project = QgsProject.instance()
        self.project.setFileName(qgsfile)
        self.root = self.project.layerTreeRoot()
        self.manager = self.project.layoutManager()

        self.canvas = QgsMapCanvas()
        self.canvas.setCanvasColor(Qt.white)

        self.epsg = epsg
        my_crs = QgsCoordinateReferenceSystem(epsg, QgsCoordinateReferenceSystem.EpsgCrsId)
        self.canvas.setDestinationCrs(my_crs)
        return

    def addProjectMetaData(self):
        md = self.project.metadata()
        title = 'Modelberekeningen CNWS - %s' % self.name
        md.setTitle(title)
        md.setAuthor('Departement Omgeving, Vlaamse Overheid, Daan Renders (Fluves)')
        md.setIdentifier('CNWS model output')
        md.setAbstract('output CNWS model')
        self.project.setMetadata(md)

    def saveProject(self):
        self.project.write()
        return

    def AddShp(self, inShp, lyrname, key, style):
        if inShp is not None:
            lyr = QgsVectorLayer(inShp, lyrname, 'ogr')
        if lyr.isValid():
            if style is not None:
                lyr.loadNamedStyle(style)
            lyr.setCrs(QgsCoordinateReferenceSystem(self.epsg))
            self.project.addMapLayer(lyr)
        else:
            self.logger.info('%s is not loaded to the qgs-project!' % lyrname)
        return

    def AddRst(self, rst, key, style):
        if rst is not None:
            fileInfo = QFileInfo(rst)
            baseName = fileInfo.baseName()
            lyr = QgsRasterLayer(rst, baseName)
            if lyr.isValid():
                if style is not None:
                    lyr.loadNamedStyle(style)
                lyr.setCrs(QgsCoordinateReferenceSystem(self.epsg))
                self.project.addMapLayer(lyr)
            else:
                self.logger.info('%s is not loaded to the qgs-project!' % baseName)
        return

    def CreateLayout(self, layoutName, lstLyrs, extent, title):
        layout = QgsPrintLayout(QP.project)
        layout.initializeDefaults()
        layout.setName(layoutName)
        self.manager.addLayout(layout)

        page = layout.pageCollection().page(0)
        if extent.width() > extent.height():
            page.setPageSize('A4', 1)  # landscape
        else:
            page.setPageSize('A4', 0)  # portrait

        h = page.pageSize().height() - 60
        w = page.pageSize().width() - 20

        # toevoegen van de kaart
        itemMap = QgsLayoutItemMap(layout)
        itemMap.attemptSetSceneRect(QRectF(10, 10, w, h))
        itemMap.setFrameEnabled(True)
        itemMap.setFrameStrokeWidth(QgsLayoutMeasurement(0.1, QgsUnitTypes.LayoutMillimeters))
        itemMap.setLayers(lstLyrs)
        layout.addLayoutItem(itemMap)
        # itemMap.setExtent(extent)
        itemMap.zoomToExtent(extent)

        # toevoegen van de legende
        itemLegend = QgsLayoutItemLegend(layout)
        itemLegend.setLinkedMap(itemMap)
        itemLegend.setLegendFilterByMapEnabled(True)
        itemLegend.setTitle('Legende')
        itemLegend.attemptSetSceneRect(QRectF(10, 150, 10, 10))
        itemLegend.setResizeToContents(True)
        layout.addLayoutItem(itemLegend)

        # toevoegen van de noordpijl
        itemNorth = QgsLayoutItemPicture(layout)
        itemNorth.setLinkedMap(itemMap)
        svg = QgsApplication.prefixPath()
        svg = os.path.join(svg, 'svg/arrows/NorthArrow_04.svg')
        itemNorth.setPicturePath(svg)
        # itemNorth.setReferencePoint(QgsLayoutItem.LowerRight)
        itemNorth.attemptSetSceneRect(QRectF(10, 200, 10, 10))
        itemNorth.setLocked(True)
        layout.addLayoutItem(itemNorth)

        # toevoegen van de schaal
        itemScale = QgsLayoutItemScaleBar(layout)
        itemScale.setLinkedMap(itemMap)
        itemScale.applyDefaultSettings()
        itemScale.setNumberOfSegments(5)
        itemScale.setNumberOfSegmentsLeft(0)
        itemScale.setUnits(QgsUnitTypes.DistanceKilometers)
        itemScale.setUnitLabel('km')
        itemScale.setUnitsPerSegment(1)
        itemNorth.setLocked(True)
        layout.addLayoutItem(itemScale)

        # toevoegen van de titel
        itemTitle = QgsLayoutItemLabel(layout)
        itemTitle.setText(title)
        itemTitle.attemptSetSceneRect(QRectF(10, 10, 10, 10))
        itemTitle.adjustSizeToText()
        itemTitle.setLocked(True)
        layout.addLayoutItem(itemTitle)

        # toevoegen van de uitleg

        # toevoegen van de disclaimer

        return

    def WriteLayoutToPDF(self, layoutName, dst):
        layoutobject = self.manager.layoutByName(layoutName)
        export = QgsLayoutExporter(layoutobject)
        settings = QgsLayoutExporter.PdfExportSettings()
        settings.dpi = 300
        settings.forceVectorOutput = True
        settings.exportMetadata = True
        settings.rasterizeWholeImage = False
        export.exportToPdf(dst, settings)
        return