# README #
## Inleiding
CNWS.py bevat alle tools en functies die nodig zijn om de inputbestanden voor het CNWS-model aan te maken.
Deze tools worden aangesproken vanuit ExecuteCNWSmodel.py
Dasets.xlsx is een configuratiefile voor het masterscript. Hier worden alle paden naar de brondata bewaard.

## Dependencies 
python 3.6
numpy
pandas
geopandas
rasterio

gdal/ogr utilities
saga_cmd

De python-environment waarin alle functies getest zijn wordt beschreven in CNWS_specs.txt
dit environment kun je instaleren door
conda create --name CNWSenv -- file [pad naar file]CNWS_specs.txt

In deze environment zijn de gdal/ogr utilities reeds opgenomen. Saga moet nog afzonderlijk toegevoegd worden aan PATH

## Branches
De stabiele versie is in de 'stable' branch te vinden, de ontwikkelingsbranch in 'development'. Voor elke nieuwe feature wordt een nieuwe branch gemaakt uit de 'development' branch. Eens de feature af is dan wordt er een pull request aangemaakt, en vervolgens gemerged met de development branch.  Opmerking: Het is aangewezen om eerst de development branch te pullen, om dan een merge van de feature branch te doen in de development branch!