import os
import logging 

from qgis.core import *
from qgis.utils import iface
from qgis.gui import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
        
class QProject:
    def __init__(self, folder, name):
        self.folder = folder
        self.name = name
        self.project = None
        self.root = None
        self.canvas = None
        self.epsg = None
        self.logger = logging.getLogger(__name__)
    
    def createProject(self, epsg):
        os.chdir(self.folder)
        qgsfile = self.name
        
        if os.path.isfile(qgsfile):
            os.remove(qgsfile)
        self.project = QgsProject.instance()
        self.project.setFileName(qgsfile)
        self.root = self.project.layerTreeRoot()
        self.manager = self.project.layoutManager()

        self.canvas = QgsMapCanvas()
        self.canvas.setCanvasColor(Qt.white)
        
        self.epsg = epsg
        my_crs = QgsCoordinateReferenceSystem(epsg,QgsCoordinateReferenceSystem.EpsgCrsId)
        self.canvas.setDestinationCrs(my_crs)
        return
        
    def addProjectMetaData(self):
        md = self.project.metadata()
        title = 'Modelberekeningen CNWS - %s' % self.name
        md.setTitle(title)
        md.setAuthor('Departement Omgeving, Vlaamse Overheid, Daan Renders (Fluves)')
        md.setIdentifier('CNWS model output')
        md.setAbstract('output CNWS model')
        self.project.setMetadata(md)
        
    def saveProject(self):
        self.project.write()
        return

    def AddShp(self, inShp, lyrname, key, style):
        if inShp is not None:
            lyr = QgsVectorLayer(inShp, lyrname, 'ogr')
        if lyr.isValid():
            if style is not None:
                lyr.loadNamedStyle(style)
            lyr.setCrs(QgsCoordinateReferenceSystem(self.epsg))
            self.project.addMapLayer(lyr)
        else:
            self.logger.info('%s is not loaded to the qgs-project!' % lyrname)
        return

    def AddRst(self, rst, key, style):
        if rst is not None:
            fileInfo = QFileInfo(rst)
            baseName = fileInfo.baseName()
            lyr = QgsRasterLayer(rst, baseName)
            if lyr.isValid():
                if style is not None:
                    lyr.loadNamedStyle(style)
                lyr.setCrs(QgsCoordinateReferenceSystem(self.epsg))
                self.project.addMapLayer(lyr)
            else:
                self.logger.info('%s is not loaded to the qgs-project!' % baseName)
        return
    
    def CreateLayout(self, layoutName, lstLyrs, extent, title):
        layout = QgsPrintLayout(QP.project)
        layout.initializeDefaults()
        layout.setName(layoutName)
        self.manager.addLayout(layout)
        
        page = layout.pageCollection().page(0)
        if extent.width() > extent.height():
            page.setPageSize('A4', 1) #landscape
        else:
            page.setPageSize('A4', 0) #portrait
            
        h = page.pageSize().height() - 60
        w = page.pageSize().width() - 20

        #toevoegen van de kaart
        itemMap = QgsLayoutItemMap(layout)
        itemMap.attemptSetSceneRect(QRectF(10, 10, w, h))
        itemMap.setFrameEnabled(True)
        itemMap.setFrameStrokeWidth(QgsLayoutMeasurement(0.1, QgsUnitTypes.LayoutMillimeters))
        itemMap.setLayers(lstLyrs)
        layout.addLayoutItem(itemMap)
        #itemMap.setExtent(extent)
        itemMap.zoomToExtent(extent)
        
        #toevoegen van de legende
        itemLegend = QgsLayoutItemLegend(layout)
        itemLegend.setLinkedMap(itemMap)
        itemLegend.setLegendFilterByMapEnabled(True)
        itemLegend.setTitle('Legende')
        itemLegend.attemptSetSceneRect(QRectF(10, 150, 10, 10))
        itemLegend.setResizeToContents(True)
        layout.addLayoutItem(itemLegend)
        
        #toevoegen van de noordpijl
        itemNorth = QgsLayoutItemPicture(layout)
        itemNorth.setLinkedMap(itemMap)
        svg = QgsApplication.prefixPath()
        svg = os.path.join(svg, 'svg/arrows/NorthArrow_04.svg')
        itemNorth.setPicturePath(svg)
        #itemNorth.setReferencePoint(QgsLayoutItem.LowerRight)
        itemNorth.attemptSetSceneRect(QRectF(10, 200, 10, 10))
        itemNorth.setLocked(True)
        layout.addLayoutItem(itemNorth)
        
        #toevoegen van de schaal
        itemScale = QgsLayoutItemScaleBar(layout)
        itemScale.setLinkedMap(itemMap)
        itemScale.applyDefaultSettings()
        itemScale.setNumberOfSegments(5)
        itemScale.setNumberOfSegmentsLeft(0)
        itemScale.setUnits(QgsUnitTypes.DistanceKilometers)
        itemScale.setUnitLabel('km')
        itemScale.setUnitsPerSegment(1)
        itemNorth.setLocked(True)
        layout.addLayoutItem(itemScale)
        
        #toevoegen van de titel
        itemTitle = QgsLayoutItemLabel(layout)
        itemTitle.setText(title)
        itemTitle.attemptSetSceneRect(QRectF(10, 10, 10, 10))
        itemTitle.adjustSizeToText()
        itemTitle.setLocked(True)
        layout.addLayoutItem(itemTitle)
        
        #toevoegen van de uitleg
        
        #toevoegen van de disclaimer
        

        return
        
    def WriteLayoutToPDF(self, layoutName, dst):
        layoutobject = self.manager.layoutByName(layoutName)
        export = QgsLayoutExporter(layoutobject)
        settings  = QgsLayoutExporter.PdfExportSettings()
        settings.dpi = 300
        settings.forceVectorOutput = True
        settings.exportMetadata = True
        settings.rasterizeWholeImage = False
        export.exportToPdf(dst, settings)
        return
    

##vars
ppfolder = r'D:\Dropbox (Fluves)\ALBON\testen\qgis\Etikhove\Scenario_2\postprocessing'
outputfolder = r'D:\Dropbox (Fluves)\ALBON\testen\qgis\Etikhove\Scenario_2\modeloutput'
inputfolder = r'D:\Dropbox (Fluves)\ALBON\testen\qgis\Etikhove\Scenario_2\2017'
shpfolder_bekken = r'D:\Dropbox (Fluves)\ALBON\testen\qgis\Etikhove\Data_Bekken\Shps'
bekken_naam = 'Etikhove'
scenario_nr = 2
jaar = 2017
qgsfile = '%s_scenario_%s.qgs' % (bekken_naam, scenario_nr)

stylefolder = r'D:\Dropbox (Fluves)\ALBON\Scr\styles'

waterErosRST = os.path.join(outputfolder, 'WATEREROS (kg per gridcel).rst')
rusleRST = os.path.join(outputfolder, 'RUSLE.rst')
upareaRST = os.path.join(outputfolder, 'UPAREA.rst')
SediOutRST = os.path.join(outputfolder, 'SediOut_kg.rst')
PerceelsRST = os.path.join(ppfolder, 'perceelskaart_%s_%s_scenario_%s_nodata.tif' %(bekken_naam, jaar, scenario_nr))
PercelenShp = os.path.join(inputfolder, 'percelen_%s_scenario_%s_%s.shp' %(bekken_naam, scenario_nr, jaar))
#GrasShp = os.path.join(
InfraLineShp = os.path.join(shpfolder_bekken, 'Wegsegment_%s.shp' %bekken_naam)
InfraPolyShp = os.path.join(shpfolder_bekken, 'infra_GRB_%s.shp' %bekken_naam)
WaterlopenShp = os.path.join(shpfolder_bekken, 'VHA_09052017_la72_%s.shp' %bekken_naam)
#BufferShp = os.path.join(
RoutingShp = os.path.join(ppfolder, 'routing_%s_scenario_%s.shp' %(bekken_naam, scenario_nr))
SubCatchmShp = os.path.join(ppfolder, 'subcatchments_%s_%s.shp' %(bekken_naam, scenario_nr))
SedInputShp = os.path.join(ppfolder, 'Sedimentexport2Segments_%s_scenario_%s.shp' %(bekken_naam, scenario_nr))

QP = QProject(ppfolder, qgsfile)
QP.createProject(31370)

QP.AddRst(rusleRST, 'RUSLE', os.path.join(stylefolder, 'RUSLE.qml'))
QP.AddRst(upareaRST, 'UPAREA', os.path.join(stylefolder, 'UPAREA.qml'))
QP.AddRst(waterErosRST, 'WATEROS', os.path.join(stylefolder, 'WATEREROS (kg per gridcel).qml'))
QP.AddRst(SediOutRST, 'SediOut', os.path.join(stylefolder, 'SediOut_kg.qml'))
QP.AddRst(PerceelsRST, 'Perceelskaart', os.path.join(stylefolder, 'perceelskaart.qml'))
QP.AddShp(PercelenShp, 'Perceelsgrenzen', 'Perceelsgrenzen', os.path.join(stylefolder, 'Percelen.qml'))
QP.AddShp(InfraLineShp, 'Infrastructuur', 'Infrastructuur_line', os.path.join(stylefolder, 'InfraLine.qml'))
QP.AddShp(InfraPolyShp, 'Infrastructuur', 'Infrastructuur_poly', os.path.join(stylefolder, 'InfraPoly.qml'))
QP.AddShp(WaterlopenShp, 'Waterlopen', 'Waterlopen', os.path.join(stylefolder, 'Waterlopen.qml'))
QP.AddShp(RoutingShp, 'Routing (springen)', 'Routing_jump', os.path.join(stylefolder, 'Routering.qml'))
QP.AddShp(RoutingShp, 'Routing (parts)', 'Routing_parts', os.path.join(stylefolder, 'Routering_parts.qml'))
QP.AddShp(SubCatchmShp, 'Deelbekkens', 'Deelbekkens', os.path.join(stylefolder, 'Subcatchments.qml'))
QP.AddShp(SubCatchmShp, 'Specifieke sediment productie (ton/ha)', 'SedAr', os.path.join(stylefolder, 'klassen_tonperha.qml'))
QP.AddShp(SedInputShp, 'Sedimentinput per riviersegment (ton/m)', 'SedLen', os.path.join(stylefolder, 'SedLen.qml'))

#QP.AddShp(CatchShp, 'Bekkengrenzen', 'Bekkengrenzen', os.path.join(stelyfolder, 'Catch.qml'))

extent = QP.project.mapLayersByName('Deelbekkens')[0].extent()

#layout1 - uparea
layers = []
layers.append(QP.project.mapLayersByName('Infrastructuur')[0])
layers.append(QP.project.mapLayersByName('Infrastructuur')[1])
layers.append(QP.project.mapLayersByName('Waterlopen')[0])
layers.append(QP.project.mapLayersByName('UPAREA')[0])
title = 'Afstromingspatroon'
QP.CreateLayout('Uparea', layers, extent, title)
QP.WriteLayoutToPDF('Uparea', 'uparea.pdf')

#layout2 - SediOut
layers = []
layers.append(QP.project.mapLayersByName('Infrastructuur')[0])
layers.append(QP.project.mapLayersByName('Infrastructuur')[1])
layers.append(QP.project.mapLayersByName('Waterlopen')[0])
layers.append(QP.project.mapLayersByName('SediOut_kg')[0])
title = 'Sedimentdoorvoer (kg/jaar)'
QP.CreateLayout('SediOut', layers, extent, title)
QP.WriteLayoutToPDF('SediOut', 'sediout.pdf')

#layout3 - WaterEros
layers = []


#layout4 - SubCatchments
layers = []



QP.saveProject()









