import CNWS
import logging
import time
import os

def CalculateScenario(defaultini, name_catchm, shp_catch, modelversion,
                      homefolder, nr, Output, Opt, EBMopt,
                      Vars, extradata_path, extradata_type, resolutie, postprocess=True):
    """
    Prepare all modelininput for a scenario, run the CN-WS model and do some basic post-processing

    :param configfile: string, the path of the configuration xlsx
    :param defaultini: string, the path of the file with default values (ini-file)
    :param naam: string, name of the catchment
    :param shp_catch: string, the path of the shapefile of the catchment (polygon, 1 feature)
    :param modelversie: string, Modelversion, WS or CNWS
    :param homefolder: string, path to the folder where all data are written and stored
    :param Cnst: dict,
    :param nr: int, number of the scenario
    :param Output: dict
    :param Opt: dict
    :param EBMopt: dict
    :param Vars: dict
    :param extradata_path: list
    :param extradata_type: list
    :return:  nothing
    """

# verder aan te vullen als alle data er is

    try:
        logger = logging.getLogger('CNWS')
        logger.setLevel(logging.INFO)
        starttime = time.time()
        logger.info('----------------')
        logger.info('     CNWS       ')
        logger.info('----------------')
        logger.info(name_catchm)
        logger.info('----------------')

        catchment = UK_catchment(name_catchm, shp_catch)
        scenario = CNWS.Scenario(catchment.catchm, nr)
        scenario.SetModelVersie(modelversion)
        scenario.SetOutput(Output, defaultini)
        scenario.SetModelOptions(Opt, defaultini)
        scenario.SetEBMOptions(EBMopt, defaultini)
        scenario.SetModelVariables(Vars, defaultini)
        scenario.CalculateTimeSettings()

        catchment.catchm.SetConstants(res=resolutie, epsg='EPSG:XXXXXX')
        #catchment.catchm.PrepareRasterProperties(DTM_src)
        catchment.catchm.CreateFolderStructure(homefolder, scenario.jaren)
        catchment.catchm.PrepareP()
        #catchment.catchm.PrepareDTM(DTM_src)
        #catchment.catchm.PrepareK(Kfact_src)
        #catchment.catchm.PrepareLandGebruik(LandUseSrc)

        #catchment.PrepareWater(water_src)

        #bekken.PrepareParcesls(parcels_src)

        catchment.catchm.PrepareWaterShps()
        catchment.catchm.TopologizeRivers()
        catchment.catchm.PrepareWaterRsts()
        catchment.catchm.CreateInfraTif()

        scenario.CreateFolderStructure()

        for i, extra in enumerate(extradata_path):
            scenario.AddExtraEBMData(extra, extradata_type[i])

        #scenario.CreateModelInput(b.GetGewasInfo(), {})

        endtime = time.time()
        logger.info('Pre-processing took %s seconds' % round((endtime - starttime), 2))
        scenario.RunModel()
        starttime = time.time()
        logger.info('Processing took %s seconds' % round((starttime - endtime), 2))

        if postprocess:
            pp = CNWS.PostProcessScenario(scenario)
            pp.DoPostprocessing()
            endtime = time.time()
            logger.info('Post-processing took %s seconds' % round((endtime - starttime), 2))

        logger.info('-------------')
        logger.info('FINISHED! \o/')
        logger.info('-------------')
    except CNWS.CNWSException as e:
        logger.error(e)
    return


#In de class UK_catchment kun je alle data pre-processen die nodig zijn voor het bekken dat doorgerekend wordt
#Sommige datasets, zoals het DTM, raster met K-factoren of Land-Use raster maak je best 1x op voorhand en zet ze in het juiste formaat
#dan kunnen deze reeds met de CNWS.py module verwerkt worden


#eventueel kun je ook een class SourceData aanmaken (net zoals ik gedaan heb met Brondata en een xls waarin alle paden naar de data in staan)

class UK_catchment():
    def __init__(self, catchmentname, catchmentshp):
        self.catchm = CNWS.Catchment(catchmentname, catchmentshp)

        self.logger = logging.getLogger(__name__)

    def PrepareWater(self, inShp):
        #probeer een lijnenbestand met riviersegmenten te maken/verkrijgen voor het catchment.
        # de variabele die het pad naar dit lijnenbestand bevat is
        self.catchm.waterLine = ''
        # probeer, indien mogelijk, de dataset weg te schrijven in
        self.catchm.shpfolder
        # de waterlijnsegmenten moeten aan volgende eisen voldoen:
        #- lijnen
        # - getekend van stroomopwaarts naar stroomafwaarts, indien dit niet het geval is kan de optie riverrouting= 1
        # niet gebruikt worden
        # attributen zijn niet noodzakelijk, elk segment wordt later in de workflow opnieuw genummerd en krijgt daar de nodige attributen
        return

    def PrepareParcels(self, inShp, year):

        verwerkteshp = ''
        #de verwerkte shp opslaan in
        path = os.path.join(self.catchm.shpfolder, year)

        self.catchm.percelen[year] = verwerkteshp

        return
